//
//  SocialCommentResponse.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialCommentResponse {
    var next_page_cursor_id: String?
    var comments: [SocialComment] = []
    
    init(_ json: JSON) {
        next_page_cursor_id = json["next_page_cursor_id"].string
        
        json["comments"].arrayValue.forEach { (json) in
            self.comments.append(SocialComment(json))
        }
        
    }
}
