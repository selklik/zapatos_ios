//
//  Transaction.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Transaction {
    var reference_id: String
    var type: String
    var value: Int
    var post: Post?
    var timestamp: String

    init(_ json: JSON) {
        reference_id = json["reference_id"].stringValue
        type = json["type"].stringValue
        value = json["value"].intValue
        
        if json["post"].dictionary != nil {
            post = Post(json["post"])
        }
        
        timestamp = json["timestamp"].stringValue
    }
}
