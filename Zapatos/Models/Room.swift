//
//  Room.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Room {
    var read: Bool
    var artist: Artist
    var preview: String
    var timestamp: String
    
    init(_ json: JSON) {
        read = json["read"].boolValue
        artist = Artist(json["artist"])
        preview = json["preview"].stringValue
        timestamp = json["timestamp"].stringValue
    }
}
