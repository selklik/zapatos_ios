//
//  Prodcuts.swift
//  Zain Saidin
//
//  Created by MacBook Air on 22/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Product {
    var id: Int
    var sku: String?
    var title: String
    var price: String
    var numeric_price: Double
    var currency: String
    var currency_symbol: String
    var view_count: Int
    var details: String
    //var category: ProductCategory
    var photos: [Versions] = []
    var isFake = false
    var json: JSON
    var bought_by: User?
    var timestamp: String?
    
    init(_ json: JSON) {
        self.json = json
        id = json["id"].intValue
        sku = json["sku"].string
        title = json["title"].stringValue
        price = json["price"].stringValue
        numeric_price = json["numeric_price"].doubleValue
        currency = json["currency"].stringValue
        currency_symbol = json["currency_symbol"].stringValue
        view_count = json["view_count"].intValue
        details = json["details"].stringValue
        //category = ProductCategory(json["category"])
        json["photos"].arrayValue.forEach { (json) in
            photos.append(Versions(json))
        }
        
        if json["bought_by"].dictionary != nil {
            bought_by = User(json["bought_by"])
            timestamp = nil
        }
    }
    
    init() {
        json = JSON()
        id = 0
        sku = ""
        title = ""
        price = ""
        numeric_price = 0.0
        currency = ""
        currency_symbol = ""
        view_count = 0
        details = ""
        //category = ProductCategory(JSON([
          //  "id": 0,
            //"name": ""
        //]))
        
        photos = [Versions(JSON(["full_resolution": "https://i.imgur.com/YslHv5c.jpg"]))]
        isFake = true
    }
    
    func toDictionary() -> [String: Any] {
        return json.dictionaryObject!
    }
}

