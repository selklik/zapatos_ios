
//  Country.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Country {
    var id: Int
    var name: String
    var country_code: String
    var continent: String
    var flag: String
    
    init(_ json: JSON)  {
        id = json["id"].intValue
        name = json["name"].stringValue
        country_code = json["country_code"].stringValue
        continent = json["continent"].stringValue
        flag = json["flag"].stringValue
    }
}
