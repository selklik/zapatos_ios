//
//  Card.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 26/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Card {
    var id: Int
    var source_id: String
    var brand: String
    var last4: String
    var exp_month: Int
    var exp_year: Int
    var country: String
    var is_default: Bool
    var user: User
    var kind: String
    
    init(_ json: JSON) {
        id = json["id"].intValue
        source_id = json["source_id"].stringValue
        brand = json["brand"].stringValue
        last4 = json["last4"].stringValue
        exp_month = json["exp_month"].intValue
        exp_year = json["exp_year"].intValue
        country = json["country"].stringValue
        is_default = json["is_default"].boolValue
        user = User(json["user"])
        kind = json["kind"].stringValue
    }
}
