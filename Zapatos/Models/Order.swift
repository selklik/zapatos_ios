//
//  Order.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Order {
    
    var cus_email: String
    var cus_orderid: String
    var prod_id: String
    var prod_name: String
    var description: String
    var cus_prodquantity: String
    var cus_sizes: String
    var prod_image: String
    var totalAmount: String
    var trackid: String
    var date: String
    
    init(_ json: JSON) {
        cus_email = json["cus_email"].stringValue
        cus_orderid = json["cus_orderid"].stringValue
        prod_id = json["prod_id"].stringValue
        prod_name = json["prod_name"].stringValue
        description = json["description"].stringValue
        cus_prodquantity = json["cus_prodquantity"].stringValue
        cus_sizes = json["cus_sizes"].stringValue
        prod_image = json["prod_image"].stringValue
        totalAmount = json["totalAmount"].stringValue
        trackid = json["trackid"].stringValue
        date = json["date"].stringValue
    }
}
