//
//  GenericResponse.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class GenericResponse {
    var liked: Bool
    var deleted: Bool
    var logged_out: Bool
    var email_sent: Bool
    
    init(_ json: JSON) {
        liked = json["liked"].boolValue
        deleted = json["deleted"].boolValue
        logged_out = json["logged_out"].boolValue
        email_sent = json["email_sent"].boolValue
    }
}
