//
//  Address.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 26/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Address {

    var id: Int
    var email: String
    var phone: String
    var address: String
    var is_default: String
    
    init(_ json: JSON) {
        id = json["id"].intValue
        email = json["email"].stringValue
        phone = json["phone"].stringValue
        address = json["address"].stringValue
        is_default = json["default_address"].stringValue

    }
}
