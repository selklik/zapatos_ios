//
//  AddressComponent.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import Alamofire

class AddressComponent {
    var line1: String?
    var line2: String?
    var city: String?
    var state: String?
    var country: String?
    var zipcode: Int?
    var isValid: Bool {
        return line1 != nil && line2 != nil && city != nil && state != nil && country != nil && zipcode != nil
    }
    var parameters: Parameters {
        return [
            "AL-1": line1!,
            "AL-2": line2!,
            "City": city!,
            "State": state!,
            "country": country!,
            "Zipcode": zipcode!
        ]
    }
}
