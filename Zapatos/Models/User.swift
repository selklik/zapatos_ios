//
//  User.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {    
    var id: Int
    var name: String
    var email: String
    var notification: Bool {
        didSet {
            json["current_balance"].bool = notification
            save()
        }
    }
    
    var current_balance: Int {
        didSet {
            json["current_balance"].int = current_balance
            save()
        }
    }
    var profile_photos: Versions {
        didSet {
            json["notification"] = profile_photos.json
            save()
        }
    }
    var access_token: String?
    var json: JSON
    
    init(_ json: JSON) {
        self.json = json
        print(json)
        id = json["id"].intValue
        name = json["name"].stringValue
        email = json["email"].stringValue
        notification = json["notification"].boolValue
        current_balance = json["current_balance"].intValue
        profile_photos = Versions(json["profile_photos"])
        access_token = json["access_token"].string
  
    }
    
    func save() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userSaved"), object: nil)
        UserDefaults.standard.set(json.rawString()!, forKey: "user")
        // print("Saving user: \(json.rawString()!)")
    }        
    
    class func load() -> User? {
        if let jsonString = UserDefaults.standard.value(forKey: "user") as? String {
            let json = JSON(parseJSON: jsonString)
            return User(json)
        }
        
        return nil
    }
}


