//
//  Products.swift
//  Zapatos
//
//  Created by MacBook Air on 26/09/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//
import Foundation
import SwiftyJSON

class ProductImage {
    
    var prod_img: String
    var prod_color: String
    var size_quantity: [SizeQuantity]
    
    
    init(_ json: JSON) {
        prod_img = json["prod_img"].stringValue
        prod_color = json["prod_color"].stringValue
        size_quantity = json["size_quantity"].arrayValue.map({ (json) -> SizeQuantity in
            return SizeQuantity(json)
        })
    }

}

