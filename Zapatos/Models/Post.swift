//
//  Post.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Post {
    var id: Int
    var artist: Artist
    var type: String
    var body: String?
    var attachment: URL?
    var attachment_type: String?
    var metadata: PostMetadata
    var comment_count: Int
    var like_count: Int
    var liked: Bool
    var timestamp: String
    var photos: [PostPhoto] = []
    var json: JSON
    
    init(_ json: JSON) {
        self.json = json
        
        id = json["id"].intValue
        artist = Artist(json["artist"])
        type = json["type"].stringValue
        body = json["body"].string
        
        if let string = json["attachment"].string {
            attachment = URL(string: string)!
        }
        
        attachment_type = json["attachment_type"].string
        metadata = PostMetadata(json["metadata"])
        comment_count = json["comment_count"].intValue
        like_count = json["like_count"].intValue
        liked = json["liked"].boolValue
        timestamp = json["timestamp"].stringValue
        
        photos = json["photos"].arrayValue.map({ (json) -> PostPhoto in
            return PostPhoto(json)
        })
    }
    
    func toJSONString() -> String {
        return json.rawString()!
    }
}
