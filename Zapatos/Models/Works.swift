//
//  Works.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Works {
    var filmography: [Work] = []
    var discography: [Work] = []
    
    init(_ json: JSON) {
        filmography = json["filmography"].arrayValue.map({ (json) -> Work in return Work(json) })
        discography = json["discography"].arrayValue.map({ (json) -> Work in return Work(json) })
    }
}
