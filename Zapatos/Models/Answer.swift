//
//  Answer.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Answer {
    var video: AnswerVideo
    var screenshots: Versions
    
    init(_ json: JSON) {
        video = AnswerVideo(json["video"])
        screenshots = Versions(json["screenshots"])
    }
}
