//
//  SocialNetworkAccount.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation

class SocialNetworkAccount {
    var type: SocialNetworkAccountType
    var url: URL
    var displayName: String {
        let handle = url.description.components(separatedBy: "/").filter { (component) -> Bool in
            return component.count > 0
        }.last!
        
        switch type {
            case .facebook:
                return kArtistFBName
            
            default:
                return "@\(handle)"
        }
    }
    
    init(type: SocialNetworkAccountType, url: URL) {
        self.type = type
        self.url = url
    }
}
