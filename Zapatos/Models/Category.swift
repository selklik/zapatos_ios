//
//  Category.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Category {
    var id: Int
    var name: String
    var icon_light: String
    var icon_dark: String
    
    init(_ json: JSON) {
        
        id = json["id"].intValue
        name = json["name"].stringValue
        icon_light = json["icon_light"].stringValue
        icon_dark = json["icon_dark"].stringValue
    }
}
