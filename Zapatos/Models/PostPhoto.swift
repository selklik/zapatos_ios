//
//  PostPhoto.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class PostPhoto {
    var width: Double
    var height: Double
    var versions: Versions
    
    init(_ json: JSON) {        
        width = json["width"].doubleValue
        height = json["height"].doubleValue
        versions = Versions(json["versions"])
    }
}
