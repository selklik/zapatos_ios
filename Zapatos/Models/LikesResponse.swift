//
//  LikesResponse.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class LikesResponse{
    var count: Int
    var liked_by: [UserOrArtist] = []
 
    init(_ json: JSON) {
        count = json["count"].intValue
        json["liked_by"].arrayValue.forEach { (json) in
            self.liked_by.append(UserOrArtist(json))
        }
    }
}
