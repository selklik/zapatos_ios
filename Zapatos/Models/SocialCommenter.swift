//
//  SocialCommenter.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialCommenter {
    var name: String
    var profile_photo: String

    init(_ json: JSON) {
        name = json["name"].stringValue
        profile_photo = json["profile_photo"].stringValue
    }
}
