//
//  datas.swift
//  Zapatos
//
//  Created by MacBook Air on 26/09/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Datas {
    var data: [Shoes] = []

    var json: JSON
    
    init(_ json: JSON) {
        self.json = json
        data = json["data"].arrayValue.map({ (json) -> Shoes in
            return Shoes(json)
        })

    }
}
