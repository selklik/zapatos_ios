//
//  SizeQuantity.swift
//  Zapatos
//
//  Created by MacBook Air on 03/10/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class SizeQuantity {
    var size: String
    var quantity: String
    
    init(_ json: JSON) {
        
        size = json["size"].stringValue
        quantity = json["quantity"].stringValue
   
    }
    
}
