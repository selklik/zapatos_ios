//
//  OrderData.swift
//  Zapatos
//
//  Created by MacBook Air on 21/12/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class OrderData {
    var data: [Order] = []
    
    var json: JSON
    
    init(_ json: JSON) {
        self.json = json
        data = json["data"].arrayValue.map({ (json) -> Order in
            return Order(json)
        })
        
    }
}
