//
//  ProfilePhotoResponse.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProfilePhotoResponse {
    var id: Int
    var content_type: String
    var urls: Versions
    
    init(_ json: JSON) {
        id = json["id"].intValue
        content_type = json["content_type"].stringValue
        urls = Versions(json["urls"])
    }
}
