//
//  UserOrArtist.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserOrArtist {
    // Shared
    var id: Int
    var name: String
    var profile_photos: Versions
    
    // User
    var email: String?
    var notification: Bool
    
    // Artist
    var gender: String?
    var followers_count: Int
    var genre: String?
    var country: String?
    var referral_code: String?
    
    var type: String {
        get {
            if gender == nil {
                return "user"
            }
            
            return "artist"
        }
    }
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        profile_photos = Versions(json["profile_photos"])
        email = json["email"].string
        notification = json["notification"].boolValue
        gender = json["gender"].string
        followers_count = json["followers_count"].intValue
        genre = json["genre"].string
        country = json["country"].string
        referral_code = json["referral_code"].string
    }
    init( id: Int, name: String, profile_photos: Versions, notification: Bool, followers_count: Int) {
        
        self.name = name
        self.id = id
        self.profile_photos = profile_photos
        self.notification = notification
        self.followers_count = followers_count
    }
}

