//
//  Artist.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Artist {
    var id: Int
    var name: String
    var gender: String
    var following = false
    var followers_count: Int
    var genre: String
    var country: String
    var country_name: String
    var profile_photos: Versions
    var referral_code: String
    var qna_enabled = false
    var social_network_accounts: SocialNetworkAccounts
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        gender = json["gender"].stringValue
        following = json["following"].boolValue
        followers_count = json["followers_count"].intValue
        genre = json["genre"].stringValue
        country = json["country"].stringValue
        country_name = json["country_name"].stringValue
        profile_photos = Versions(json["profile_photos"])
        referral_code = json["referral_code"].stringValue
        qna_enabled = json["qna_enabled"].boolValue
        social_network_accounts = SocialNetworkAccounts(json["social_network_accounts"])
    }
}
