//
//  CommentsResponse.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class CommentsResponse {
    var count: Int
    var comments: [Comment] = []
    
    init(_ json: JSON) {
        count = json["count"].intValue
        json["comments"].arrayValue.forEach { (json) in
            self.comments.append(Comment(json))
        }

    }
}

