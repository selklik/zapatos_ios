//
//  Following.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Following {
    var count: Int
    var followings: [Artist] = []
    
    init(_ json: JSON) {
        count = json["count"].intValue
        
        json["followings"].arrayValue.forEach { (json) in
            self.followings.append(Artist(json))
    }
 }
    
}
