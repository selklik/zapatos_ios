//
//  Purchase.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
class Purchase {
    var id: Int
    var current_balance: Int
    var timestamp: String

    init(_ json: JSON) {
        id = json["id"].intValue
        current_balance = json["current_balance"].intValue
        timestamp = json["timestamp"].stringValue
    }
}
