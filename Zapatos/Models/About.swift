//
//  About.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class About {
    var about: String
    var about_short: String?
    var welcome_message: String
    var welcome_video_url: URL
    var welcome_video_photos: Versions
    var works: [String: [Work]] = [:]
    
    init(_ json: JSON) {
        about = json["about"].stringValue
        about_short = json["about_short"].string
        welcome_message = json["welcome_message"].stringValue
        welcome_video_url = URL(string: json["welcome_video_url"].stringValue)!
        welcome_video_photos = Versions(json["welcome_video_photos"])
        
        let dict = json["works"].dictionaryValue        
        
        dict.keys.forEach { (key) in
            works[key] = dict[key]!.arrayValue.map({ (json) -> Work in
                return Work(json)
            })
        }
    }
}
