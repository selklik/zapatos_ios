//
//  Pricing.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation

class Pricing {
    var name: String
    var value: Int
    var price: Double
    
    init(name: String, value: Int, price: Double) {
        self.name = name
        self.value = value
        self.price = price
    }
}
