//
//  Banner.swift
//  Zapatos
//
//  Created by MacBook Air on 03/10/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Banner {
    
    var id: String
    var image_url: String
 
    
    init(_ json: JSON) {
        
        id = json["id"].stringValue
        image_url = json["image_url"].stringValue
       
    }
    
}
