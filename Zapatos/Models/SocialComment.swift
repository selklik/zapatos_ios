//
//  SocialComment.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialComment {
    var body: String
    var timestamp: String
    var from: SocialCommenter
    
    init(_ json: JSON) {
        
        body = json["body"].stringValue
        timestamp = json["timestamp"].stringValue
        from = SocialCommenter(json["from"])
    }
}
