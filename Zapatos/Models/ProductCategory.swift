//
//  ProductCategory.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 22/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class ProductCategory {
    var id: Int
    var name: String
    var products: [Product] = []
    var type: String
    var photo: Versions?
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        type = json["type"].stringValue
        
        if let array = json["products"].array {
            products = array.map({ (json) -> Product in
                return Product(json)
            })
        }
        
        if json["photo"].dictionary != nil {
            photo = Versions(json["photo"])
        }
    }
}
