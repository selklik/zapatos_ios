//
//  Comment.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//
import Foundation
import SwiftyJSON

class Comment {
    var id: Int
    var from: UserOrArtist
    var owner: Bool
    var body: String
    var timestamp: String
    
    init(_ json: JSON) {
        id = json["id"].intValue
        from = UserOrArtist(json["from"])
        owner = json["owner"].boolValue
        body = json["body"].stringValue
        timestamp = json["timestamp"].stringValue
    }
    init(id: Int, from: UserOrArtist, owner: Bool, body: String, timestamp: String) {
        
        self.id = id
        self.from = from
        self.owner = owner
        self.body = body
        self.timestamp = timestamp
    }
}
