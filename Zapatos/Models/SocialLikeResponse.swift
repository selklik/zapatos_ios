//
//  SocialLikeResponse.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialLikeResponse {
    var next_page_cursor_id: String?
    var liked_by: [SocialLiker] = []

    init(_ json: JSON) {
        next_page_cursor_id = json["next_page_cursor_id"].string
        
        json["liked_by"].arrayValue.forEach { (json) in
            self.liked_by.append(SocialLiker(json))
        }
  
    }
}
