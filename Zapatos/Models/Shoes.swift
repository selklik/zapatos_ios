//
//  Zapatos.swift
//  Zapatos
//
//  Created by MacBook Air on 26/09/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Shoes {
    
    var id: String
    var prod_id: String
    var cat_id: String
    var prod_name: String
    var description: String
    var price: String
    var int_price: String
    var created: String
    var size: String
    var json: JSON
    var products: [ProductImage]
    
    init(_ json: JSON) {
        self.json = json

        id = json["id"].stringValue
        prod_id = json["prod_id"].stringValue
        cat_id = json["cat_id"].stringValue
        prod_name = json["prod_name"].stringValue
        description = json["description"].stringValue
        price = json["price"].stringValue
        int_price = json["int_price"].stringValue
        created = json["created"].stringValue
        size = json["size"].stringValue
        products = json["products"].arrayValue.map({ (json) -> ProductImage in
            return ProductImage(json)
        })
    }

}
