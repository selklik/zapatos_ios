//
//  Work.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Work {
    var title: String
    var year: String
    var url: URL
    var photos: Versions
    var full_title: String {
        return "\(title)\n(\(year))"
    }
    
    init(_ json: JSON) {
        title = json["title"].stringValue
        year = json["year"].stringValue
        url = URL(string: json["url"].stringValue)!
        photos = Versions(json["photos"])
    }
}
