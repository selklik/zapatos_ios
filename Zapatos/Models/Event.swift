//
//  Event.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Event {
    var id: Int
    var name: String
    var poster: String
    
    init(_ json: JSON) {
        id = json["id"].intValue
        name = json["name"].stringValue
        poster = json["poster"].stringValue
    }
}

