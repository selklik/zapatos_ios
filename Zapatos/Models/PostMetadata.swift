//
//  PostMetadata.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class PostMetadata {
    var category: Category?
    var question_and_answer_id: Int
    var price: Int
    var earning: Int
    var full_access = false
    var number_of_purchases: Int
    var account_name: String?
    var account_username: String?
    var post_id: String?
    var comment_count: Int
    var like_count: Int
    var liked = false
    var user: User?
    
    init(_ json: JSON) {
        if json["category"].dictionary != nil {
            category = Category(json["category"])
        }
        question_and_answer_id = json["question_and_answer_id"].intValue
        price = json["price"].intValue
        earning = json["earning"].intValue
        full_access = json["full_access"].boolValue
        number_of_purchases = json["number_of_purchases"].intValue
        account_name = json["account_name"].string
        account_username = json["account_username"].string
        post_id = json["post_id"].string
        comment_count = json["comment_count"].intValue
        like_count = json["like_count"].intValue
        liked = json["liked"].boolValue
        
        if json["user"].dictionary != nil {
            user = User(json["user"])
        }
    }
}
