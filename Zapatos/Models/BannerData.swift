//
//  BannerData.swift
//  Zapatos
//
//  Created by MacBook Air on 03/10/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class BannerData {
    var data: [Banner]
    
    var json: JSON
    
    init(_ json: JSON) {
        self.json = json
        data = json["data"].arrayValue.map({ (json) -> Banner in
            return Banner(json)
        })
        
    }
}
