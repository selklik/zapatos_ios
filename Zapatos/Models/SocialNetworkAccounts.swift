//
//  SocialNetworkAccounts.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class SocialNetworkAccounts {
    var facebook: URL?
    var twitter: URL?
    var instagram: URL?
    var youtube = URL(string: "https://www.youtube.com/user/DatoSNurhalizaVEVO")
    
    var accounts: [SocialNetworkAccount] {
        var accts: [SocialNetworkAccount] = []
        
        if let instagram = instagram {
            accts.append(SocialNetworkAccount(type: .instagram, url: instagram))
        }
        
        if let youtube = youtube {
            accts.append(SocialNetworkAccount(type: .youtube, url: youtube))
        }
        if let facebook = facebook {
            accts.append(SocialNetworkAccount(type: .facebook, url: facebook))
        }
        
        if let twitter = twitter {
            accts.append(SocialNetworkAccount(type: .twitter, url: twitter))
        }
        
        return accts
    }
    
    init(_ json: JSON) {
        
        if let string = json["instagram"].string {
            instagram = URL(string: string)!
        }
       
        if let string = json["facebook"].string {
            facebook = URL(string: string)!
        }
        
        if let string = json["twitter"].string {
            twitter = URL(string: string)!
        }
    }
}
