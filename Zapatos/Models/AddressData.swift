
//
//  File.swift
//  Zapatos
//
//  Created by MacBook Air on 19/12/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//
import Foundation
import SwiftyJSON

class AddressData {
    var data: [Address]
    
    var json: JSON
    
    init(_ json: JSON) {
        self.json = json
        data = json["data"].arrayValue.map({ (json) -> Address in
            return Address(json)
        })
        
    }
}

