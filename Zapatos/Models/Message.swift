//
//  Message.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Message {
    var id: Int
    var kind: String
    var question: String?
    var answer: Answer?
    var user: User
    var artist: Artist
    var timestamp: String
    
    init(_ json: JSON) {
        id = json["id"].intValue
        kind = json["kind"].stringValue
        question = json["question"].string
        
        if json["answer"].dictionary != nil {
            answer = Answer(json["answer"])
        }
        
        user = User(json["user"])
        artist = Artist(json["artist"])
        timestamp = json["timestamp"].stringValue
    }
}
