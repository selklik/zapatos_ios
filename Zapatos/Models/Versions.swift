//
//  Versions.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Versions {
    var full_resolution: URL
    var preview: URL?
    var low_resolution: URL?
    var square: URL?
    var thumbnail: URL?
    var json: JSON
    
     init(_ json: JSON)  {
        self.json = json
        
        full_resolution = (URL(string: json["full_resolution"].stringValue))!
        
        if let string = json["preview"].string {
            preview = URL(string: string)!
        }
        
        if let string = json["low_resolution"].string {
            low_resolution = URL(string: string)!
        }
        
        if let string = json["square"].string {
            square = URL(string: string)!
        }
        
        if let string = json["thumbnail"].string {
            thumbnail = URL(string: string)!
        }
    }
}
