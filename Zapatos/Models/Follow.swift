//
//  Follow.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

class Follow {
    var artist_id: Int
    var follow: Bool
    var notification : Bool
    
    init(_ json: JSON) {
        artist_id = json["artist_id"].intValue
        follow = json["follow"].boolValue
        notification = json["notification"].boolValue
    }
}
