//
//  OrderList+CoreDataProperties.swift
//  
//
//  Created by MacBook Air on 02/01/2019.
//
//

import Foundation
import CoreData


extension OrderList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OrderList> {
        return NSFetchRequest<OrderList>(entityName: "OrderList")
    }

    @NSManaged public var product_color: String?
    @NSManaged public var product_id: String?
    @NSManaged public var product_imag: String?
    @NSManaged public var product_name: String?
    @NSManaged public var product_price: String?
    @NSManaged public var product_qty: String?
    @NSManaged public var product_size: String?

}
