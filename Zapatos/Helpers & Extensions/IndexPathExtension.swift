//
//  IndexPathExtension.swift
//  Zain Saidin
//
//  Created by MacBook Air on 16/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

extension IndexPath {
    
    var tuple: (Int, Int) {
        get {
            return (section, row)
        }
}
}
