//
//  API.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 21/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON


enum APIError {
    case badRequest(String)
    case noInternetConnection
    case unauthorized
    case generic(String)
}

class API {
    class var authlessEndpoints: [String] {
        return ["auth/login", "auth/signup", "auth/facebook/login", "auth/facebook/signup", "auth/reset-password"]
        
    }

    class func makeGETRequest(to module: String, parameters: Parameters? = nil, completion: @escaping (JSON) -> (), failure: ((APIError) -> ())? = nil) {
        makeRequest(to: module, method: .get, parameters: parameters, completion: completion, failure: failure)
    }

    class func makePOSTRequest(to module: String, parameters: Parameters? = nil, completion: @escaping (JSON) -> (), failure: ((APIError) -> ())? = nil) {
        makeRequest(to: module, method: .post, parameters: parameters, completion: completion, failure: failure)
    }

    class func makeDELETERequest(to module: String, parameters: Parameters? = nil, completion: @escaping (JSON) -> (), failure: ((APIError) -> ())? = nil) {
        makeRequest(to: module, method: .delete, parameters: parameters, completion: completion, failure: failure)
    }

    class func makePUTRequest(to module: String, parameters: Parameters? = nil, completion: @escaping (JSON) -> (), failure: ((APIError) -> ())? = nil) {
        makeRequest(to: module, method: .put, parameters: parameters, completion: completion, failure: failure)
    }

    class func makeUploadRequest(to module: String, fileURL: URL, parameterName: String = "file", parameters: Parameters? = nil, completion: @escaping (JSON) -> (), failure: ((APIError) -> ())? = nil) {
        var appendedParameters: Parameters? = parameters

        if appendedParameters == nil {
            appendedParameters = [:]
        }

        if authlessEndpoints.contains(module) == false {
            appendedParameters!["access_token"] = kAppDelegate.user!.access_token!
        }

        let credentialData = "\(kAPIKey):\(kAPISecret)".data(using: .utf8, allowLossyConversion: false)
        let base64Credentials = credentialData?.base64EncodedString()
        let headers: HTTPHeaders = ["Authorization": "Basic \(base64Credentials!)"]

        Alamofire.upload(
            multipartFormData: { (formData) in
                formData.append(fileURL, withName: parameterName)

                if let appendedParameters = appendedParameters {
                    appendedParameters.forEach({ (key, value) in
                        let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue)!
                        formData.append(data, withName: key)
                    })
                }
            },
            to: "\(kBaseAPIURL)/\(module)",
            method: .post,
            headers: headers,
            encodingCompletion: { result in
                switch result {
                case .success(let request, _, _):
                    request.responseJSON(completionHandler: { (response) in
                        if let error = response.error {
                            if let failure = failure {
                                if error.localizedDescription == "The Internet connection appears to be offline." {
                                    failure(.noInternetConnection)
                                }
                                else {
                                    failure(.generic(error.localizedDescription))
                                }
                            }
                            else {
                                HUD.showError(withStatus: error.localizedDescription)
                            }

                            return
                        }

                        guard let resp = response.response else {
                            if let failure = failure {
                                failure(.generic(response.description))
                            }
                            else {
                                HUD.showError(withStatus: response.description)
                            }

                            return
                        }

                        switch resp.statusCode {
                        case 200:
                            completion(JSON(response.result.value!))

                        case 401:
                            if let failure = failure {
                                failure(.unauthorized)
                            }
                            else {
                                kAppDelegate.logout()
                            }

                        default:
                            if let value = response.result.value {
                                let json = JSON(value)
                                var errorString = ""

                                json["errors"].arrayValue.forEach({ (json) in
                                    errorString.append("\(json.stringValue)\n")
                                })
                                
                                errorString = errorString.substring(to: errorString.index(before: errorString.endIndex))

                                if let failure = failure {
                                    failure(.badRequest(errorString))
                                }
                                else {
                                    HUD.showError(withStatus: errorString)
                                }
                            }
                            else {
                                if let failure = failure {
                                    failure(.generic(response.description))
                                }
                                else {
                                    HUD.showError(withStatus: response.description)
                                }
                            }
                        }
                    })

                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        )
    }

    class func makeRequest(to module: String, method: HTTPMethod, parameters: Parameters?, completion: @escaping (JSON) -> (), failure: ((APIError) -> ())?) {
        var appendedParameters: Parameters? = parameters

        if appendedParameters == nil {
            appendedParameters = [:]
        }

        if authlessEndpoints.contains(module) == false {
            appendedParameters!["access_token"] = kAppDelegate.user!.access_token!
        }

        let credentialData = "\(kAPIKey):\(kAPISecret)".data(using: .utf8, allowLossyConversion: false)
        let base64Credentials = credentialData?.base64EncodedString()
        let headers: HTTPHeaders = ["Authorization": "Basic \(base64Credentials!)"]

        Alamofire.request(
            "\(kBaseAPIURL)/\(module)",
            method: method,
            parameters: appendedParameters,
            headers: headers
        ).responseJSON { (response) in
            print("\(kBaseAPIURL)/\("module")",appendedParameters!)
            if let error = response.error {
                if let failure = failure {
                    if error.localizedDescription == "The Internet connection appears to be offline." {
                        failure(.noInternetConnection)
                    }
                    else {
                        failure(.generic(error.localizedDescription))
                    }
                }
                else {
                    HUD.showError(withStatus: error.localizedDescription)
                }

                return
            }

            guard let resp = response.response else {
                if let failure = failure {
                    failure(.generic(response.description))
                }
                else {
                    HUD.showError(withStatus: response.description)
                }

                return
            }

            switch resp.statusCode {
            case 200:
                completion(JSON(response.result.value!))

            case 401:
                if let failure = failure {
                    failure(.unauthorized)
                }
                else {
                    kAppDelegate.logout()
                }

            default:
                if let value = response.result.value {
                    let json = JSON(value)
                    var errorString = ""

                    json["errors"].arrayValue.forEach({ (json) in
                        errorString.append("\(json.stringValue)\n")
                    })
                    
                    errorString = errorString.substring(to: errorString.index(before: errorString.endIndex))

                    if let failure = failure {
                        failure(.badRequest(errorString))
                    }
                    else {
                        HUD.showError(withStatus: errorString)
                    }
                }
                else {
                    if let failure = failure {
                        failure(.generic(response.description))
                    }
                    else {
                        HUD.showError(withStatus: response.description)
                    }
                }
            }
        }
    }
}
