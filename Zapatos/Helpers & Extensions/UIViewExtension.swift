//
//  UIViewExtension.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/5/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    var secondLastView: UIView {
        get {
            let count = subviews.count
            return subviews[count - 2]
        }
    }
    
    func previousView() -> UIView {
        let count = subviews.count
        return subviews[count - 2]
    }
}
