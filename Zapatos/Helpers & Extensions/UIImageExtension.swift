//
//  UIImageExtension.swift
//  Selklik Star
//
//  Created by Izad Che Muda on 6/29/16.
//  Copyright © 2016 Izad Che Muda. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    var pathAfterSaving: URL {
        get {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let time = Date().timeIntervalSince1970
            let fullPath = "\(path)/\(time).jpg"
            
            let size = UIImageJPEGRepresentation(self, 1)!.count
            var data: Data!
            
            if size > 400000 {
                data = UIImageJPEGRepresentation(self, 0.4)!
                print("Before \(size), After: \(data.count)")
            }
            else {
                data = UIImageJPEGRepresentation(self, 1)!
                print("Not compressed: \(size)")
            }
            
            FileManager.default.createFile(atPath: fullPath, contents: data, attributes: nil)
            
            return URL(fileURLWithPath: fullPath)
        }
    }
    
    func imageWithColor(_ color: UIColor) -> UIImage {       
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: size.height)
        context?.scaleBy(x: 1, y: -1)
        context?.setBlendMode(.normal)
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context?.clip(to: rect, mask: self.cgImage!)
        color.setFill()
        context?.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
 
}
