//
//  UIViewControllerExtension.swift
//  Selklik Star
//
//  Created by Izad Che Muda on 2/3/16.
//  Copyright © 2016 Izad Che Muda. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func addKeyboardDismisserTo(view: UIView) {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        recognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(recognizer)
    }
    
    @objc func dismissKeyboard(sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    func backButtonTapped(sender: UIBarButtonItem) {
        navigationController!.popViewController(animated: true)
    }
    
    func addTitlelessBackButton() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
  
  
}
