//
//  CustomAVPlayerViewController.swift
//  Siti
//
//  Created by Izad Che Muda on 14/03/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SnapKit

class CustomAVPlayerViewController: AVPlayerViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let label = UILabel()
//        label.text = "Taniah kerana membeli, \(kAppDelegate.user!.name)! ♥️ CT"
//        contentOverlayView!.addSubview(label)
//        label.textColor = UIColor.white
//        label.alpha = 0.5
//        label.snp.makeConstraints { (make) in
//           // make.width.equalTo(50)
//            make.height.equalTo(50)
//            make.centerX.equalToSuperview()
//            make.center.equalToSuperview().offset(100)
//        }
        let imageView = UIImageView(image: R.image.water_mark())
        imageView.alpha = 0.2
        contentOverlayView!.addSubview(imageView)
        
        imageView.snp.makeConstraints { (make) in
            make.width.equalTo(50)
            make.height.equalTo(50)
            make.center.equalToSuperview()
        }
    }
}
