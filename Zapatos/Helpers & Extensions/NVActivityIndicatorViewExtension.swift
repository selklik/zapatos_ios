//
//  ActivityIndicatorView.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 02/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

extension NVActivityIndicatorView {
    convenience init(superview: UIView) {
        let dimension: CGFloat = 30
        
        self.init(
            frame: CGRect(x: 0, y: 0, width: dimension, height: dimension),
            type: .ballSpinFadeLoader,
            color: UIColor(hexString: "#DE206A"),
            padding: nil
        )
        
        let bounds = UIScreen.main.bounds        
        center = CGPoint(x: bounds.width / 2, y: bounds.height / 2 - dimension)
        superview.addSubview(self)
    }
}
