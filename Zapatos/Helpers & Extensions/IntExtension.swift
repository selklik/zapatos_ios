//
//  IntExtension.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 25/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation

extension Int {
    var withComma: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.usesGroupingSeparator = true
        
        let stars = NSNumber(integerLiteral: self)
        return formatter.string(from: stars)!
    }
}
