//
//  NSDateExtension.swift
//  Selklik Star
//
//  Created by Izad Che Muda on 2/12/16.
//  Copyright © 2016 Izad Che Muda. All rights reserved.
//

import Foundation

extension Date {
    static func dateFromSQLString(_ dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        return dateFormatter.date(from: dateString)!
    }        
    
    var dateString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy h:mma"
        dateFormatter.timeZone = TimeZone.current
        
        return dateFormatter.string(from: self)
    }
}
