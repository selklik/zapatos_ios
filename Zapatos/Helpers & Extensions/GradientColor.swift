//
//  GradientColor.swift
//  Zapatos
//
//  Created by MacBook Air on 08/10/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func setGradientBackground(color1: UIColor, color2: UIColor,color3: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [color1.cgColor, color2.cgColor,color3.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
