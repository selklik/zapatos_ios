//
//  MyViewDelegate.swift
//  Zapatos
//
//  Created by MacBook Air on 03/01/2019.
//  Copyright © 2019 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol MyViewDelegate {
    func viewString() -> String;
}

class MyView : UIView {
    var myViewDelegate : MyViewDelegate?
    private var str : String?
    
    func reloadData() {
        if myViewDelegate != nil {
            str = myViewDelegate!.viewString()
        }
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        UIColor.white.setFill()
        UIRectFill(self.bounds)
        if str != nil {
            let ns = str! as NSString
            ns.draw(in: self.bounds, withAttributes:  [NSAttributedStringKey.foregroundColor: UIColor.blue, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 10)])
            
        }
    }
}

