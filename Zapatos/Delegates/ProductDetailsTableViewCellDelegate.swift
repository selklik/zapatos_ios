//
//  ProductDetailsTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 02/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol ProductDetailsTableViewCellDelegate {
    func productDetailsTableViewCell(cell: ProductDetailsTableViewCell, didTapProductImageFor product: Product)
}
