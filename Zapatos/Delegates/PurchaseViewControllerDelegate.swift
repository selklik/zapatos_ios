//
//  PurchaseViewControllerDelegate.swift
//  Zain Saidin
//
//  Created by MacBook Air on 12/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol PurchaseViewControllerDelegate {
    
    func purchaseViewController(controller: PurchaseViewController, didPurchasedPost post: Post, withPurchase purchase: Purchase)
    
}

