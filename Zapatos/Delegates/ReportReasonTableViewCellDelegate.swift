//
//  ReportReasonTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol ReportReasonTableViewCellDelegate {
    
    func reportReasonTableViewCell(cell: ReportReasonTableViewCell, didTypeReason reason: String?)
    
}
