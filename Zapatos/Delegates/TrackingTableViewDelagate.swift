
//
//  TrackingTableViewDe,e.swift
//  Zapatos
//
//  Created by MacBook Air on 18/01/2019.
//  Copyright © 2019 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol TrackingTableViewDelagate {
    
      func trackTapped(cell: OrderTableViewCell, didTapBuyButton button: UIButton)
   
}
