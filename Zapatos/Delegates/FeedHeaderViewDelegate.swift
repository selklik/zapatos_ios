//
//  FeedHeaderViewDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol FeedHeaderViewDelegate {
    func feedHeaderView(view: FeedHeaderView, didTapButton button: UIButton)
}
