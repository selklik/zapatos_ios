//
//  ReloadTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by MacBook Air on 12/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//
import Foundation
import UIKit
import StoreKit

protocol ReloadTableViewCellDelegate {
    func reloadTableViewCell(cell: ReloadTableViewCell, didTapBuyButton button: UIButton, forProduct product: SKProduct)
}
