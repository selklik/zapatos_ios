//
//  ReportBlockTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

protocol ReportBlockTableViewCellDelegate {
    
    func reportBlockTableViewCell(cell: ReportBlockTableViewCell, didSetBlock block: Bool)
    
}
