//
//  RecentPostsTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol RecentPostsTableViewCellDelegate {
    func recentPostsTableViewCell(cell: RecentPostsTableViewCell, didSelectPost post: Shoes)
}
