//
//  AddAddressViewControllerDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation

protocol AddAddressViewControllerDelegate {
    func addAddressViewController(controller: AddAddressViewController, didCreateAddress address: Address)
}
