//
//  CardsViewControllerDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol CardsViewControllerDelegate {
    func cardsViewController(controller: CardsViewController, didSetCardAsDefault card: Card)
}
