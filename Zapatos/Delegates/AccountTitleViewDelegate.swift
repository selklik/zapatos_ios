//
//  AccountTitleViewDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 25/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol AccountTitleViewDelegate {
    func accountTitleView(view: AccountTitleView, didTapButton button: UIButton)
}
