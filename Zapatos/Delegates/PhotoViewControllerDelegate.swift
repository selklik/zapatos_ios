//
//  PhotoViewControllerDelegate.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation

import Foundation
import UIKit

protocol PhotoViewControllerDelegate {
    
    func photoViewController(controller: PhotoViewController, didUpdatePost post: Post)
    func photoViewController(controller: PhotoViewController, didTapLikeLabelButton button: UIButton, forPost post: Post)
    func photoViewController(controller: PhotoViewController, didTapCommentButton button: UIButton, forPost post: Post)
    func photoViewController(controller: PhotoViewController, didTapCommentLabelButton button: UIButton, forPost post: Post)
    
}
