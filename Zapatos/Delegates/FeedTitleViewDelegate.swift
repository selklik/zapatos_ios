//
//  FeedTitleViewDelegate.swift
//  Zain Saidin
//
//  Created by MacBook Air on 21/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol FeedTitleViewDelegate {
    func feedTitleView(view: FeedTitleView, didTapButton button: UIButton)
}
