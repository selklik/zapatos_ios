//
//  FeedTableViewCellDelegate.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/6/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import Foundation
import UIKit

protocol FeedTableViewCellDelegate {
    
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapArtist artist: Artist)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTryToOpenURL url: URL)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapLikeButtonOnPost post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapLikeLabelButton button: UIButton, onPost post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapSocialMediaButtonOnPost post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapPhotoImageView post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapCommentButton button: UIButton, onPost post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapCommentLabelButton button: UIButton, onPost post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapOptionsButton button: UIButton, onPost post: Post)
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapQnaPlayButton button: UIButton, onPost post: Post)
    
}
