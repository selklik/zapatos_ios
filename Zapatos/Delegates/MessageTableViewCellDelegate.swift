//
//  MessageTableViewCellDelegate.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/16/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import Foundation
import UIKit

protocol MessageTableViewCellDelegate {
    
    func messageTableViewCell(cell: MessageTableViewCell, didTapPlayButton: UIButton, forAnswer answer: Message)
    
}
