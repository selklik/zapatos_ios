//
//  NotAddedTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 26/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol NotAddedTableViewCellDelegate {
    func notAddedTableViewCell(cell: NotAddedTableViewCell, didTapButton button: UIButton, forType type: NotAddedTableViewCellType)
}
