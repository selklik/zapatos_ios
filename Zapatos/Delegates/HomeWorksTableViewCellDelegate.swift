//
//  HomeWorksTableViewCellDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

protocol HomeWorksTableViewCellDelegate {
    func homeWorksTableViewCell(cell: HomeWorksTableViewCell, didSelectWork work: Work)
}
