//
//  AppDelegate.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 16/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SwiftyJSON
import FBSDKCoreKit
import Stripe
import OneSignal
import Siren
import PMAlertController
import FacebookCore
import Instabug

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var oneSignalUserID: String?
    
    var user: User? {
        didSet {
            user?.save()
            
        }
    }
    
    var shoes: Shoes?
    var artist: Artist?
    var about: About?
    var datas: Datas?
    

    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        return true
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window!.makeKeyAndVisible()
        user = User.load()
      
        STPPaymentConfiguration.shared().publishableKey = kStripePublishableKey
        
       
            showAuthStoryboard()
        
       // Instabug.start(withToken: "ce827e989868339f55fa4ddf3cd362ee", invocationEvents: [.shake, .screenshot])
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        setupSiren()
        return true;
    }
    
    
    func setupSiren() {
        let siren = Siren.shared
        
        // Optional
        siren.delegate = self
        
        // Optional
        siren.debugEnabled = true
      

       
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        print(url)
        print(url.host!)
        print(url.path)
       
        if url.host!.contains("order-paid") {
            let id = Int(url.path.replacingOccurrences(of: "/", with: ""))!
            
            API.makeGETRequest(
                to: "me/orders",
                parameters: ["artist_id": kAppDelegate.artist!.id],
                completion: { json in
                    let order = json.arrayValue.map({ (json) -> Order in
                        return Order(json)
                    }).filter({ (o) -> Bool in
                        return true
                    }).first
                    
                    if order != nil {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderCreated"), object: order!)
                    }
                    else {
                        print("failed to fetch order!")
                    }
                }
            )
            // 3b874ffa-2b0a-47ba-834a-a934c90054bc
            Siren.shared.checkVersion(checkType: .immediately)
            return false
        }
        else if url.host!.contains("order-unpaid") {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "orderNotCreated"), object: nil)
            return false
        }
        else {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        AppEventsLogger.activate(application)
        Siren.shared.checkVersion(checkType: .immediately)
        print(AppEventsLogger.activate(application),"appx")
    }
    
    func showAuthStoryboard() {
        window?.rootViewController = R.storyboard.tab().instantiateInitialViewController() as! TabBarController
    }

    func showLoadingStoryboard(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        UserDefaults.standard.set(true, forKey: "Auth")
        OneSignal.initWithLaunchOptions(
            launchOptions,
            appId: kOneSignalAppID,
            handleNotificationReceived: { notification in
                // App open
                print("handleNotificationReceived")
                
                //guard let notification = notification else { return }
                //self.handle(notification: notification)
            },
            handleNotificationAction: { result in
                // App in background
                print("handleNotificationAction")
                guard let result = result, let notification = result.notification else { return }
                let json = JSON(notification.payload.additionalData)
                
                if let id = json["post_id"].int {
                    API.makeGETRequest(
                        to: "artists/\(kArtistID)/posts/\(id)",
                        completion: { json in
                            guard let tbc = self.window!.rootViewController!.presentedViewController as? TabBarController else {
                                return
                            }
                                                        
                            let post = Post(json)
                            let feedVC = R.storyboard.feed.feedViewController()!
                            feedVC.posts = [post]
                            feedVC.isSingle = true
                            feedVC.isModal = true
                            
                            let nc = NavigationController(rootViewController: feedVC)
                            tbc.present(nc, animated: true, completion: nil)
                        }
                    )
                }
            },
            settings: [kOSSettingsKeyInAppAlerts: false]
        )
        
        OneSignal.idsAvailable { (userID, _) in
            guard let userID = userID else { return }
            self.oneSignalUserID = userID
            
            API.makePUTRequest(
                to: "me",
                parameters: ["device_id": userID],
                completion: { json in
                    print("OneSignal User ID: \(userID)")
                    self.user = User(json)
                    
                }
            )
        }
                Siren.shared.checkVersion(checkType: .immediately)
        window!.rootViewController = R.storyboard.tab().instantiateInitialViewController()
    }
    
    func logout() {
        let domainName = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domainName)
        
        user = nil
        showAuthStoryboard()
    }
    
    
}
extension AppDelegate: SirenDelegate
{
    func sirenDidShowUpdateDialog(alertType: Siren.AlertType) {
        print(#function, alertType)
    }
    
    func sirenUserDidCancel() {
        print(#function)
    }
    
    func sirenUserDidSkipVersion() {
        print(#function)
    }
    
    func sirenUserDidLaunchAppStore() {
        print(#function)
    }
    
    func sirenDidFailVersionCheck(error: Error) {
        print(#function, error)
    }
    
    func sirenLatestVersionInstalled() {
        print(#function, "Latest version of app is installed")
    }
    
    // This delegate method is only hit when alertType is initialized to .none
    func sirenDidDetectNewVersionWithoutAlert(message: String, updateType: UpdateType) {
        print(#function, "\(message).\nRelease type: \(updateType.rawValue.capitalized)")
    }
}
