//
//  Constants.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 21/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit

let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let kScreenWidth = UIScreen.main.bounds.size.width
let kScreenHeight = UIScreen.main.bounds.size.height
let kBaseAPIURL = "https://myyb2.mobsocial.net/fan-api/1.0"
let kZapatosURL = "http://shoplot.biz/zapatos-admin/pages"
let kAPIKey = "990030838451504"
let kAPISecret = "a7a022dcbd159b405b13c21cf280c66a"
let kArtistFullName = "Zain Saidin"
let kArtistID = "3277,256"
let kArtistShortName = "Zain"
let kArtistFBName = "Zain Saidin"
let kViewHeaderHeight: CGFloat = 50
let kAuthBorderColor = UIColor(hexString: "#99A9B3", alpha: 0.5)!.cgColor
let kOneSignalAppID = "2479a966-f09b-425c-bc6c-2b105a04ea34"
//let kStripePublishableKey = "pk_test_O2CtbTj29L33SKIAtkQ4gAU0"
let kStripePublishableKey = "pk_live_Qs4OjvJhy9Tx6ofILcVMVgZy"
let kTextColor = UIColor(hexString: "#1BBFE8")!
let kBackgroundColor = UIColor(hexString: "#F1F1F1")
let kSecondaryTextColor = UIColor(hexString: "#99A9B4")
let kButtonColor = UIColor(hexString: "#5B8C5A")
let kTabColor = UIColor.white
let zapatosColor = UIColor(hexString: "#FBC254")
enum HomeViewControllerSection {
    case header
    case works
    case blankCell
    case socialAccounts
    case headerBottom
    case sectionHeader1
    case recentPosts
    case sectionHeader2
    case featuredProducts
    case poweredBy
    case simplysiti
}

enum SocialNetworkAccountType {
    case facebook
    case twitter
    case instagram
    case youtube
}

enum SectionHeaderType {
    case recentPosts
    case featuredProducts
    case collections
    case cards
    case addresses
}

enum CategoryViewControllerSection {
    case photo
    case products
}

enum ShoppingCartViewMode {
    case dark
    case light
}

enum CartViewControllerSection {
    case sectionHeaderProducts
    case products
    case sectionHeaderCard
    case card
    case sectionHeaderAddress
    case address
}

enum NotAddedTableViewCellType {
    case card
    case address
}

enum SectionHeaderTableViewCellType {
    case manageCards
    case manageAddresses
}
struct Colors {
    
    static let zapatosColor     = UIColor(hexString: "#FBC254")!.cgColor
    static let red              = UIColor(red: 255.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
    static let orange           = UIColor(red: 255.0/255.0, green: 175.0/255.0, blue: 72.0/255.0, alpha: 1.0)
    static let blue             = UIColor(red: 74.0/255.0, green: 144.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let green            = UIColor(red: 91.0/255.0, green: 197.0/255.0, blue: 159.0/255.0, alpha: 1.0)
    static let darkGrey         = UIColor(red: 85.0/255.0, green: 85.0/255.0, blue: 85.0/255.0, alpha: 1.0)
    static let veryDarkGrey     = UIColor(red: 13.0/255.0, green: 13.0/255.0, blue: 13.0/255.0, alpha: 1.0)
    static let lightGrey        = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0)
    static let black            = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let white            = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
}
