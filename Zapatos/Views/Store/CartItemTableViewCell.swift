//
//  CartItemTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 03/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import DropDown


class CartItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ColorLabel: UILabel!
    @IBOutlet weak var SizeLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    var product: OrderList!
    var Color: String!
    var Size: String!
    let chooseDropDownForSize = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            
            self.chooseDropDownForSize,
            
            ]
    }()
    override func awakeFromNib() {
        super.awakeFromNib()
        ColorLabel.layer.cornerRadius = ColorLabel.frame.height/2
        SizeLabel.layer.cornerRadius = SizeLabel.frame.height/2
        ColorLabel.textColor = zapatosColor
        SizeLabel.textColor = zapatosColor
        
        photoImageView.layer.cornerRadius = 3
    }

    func configure(product: OrderList) {
        self.product = product        
        photoImageView.image = nil
        let url = URL(string: (product.product_imag)!)!
        photoImageView.af_setImage(withURL: url)
        SizeLabel.text = product.product_size
        titleLabel.text = product.product_name
        priceLabel.text = product.product_price
        ColorLabel.text = product.product_color
        
       
    }        
   
    
  
  
}
