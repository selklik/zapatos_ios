//
//  CardTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 03/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {
    var isListMode = false

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(card: Card, isListMode: Bool = false) {
        self.isListMode = isListMode
        selectionStyle = .none

        contentView.subviews.forEach { view in
            view.removeFromSuperview()
        }

        let brandImageView = UIImageView()
        if let image = UIImage(named: card.brand) {
            brandImageView.image = image
        }
        else if card.kind == "fpx" {
            brandImageView.image = R.image.fPX()
        }
        else {
            brandImageView.image = R.image.cardDefault()
        }

        contentView.addSubview(brandImageView)

        brandImageView.snp.makeConstraints { (make) in
            make.width.equalTo(44)
            make.height.equalTo(31)
            make.top.equalToSuperview().offset(15)
            make.left.equalToSuperview().offset(15)
            make.bottom.equalToSuperview().offset(-15)
        }
        
        if card.kind == "card" {
            let last4Label = UILabel()
            last4Label.font = R.font.openSans(size: 16)
            last4Label.textColor = UIColor(hexString: "#333333")
            last4Label.text = card.last4
            contentView.addSubview(last4Label)
            
            last4Label.snp.makeConstraints { (make) in
                make.centerY.equalTo(brandImageView)
                
                if !isListMode {
                    make.right.equalToSuperview().offset(-15)
                }
            }
            
            (0...12).forEach { (index) in
                let dotImageView = UIImageView(image: R.image.cardDot())
                contentView.addSubview(dotImageView)
                
                dotImageView.snp.makeConstraints({ (make) in
                    make.centerY.equalTo(last4Label)
                    make.width.equalTo(4)
                    make.height.equalTo(4)
                    
                    switch index {
                    case 0:
                        make.right.equalTo(last4Label.snp.left).offset(-15)
                    case 4, 8:
                        make.right.equalTo(contentView.secondLastView.snp.left).offset(-15)
                    case 11:
                        if isListMode {
                            make.left.equalTo(brandImageView.snp.right).offset(30)
                        }
                        
                        make.right.equalTo(contentView.secondLastView.snp.left).offset(-6)
                    default:
                        make.right.equalTo(contentView.secondLastView.snp.left).offset(-6)
                    }
                })
            }
            
            if isListMode {
                if card.is_default {
                    let defaultContainerView = UIView()
                    contentView.addSubview(defaultContainerView)
                    
                    defaultContainerView.snp.makeConstraints({ (make) in
                        make.width.equalTo(70)
                        make.top.equalTo(last4Label).offset(-4)
                        make.right.equalToSuperview()
                        make.bottom.equalToSuperview()
                    })
                    
                    let defaultImageView = UIImageView(image: R.image.tick())
                    defaultContainerView.addSubview(defaultImageView)
                    
                    defaultImageView.snp.makeConstraints({ (make) in
                        make.top.equalToSuperview()
                        make.centerX.equalToSuperview()
                        make.width.equalTo(16)
                        make.height.equalTo(16)
                    })
                    
                    let defaultLabel = UILabel()
                    defaultLabel.text = "Default"
                    defaultLabel.textColor = kSecondaryTextColor
                    defaultLabel.font = R.font.openSansSemibold(size: 10)
                    defaultContainerView.addSubview(defaultLabel)
                    
                    defaultLabel.snp.makeConstraints({ (make) in
                        make.centerX.equalToSuperview()
                        make.top.equalTo(defaultImageView.snp.bottom).offset(4)
                    })
                }
            }
        }
        else if card.kind == "fpx" {
            let brandLabel = UILabel()
            brandLabel.font = R.font.openSans(size: 16)
            brandLabel.textColor = UIColor(hexString: "#333333")
            brandLabel.text = card.brand
            contentView.addSubview(brandLabel)
            
            brandLabel.snp.makeConstraints({ (make) in
                make.centerY.equalToSuperview()
                
                if isListMode {
                    make.left.equalTo(brandImageView.snp.right).offset(15)
                }
                else {
                    make.right.equalToSuperview().offset(-15)
                }
            })
            
            if isListMode {
                if card.is_default {
                    let defaultContainerView = UIView()
                    contentView.addSubview(defaultContainerView)
                    
                    defaultContainerView.snp.makeConstraints({ (make) in
                        make.width.equalTo(70)
                        make.top.equalTo(brandLabel).offset(-4)
                        make.right.equalToSuperview()
                        make.bottom.equalToSuperview()
                    })
                    
                    let defaultImageView = UIImageView(image: R.image.tick())
                    defaultContainerView.addSubview(defaultImageView)
                    
                    defaultImageView.snp.makeConstraints({ (make) in
                        make.top.equalToSuperview()
                        make.centerX.equalToSuperview()
                        make.width.equalTo(16)
                        make.height.equalTo(16)
                    })
                    
                    let defaultLabel = UILabel()
                    defaultLabel.text = "Default"
                    defaultLabel.textColor = kSecondaryTextColor
                    defaultLabel.font = R.font.openSansSemibold(size: 10)
                    defaultContainerView.addSubview(defaultLabel)
                    
                    defaultLabel.snp.makeConstraints({ (make) in
                        make.centerX.equalToSuperview()
                        make.top.equalTo(defaultImageView.snp.bottom).offset(4)
                    })
                }
            }
        }
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor(hexString: "#eeeeee")
        contentView.addSubview(separatorView)
        
        separatorView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(1)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if isListMode {
            if selected {
                backgroundColor = UIColor(hexString: "#f8f8f8")
            }
            else {
                backgroundColor = .white
            }
        }
    }
}
