//
//  ProductTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class ProductTableViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageOutlet: UIImageView!
    
    var singleProduct: Shoes!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageOutlet.layer.cornerRadius = 15
        photoImageView.layer.cornerRadius = 3
        imageOutlet.image = imageOutlet.image!.withRenderingMode(.alwaysTemplate)
        imageOutlet.tintColor = UIColor(hex: "#FBC254")
    }
    
    func configure(product: Shoes) {
        
        photoImageView.image = nil
        let url = URL(string: (product.products.first?.prod_img)!)!
        photoImageView.af_setImage(withURL: url)
        titleLabel?.text = product.prod_name
        priceLabel?.text = product.price
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            backgroundColor = UIColor(hexString: "#f8f8f8")
        }
        else {
            backgroundColor = .white
        }
    }
}
