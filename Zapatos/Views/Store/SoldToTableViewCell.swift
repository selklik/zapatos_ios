//
//  SoldToTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 04/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class SoldToTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var purchasetimeStamp: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profilePhotoImageView.layer.cornerRadius = 20
    }
    
    func configure(user: User) {
       
        purchasetimeStamp.text = nil
        nameLabel.text = user.name
        profilePhotoImageView.af_setImage(withURL: user.profile_photos.full_resolution)
    }
}
