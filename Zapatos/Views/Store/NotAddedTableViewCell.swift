//
//  NotAddedTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 26/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class NotAddedTableViewCell: UITableViewCell {
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    var delegate: NotAddedTableViewCellDelegate!
    var type: NotAddedTableViewCellType!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        button.layer.cornerRadius = 3
    }
    
    func configure(delegate: NotAddedTableViewCellDelegate, type: NotAddedTableViewCellType, image: UIImage, description: String, buttonTitle: String) {
        self.delegate = delegate
        self.type = type
        
        iconImageView.image = image
        descLabel.text = description
        button.setTitle(buttonTitle, for: .normal)
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        delegate.notAddedTableViewCell(cell: self, didTapButton: sender, forType: type)
    }
}
