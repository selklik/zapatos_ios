//
//  CategoryTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 30/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    
    func configure(category: ProductCategory) {
        photoImageView.af_setImage(withURL: category.photo!.full_resolution)
    }
}
