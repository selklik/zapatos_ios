//
//  AddressTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 03/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!    
    @IBOutlet weak var defaultContainerView: UIView!
    var isListMode = false
    
    override func awakeFromNib() {
        super.awakeFromNib()        
    }

    func configure(address: Address, isListMode: Bool = false) {
        self.isListMode = isListMode
        addressLabel.text = "\(address.address.replacingOccurrences(of: ",", with: "\n"))"
        
        print(address.is_default,"is_default")
        
        if isListMode {
            if address.is_default == "1" {
                defaultContainerView.alpha = 1
                separatorView.alpha = 1
            }
            else {
                separatorView.alpha = 0
                defaultContainerView.alpha = 0
            }
        }
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if isListMode {
            if selected {
                backgroundColor = UIColor(hexString: "#f8f8f8")
            }
            else {
                backgroundColor = .white
            }
        }
    }
}
