//
//  ProductDetailsTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 02/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class ProductDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!    
    @IBOutlet weak var imagesLabel: UILabel!
    var product: Product!
    var delegate: ProductDetailsTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()                
    }

    func configure(product: Product, delegate: ProductDetailsTableViewCellDelegate) {
        self.product = product
        self.delegate = delegate
        
        photoImageView.af_setImage(withURL: product.photos.first!.full_resolution)
        titleLabel.text = product.title
        priceLabel.text = product.price
        detailsLabel.text = product.details
        viewCountLabel.text = "\(product.view_count) views"
        
        if product.photos.count == 1 {
            imagesLabel.text = "1 image"
        }
        else {
            imagesLabel.text = "\(product.photos.count) images"
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        delegate.productDetailsTableViewCell(cell: self, didTapProductImageFor: product)
    }
}
