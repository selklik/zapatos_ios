//
//  FakeProductTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 30/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class FakeProductTableViewCell: UITableViewCell {
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var countView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        photoView.layer.cornerRadius = 3
        countView.layer.cornerRadius = 7.5
    }
}
