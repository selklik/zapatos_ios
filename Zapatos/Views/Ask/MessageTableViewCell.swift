//
//  MessageTableViewCell.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/16/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import SnapKit
import DateTools

class MessageTableViewCell: UITableViewCell {
    var question: Message!
    var answer: Message?
    var questionLabel: UILabel!
    var screenshotImageView: UIImageView!
    var delegate: MessageTableViewCellDelegate!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        selectionStyle = .none
    }

    func configure(delegate: MessageTableViewCellDelegate, question: Message, answer: Message?) {
        self.delegate = delegate
        self.question = question
        self.answer = answer
        
        backgroundColor = kBackgroundColor
        
        contentView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.clipsToBounds = true
        
        contentView.addSubview(containerView)
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(7.5)
            make.left.equalTo(contentView)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView).offset(-7.5)
        }
        
        
        questionLabel = UILabel()
        questionLabel.text = question.question
        questionLabel.numberOfLines = 0
        questionLabel.textColor = UIColor(hexString: "#485661")
        questionLabel.font = R.font.openSans(size: 15)
        
        containerView.addSubview(questionLabel)
        
        questionLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
        }
        
        
        let answeredImageView = UIImageView()
        
        if answer == nil {
            answeredImageView.image = R.image.messagesNotAnswered()
        }
        else {
            answeredImageView.image = R.image.messagesAnswered()
        }
        
        containerView.addSubview(answeredImageView)
        
        answeredImageView.snp.makeConstraints { (make) in
            make.width.equalTo(10)
            make.height.equalTo(10)
            make.left.equalTo(questionLabel)
            make.top.equalTo(questionLabel.snp.bottom).offset(8)
        }
        
        
        let infoLabel = UILabel()
        infoLabel.textColor = UIColor(hexString: "#999999")
        infoLabel.font = R.font.openSans(size: 11)
        
        let asked = (Date.dateFromSQLString(question.timestamp) as NSDate).timeAgoSinceNow()!
        
        if let answer = answer {
            let answered = (Date.dateFromSQLString(answer.timestamp) as NSDate).timeAgoSinceNow()!
            infoLabel.text = "Asked \(asked)  ·  Answered \(answered)"
        }
        else {
            infoLabel.text = "Asked \(asked)  ·  Waiting for answer"
        }
        
        containerView.addSubview(infoLabel)
        
        infoLabel.snp.makeConstraints { (make) in
            make.left.equalTo(answeredImageView.snp.right).offset(8)
            make.centerY.equalTo(answeredImageView)
        }
        
        if let answer = answer {
            screenshotImageView = UIImageView()
            screenshotImageView.image = nil
            screenshotImageView.backgroundColor = .groupTableViewBackground
            screenshotImageView.af_setImage(withURL: answer.answer!.screenshots.full_resolution)
            
            containerView.addSubview(screenshotImageView)
            
            screenshotImageView.snp.makeConstraints({ (make) in
                make.left.equalTo(questionLabel)
                make.right.equalTo(questionLabel)
                make.top.equalTo(answeredImageView.snp.bottom).offset(20)
                make.height.equalTo(kScreenWidth - 30)
            })
            
            
            let playButton = UIButton()
            playButton.alpha = 0.9
            playButton.setImage(R.image.feedPlay(), for: .normal)
            playButton.addTarget(self, action: #selector(playButtonTapped(_:)), for: .touchUpInside)
            
            containerView.addSubview(playButton)
            
            playButton.snp.makeConstraints({ (make) in
                make.width.equalTo(80)
                make.height.equalTo(80)
                make.center.equalTo(screenshotImageView)
            })
        }
        
        
        let borderView = UIView()
        borderView.backgroundColor = UIColor(hexString: "#fcfcfc")
        
        containerView.addSubview(borderView)
        
        borderView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.equalToSuperview().offset(2)
            make.right.equalToSuperview().offset(-2)
            make.bottom.equalToSuperview()
            
            if answer != nil {
                make.top.equalTo(screenshotImageView.snp.bottom).offset(20)
            }
            else {
                make.top.equalTo(answeredImageView.snp.bottom).offset(20)
            }
        }
    }
    
    @objc func playButtonTapped(_ sender: UIButton) {
        delegate.messageTableViewCell(cell: self, didTapPlayButton: sender, forAnswer: answer!)
    }

}
