//
//  AskTutorialView.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 03/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class AskTutorialView: UIView {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!    
    
    func configure(image: UIImage, description: NSMutableAttributedString) {
        imageView.image = image
        containerView.layer.cornerRadius = 6
        containerView.layer.borderWidth = 1
        containerView.layer.borderColor = UIColor.gray.cgColor
        containerView.clipsToBounds = true
        descLabel.attributedText = description
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
        containerView.layer.shadowOpacity = 0.1
    }
}
