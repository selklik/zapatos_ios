//
//  ItemCell.swift
//  GridViewExampleApp
//
//  Created by Chandimal, Sameera on 12/22/17.
//  Copyright © 2017 Pearson. All rights reserved.
//

import UIKit
import AlamofireImage

class ItemCell: UICollectionViewCell {
      var product: Shoes!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        priceLabel.layer.cornerRadius = priceLabel.frame.size.height / 2
    }

    func setData(pro: Shoes) {
        productImage.image = nil
        let url = URL(string: (pro.products.first?.prod_img)!)!
        productImage.af_setImage(withURL: url)
        textLabel?.text = pro.prod_name
        priceLabel?.text = "RM \(pro.price)"
        
    }
}
