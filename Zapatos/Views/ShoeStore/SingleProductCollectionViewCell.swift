//
//  SingleProductCollectionViewCell.swift
//  Zapatos
//
//  Created by MacBook Air on 01/10/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class SingleProductCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedImage.image = selectedImage.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        selectedImage.tintColor = zapatosColor
        
        
    }


    func setData(singleProduct: String) {
        imageView.image = nil
        let url = URL(string: singleProduct)
        imageView.af_setImage(withURL: url!)
        
    }
}
