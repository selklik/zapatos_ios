//
//  EditProfileTableViewCell.swift
//  Zain Saidin
//
//  Created by MacBook Air on 20/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleNameLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configure(user: User, row: Int) {

        switch row {
        case 0:
            titleLabel.text = "Full Name"
            titleNameLabel.text = kAppDelegate.user!.name
        case 1:
            titleLabel.text = "Password"
            titleNameLabel.text = "********"
        default:
            break
        }
    }
  
}
