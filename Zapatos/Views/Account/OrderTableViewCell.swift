//
//  OrderTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var quntityLabel: UILabel!
    @IBOutlet weak var orderNoLabel: UILabel!
    @IBOutlet weak var oderDateLabel: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var tracking: UIButton!
    @IBOutlet weak var trakingLabel: UILabel!
    
    var delegate: TrackingTableViewDelagate!

    override func awakeFromNib() {
        super.awakeFromNib()
         tracking.isHidden = true
         trakingLabel.isHidden = true
        sizeLabel.layer.cornerRadius = 8
        photoImageView.layer.cornerRadius = 3
        tracking.layer.cornerRadius = tracking.frame.size.height/2
        totalAmount.layer.cornerRadius = totalAmount.frame.size.height/2
        
    }
    
    func configure(delegate: TrackingTableViewDelagate, order: Order) {
        
        self.delegate = delegate
        let url = URL(string: (order.prod_image))!
        photoImageView.af_setImage(withURL: url)
        titleLabel.text = order.prod_name
        sizeLabel.text = order.cus_sizes
        quntityLabel.text = order.cus_prodquantity
        orderNoLabel.text = order.cus_orderid
        oderDateLabel.text = order.date
        let double = Double(order.totalAmount)
        totalAmount.text = "RM \(double! / 100)"
        
        if order.trackid.isEmpty {
            tracking.isHidden = true
            trakingLabel.isHidden = true
        } else {
             tracking.isHidden = false
            trakingLabel.isHidden = false
             UserDefaults.standard.set(order.trackid, forKey: "trackid")
        }
       
    }

    @IBAction func tracking(_ sender: UIButton) {
        print(sender.tag)
        delegate.trackTapped(cell: self, didTapBuyButton: sender)
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            backgroundColor = UIColor(hexString: "#f8f8f8")
        }
        else {
            backgroundColor = .white
        }
    }
}
