//
//  ReloadTableViewCell.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/17/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import StoreKit

class ReloadTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var buyButton: UIButton!
    var delegate: ReloadTableViewCellDelegate!
    var product: SKProduct!

    override func awakeFromNib() {
        super.awakeFromNib()
        buyButton.clipsToBounds = true
        buyButton.layer.cornerRadius = 3        
    }

    func configure(delegate: ReloadTableViewCellDelegate, product: SKProduct) {
        self.delegate = delegate
        self.product = product
        
        nameLabel.text = product.localizedTitle
        
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = product.priceLocale
        
        let priceString = numberFormatter.string(from: product.price)
        buyButton.setTitle(priceString, for: .normal)        
    }
    
    @IBAction func buyButtonTapped(_ sender: UIButton) {        
        delegate.reloadTableViewCell(cell: self, didTapBuyButton: sender, forProduct: product)
    }
}
