//
//  InstructionTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 25/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class InstructionTableViewCell: UITableViewCell {
    @IBOutlet weak var instructionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    func configure(attributedText: NSMutableAttributedString) {
        instructionLabel.attributedText = attributedText
    }
}
