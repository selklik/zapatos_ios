//
//  HistoryTableViewCell.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/17/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import SnapKit

class HistoryTableViewCell: UITableViewCell {
    func configure(transaction: Transaction) {
        selectionStyle = .none

        contentView.subviews.forEach { view in
            view.removeFromSuperview()
        }

        let refLabel = UILabel()
        refLabel.text = transaction.reference_id
        refLabel.textColor = UIColor(hexString: "#1a2531")
        refLabel.font = R.font.openSansSemibold(size: 14)

        contentView.addSubview(refLabel)

        refLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(15)

            switch transaction.type {
            case "premium-post-purchase":
                make.left.equalToSuperview().offset(70)

            default:
                make.left.equalToSuperview().offset(15)
            }

            make.right.equalToSuperview().offset(-15)
        }

        let date = Date.dateFromSQLString(transaction.timestamp)
        
        let timestampLabel = UILabel()        
        timestampLabel.text = date.dateString
        timestampLabel.font = R.font.openSans(size: 10)
        timestampLabel.textColor = UIColor(hexString: "#999999")

        contentView.addSubview(timestampLabel)

        timestampLabel.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-15)
            make.top.equalTo(refLabel)
        }


        if transaction.type == "premium-post-purchase" {
            let photoImageView = UIImageView()
            photoImageView.backgroundColor = .groupTableViewBackground
            photoImageView.image = nil

            if let post = transaction.post {
                photoImageView.af_setImage(withURL: post.photos.first!.versions.thumbnail!)
            }

            contentView.addSubview(photoImageView)

            photoImageView.snp.makeConstraints({ (make) in
                make.width.equalTo(40)
                make.height.equalTo(40)
                make.top.equalTo(refLabel)
                make.left.equalToSuperview().offset(15)
            })
        }


        let typeLabel = UILabel()
        typeLabel.text = "TYPE:"
        typeLabel.font = R.font.openSans(size: 10)
        typeLabel.textColor = UIColor(hexString: "#999999")

        contentView.addSubview(typeLabel)

        typeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(refLabel)
            make.top.equalTo(refLabel.snp.bottom).offset(4)
        }


        let typeValueLabel = UILabel()

        switch transaction.type {
        case "balance-topup":
            typeValueLabel.text = "Stars Reload"

        case "premium-post-purchase":
            typeValueLabel.text = "Premium Post"

        case "question-purchase":
            typeValueLabel.text = "Question (Q&A)"

        case "answer-purchase":
            typeValueLabel.text = "Answer (Q&A)"

        case "free-stars":
            typeValueLabel.text = "Free Stars"
            
        default:
            break
        }


        typeValueLabel.font = R.font.openSansSemibold(size: 10)
        typeValueLabel.textColor = UIColor(hexString: "#444444")

        contentView.addSubview(typeValueLabel)

        typeValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(typeLabel)
            make.left.equalTo(typeLabel.snp.right).offset(4)
        }


        let valueLabel = UILabel()
        valueLabel.text = "VALUE:"
        valueLabel.font = R.font.openSans(size: 10)
        valueLabel.textColor = UIColor(hexString: "#999999")

        contentView.addSubview(valueLabel)

        valueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(typeValueLabel)
            make.left.equalTo(typeValueLabel.snp.right).offset(18)
        }


        let valueValueLabel = UILabel()
        valueValueLabel.text = "\(transaction.value) Stars"
        valueValueLabel.font = R.font.openSansSemibold(size: 10)
        valueValueLabel.textColor = UIColor(hexString: "#444444")

        contentView.addSubview(valueValueLabel)

        valueValueLabel.snp.makeConstraints { (make) in
            make.top.equalTo(valueLabel)
            make.left.equalTo(valueLabel.snp.right).offset(4)
        }


        let borderView = UIView()
        borderView.backgroundColor = UIColor(hexString: "#eeeeee")

        contentView.addSubview(borderView)

        borderView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalTo(contentView.previousView().snp.bottom).offset(15)
        }
    }
}
