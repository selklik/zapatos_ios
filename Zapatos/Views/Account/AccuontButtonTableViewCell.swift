//
//  AccuontButtonTableViewCell.swift
//  Zain Saidin
//
//  Created by MacBook Air on 29/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class AccuontButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            backgroundColor = UIColor(hexString: "#f8f8f8")
        }
        else {
            backgroundColor = .white
        }
    }
}
