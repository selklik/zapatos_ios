//
//  AccountTitleView.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 25/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class AccountTitleView: UIView {
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
    
    @IBOutlet weak var photoOuterContainerView: UIView!
    @IBOutlet weak var photoContainerView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    var delegate: AccountTitleViewDelegate!
    
    func configure(delegate: AccountTitleViewDelegate) {
        self.delegate = delegate
        frame = CGRect(x: 0, y: 0, width: 250, height: 44)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(userSaved(_:)),
            name: NSNotification.Name(rawValue: "userSaved"),
            object: nil
        )
        configureUI()
    }
    

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func userSaved(_ sender: Foundation.Notification) {
          configureUI()
    }
    
    func configureUI() {
        if let user = kAppDelegate.user {
            nameLabel.text = user.name
            photoOuterContainerView.layer.cornerRadius = 18
            photoContainerView.layer.cornerRadius = 17
            photoImageView.layer.cornerRadius = 16
            photoImageView.af_setImage(withURL: user.profile_photos.square!)
            if user.current_balance > 0 {
                starsLabel.text = "\(user.current_balance.withComma) Stars"
            }
            else {
                starsLabel.text = "0 Star"
            }
        }
    }        
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        delegate.accountTitleView(view: self, didTapButton: sender)
    }
}

