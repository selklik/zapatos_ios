//
//  EditProfilePhotoTableViewCell.swift
//  Zain Saidin
//
//  Created by MacBook Air on 26/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class EditProfilePhotoTableViewCell: UITableViewCell {
    @IBOutlet weak var outerContainerView: UIView!
    @IBOutlet weak var innerContainerView: UIView!
    @IBOutlet weak var ProfilePhoto: UIImageView!
    @IBOutlet weak var tapView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ProfilePhoto.layer.cornerRadius = 50
        innerContainerView.layer.cornerRadius = 52
        outerContainerView.layer.cornerRadius = 54
    }

    func configure(user: User) {
       
        ProfilePhoto.af_setImage(withURL: kAppDelegate.user!.profile_photos.square!)
    }
    

}


