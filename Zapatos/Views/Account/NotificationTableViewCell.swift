//
//  NotificationTableViewCell.swift
//  Siti
//
//  Created by MacBook Air on 18/04/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notifiImage: UIImageView!
    @IBOutlet weak var notifiLabel: UILabel!
    @IBOutlet weak var notificationOutlet: UISwitch!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
      notificationOutlet.isOn = kAppDelegate.user!.notification
    }

    @IBAction func notificationSwtich(_ sender: UISwitch) {
        
        sender.isOn = !sender.isOn
        API.makePUTRequest(
            to: "me",
            parameters: ["notification": sender.isOn],
            completion: { (json) in
                
        },
            failure: nil
        )
       
       
    }
}
