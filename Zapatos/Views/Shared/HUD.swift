//
//  HUD.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import NVActivityIndicatorView

class HUD {
    class func show() {
        dismiss(animated: false)
        
        let backgroundView = HUDBackgroundView()
        backgroundView.configure()
        
        let circularView = UIView()
        circularView.backgroundColor = UIColor(hexString: "#ffffff", alpha: 0.1)
        circularView.layer.cornerRadius = 35
        backgroundView.addSubview(circularView)
        
        circularView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(70)
            make.height.equalTo(70)
        }
        
        let activityIndicatorView = NVActivityIndicatorView(
            frame: .zero,
            type: .ballSpinFadeLoader,
            color: kTabColor,
            padding: nil
        )
        
        backgroundView.addSubview(activityIndicatorView)
        
        activityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(50)
            make.height.equalTo(50)
        }
        
        activityIndicatorView.startAnimating()
        
        UIApplication.shared.keyWindow!.addSubview(backgroundView)
        
        backgroundView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    class func dismiss(animated: Bool = true) {
        let backgroundView = UIApplication.shared.keyWindow!.subviews.filter { (view) -> Bool in
            return (view as? HUDBackgroundView) != nil
        }.first
        
        if let backgroundView = backgroundView {
            if animated {
                UIView.animate(
                    withDuration: 1,
                    animations: {
                        backgroundView.alpha = 0
                    },
                    completion: { (_) in
                        backgroundView.removeFromSuperview()
                    }
                )
            }
            else {
                backgroundView.removeFromSuperview()
            }
        }
    }
    
    class func show(status: String, image: UIImage) {
        dismiss(animated: false)
        
        let backgroundView = HUDBackgroundView()
        backgroundView.configure()
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 3
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOpacity = 0.25
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1)
        backgroundView.addSubview(containerView)
        
        containerView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(280)
        }
        
        let imageView = UIImageView(image: image)
        containerView.addSubview(imageView)
        
        imageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.centerX.equalToSuperview()
            make.width.equalTo(50)
            make.height.equalTo(50)
        }
        
        let label = UILabel()
        label.text = status
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = UIColor(hexString: "#333333")
        label.font = R.font.openSans(size: 14)
        containerView.addSubview(label)
        
        label.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-20)
        }        
        
        UIApplication.shared.keyWindow!.addSubview(backgroundView)
        
        backgroundView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.dismiss(animated: false)
        })
    }
    
    class func showError(withStatus status: String) {
        show(status: status, image: R.image.error()!)
    }
    
    class func showInfo(withStatus status: String) {
        show(status: status, image: R.image.info()!)
    }
}

class HUDBackgroundView: UIView {
    func configure() {
        backgroundColor = UIColor(hexString: "#000000", alpha: 0.25)
    }
}
