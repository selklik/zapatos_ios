//
//  SectionHeaderButton.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class SectionHeaderButton: UIButton {
    var type: SectionHeaderType!
    var object: Any?
}
