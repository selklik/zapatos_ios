//
//  SectionHeaderTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit

class SectionHeaderTableViewCell: UITableViewCell {
    var delegate: SectionHeaderTableViewCellDelegate?
    var object: Any?
    var type: SectionHeaderTableViewCellType?
    
    func configure(title: String, image: UIImage? = nil, callToAction: String? = nil, object: Any? = nil, type: SectionHeaderTableViewCellType? = nil, delegate: SectionHeaderTableViewCellDelegate? = nil) {
        self.delegate = delegate
        self.object = object
        self.type = type
        
        var _image: UIImage?
        var _callToAction: String?
        var _object: Any?
        
        contentView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        
        if let callToAction = callToAction {
            _callToAction = callToAction
        }
        
        if let object = object {
            _object = object
        }
        
        if let image = image {
            _image = image
        }
        
        let separatorView = UIView()
        separatorView.backgroundColor = kBackgroundColor
        contentView.addSubview(separatorView)
        
        separatorView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(2)
        }
        
        let containerView = UIView()
        contentView.addSubview(containerView)
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalTo(separatorView.snp.bottom)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(50)
            make.bottom.equalToSuperview()
        }
        
        if let image = _image  {
            let iconImageView = UIImageView(image: image)
            containerView.addSubview(iconImageView)
            
            iconImageView.snp.makeConstraints({ (make) in
                make.centerY.equalToSuperview()
                make.left.equalToSuperview().offset(15)
                make.width.equalTo(16)
                make.height.equalTo(16)
            })
        }
        
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = R.font.openSansBold(size: 12)
        titleLabel.textColor = UIColor(hexString: "#333333")
        containerView.addSubview(titleLabel)
        
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            
            if _image != nil {
                make.left.equalTo(containerView.secondLastView.snp.right).offset(8)
            }
            else {
                make.left.equalToSuperview().offset(15)
            }
        }
        
        if let callToAction = _callToAction {
            let arrowImageView = UIImageView(image: R.image.arrowRight())
            containerView.addSubview(arrowImageView)
            
            arrowImageView.snp.makeConstraints({ (make) in
                make.width.equalTo(4)
                make.height.equalTo(7)
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview().offset(2)
            })
            
            let ctaLabel = UILabel()
            ctaLabel.textColor = UIColor(hexString: "#99A9B3")
            ctaLabel.font = R.font.openSansSemibold(size: 12)
            ctaLabel.text = callToAction
            containerView.addSubview(ctaLabel)
            
            ctaLabel.snp.makeConstraints({ (make) in
                make.centerY.equalToSuperview()
                make.right.equalTo(arrowImageView.snp.left).offset(-8)
            })
            
            let button = UIButton()
            button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            containerView.addSubview(button)
            
            button.snp.makeConstraints({ (make) in
                make.right.equalToSuperview()
                make.top.equalToSuperview()
                make.bottom.equalToSuperview()
                make.left.equalTo(ctaLabel).offset(-8)
            })
        }
        
        let borderView = UIView()
        borderView.backgroundColor = UIColor(hexString: "#EEEEEE")
        containerView.addSubview(borderView)
        
        borderView.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(0)
        }
    }
    
    @objc func buttonTapped(_ sender: SectionHeaderButton) {
        guard let delegate = delegate else { return }
        delegate.sectionHeaderTableViewCell(cell: self, didTapButton: sender, object: object, type: type)
    }
}
