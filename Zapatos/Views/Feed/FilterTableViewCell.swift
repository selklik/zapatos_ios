//
//  FilterTableViewCell.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/6/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var filterImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()

        let view = UIView()
        view.backgroundColor = UIColor(hexString: "#f8f8f8")
        selectedBackgroundView = view
    }

    func configure(row: Int) {
        switch row {
        case 0:
            titleLabel.text = "Show posts from all sources"
            filterImage.image = UIImage(named: "Allpost")

        case 1:
            titleLabel.text = "Show premium Selklik posts only"
            filterImage.image = UIImage(named: "HomeLogo-1")
            
        case 2:
            titleLabel.text = "Show social media posts only"
            filterImage.image = UIImage(named: "Feedfacebook")
            
        case 3:
            titleLabel.text = "Show Q&A media posts only"
            filterImage.image = UIImage(named: "Q&A")
            
        default:
            break
        }

        if UserDefaults.standard.integer(forKey: "selectedFilter") == row {
            titleLabel.font = R.font.openSansBold(size: 12)
        }
        else {
            titleLabel.font = R.font.openSans(size: 12)
        }
    }
}
