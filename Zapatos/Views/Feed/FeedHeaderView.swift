//
//  FeedHeaderView.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 29/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class FeedHeaderView: UIView {
    @IBOutlet weak var label: UILabel!
    var delegate: FeedHeaderViewDelegate!
    
    func configure(text: NSAttributedString, delegate: FeedHeaderViewDelegate) {
        self.delegate = delegate
        frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 200)
        label.attributedText = text
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        delegate.feedHeaderView(view: self, didTapButton: sender)
    }
}
