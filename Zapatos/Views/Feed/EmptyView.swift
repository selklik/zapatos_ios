//
//  EmptyView.swift
//  Selklik
//
//  Created by Izad Che Muda on 4/3/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var firstLineLabel: UILabel!
    @IBOutlet weak var secondLineLabel: UILabel!
    @IBOutlet weak var yConstraint: NSLayoutConstraint!
    
    func configure(image: UIImage?, firstLine: String, secondLine: String, offset: CGFloat?) {
        iconImageView.image = image
        firstLineLabel.text = firstLine
        secondLineLabel.text = secondLine
        
        if let offset = offset {
            yConstraint.constant = offset
        }
        else {
            yConstraint.constant = -22.5
        }
    }
}
