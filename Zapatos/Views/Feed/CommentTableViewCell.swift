//
//  CommentTableViewCell.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/13/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import AlamofireImage

class CommentTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        profilePhotoImageView.layer.cornerRadius = 18
        profilePhotoImageView.clipsToBounds = true
    }
    
    func configure(socialComment: SocialComment) {
        selectionStyle = .none
        
        profilePhotoImageView.af_setImage(withURL: URL(string: socialComment.from.profile_photo)!)
        nameLabel.text = socialComment.from.name
        bodyLabel.text = socialComment.body
        
        let timestamp = Date.dateFromSQLString(socialComment.timestamp)
        timestampLabel.text = (timestamp as NSDate).timeAgoSinceNow()
    }
    
    func configure(comment: Comment) {
        selectionStyle = .default
        profilePhotoImageView.af_setImage(withURL: comment.from.profile_photos.thumbnail!)
        nameLabel.text = comment.from.name
        bodyLabel.text = comment.body
        
        if comment.id != 0 {
            let timestamp = Date.dateFromSQLString(comment.timestamp)
            timestampLabel.text = (timestamp as NSDate).timeAgoSinceNow()
        }
        else {
            timestampLabel.text = "Posting..."
        }
    }
}
