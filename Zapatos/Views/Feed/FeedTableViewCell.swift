//
//  FeedTableViewCell.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/5/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import SnapKit
import AlamofireImage
import DateTools
import ActiveLabel

class FeedTableViewCell: UITableViewCell {
    var post: Post!
    var likeLabelButton: UIButton!
    var commentLabelButton: UIButton!
    var viewOption: UIView!
    var viewSocial: UIView!
    var welccome_WaterMArk = UILabel()
    var delegate: FeedTableViewCellDelegate!
    var pageCount = UILabel()
    var scrollView = UIScrollView()
    var width = 350
    var height = 370
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        selectionStyle = .none
    }
    
    func configure(delegate: FeedTableViewCellDelegate, post: Post) {        
        self.delegate = delegate
        self.post = post
        backgroundColor = .clear
        
        contentView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        
        let containerView = UIView()
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = 4
        containerView.clipsToBounds = true
        
        contentView.addSubview(containerView)
        
        containerView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(8)
            make.left.equalTo(contentView).offset(33)
            make.right.equalTo(contentView).offset(-8)
            make.bottom.equalTo(contentView).offset(-8)
        }
        
        
        let profilePhotoImageView = UIImageView()
        profilePhotoImageView.isUserInteractionEnabled = true
        profilePhotoImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(artistTapped(_:))))
        profilePhotoImageView.image = nil
        profilePhotoImageView.af_setImage(withURL:post.artist.profile_photos.square!)
        profilePhotoImageView.backgroundColor = UIColor(hexString: "#cccccc")
        profilePhotoImageView.layer.cornerRadius = 25
        profilePhotoImageView.clipsToBounds = true
        
        contentView.addSubview(profilePhotoImageView)
        
        profilePhotoImageView.snp.makeConstraints { (make) in
            make.width.equalTo(50)
            make.height.equalTo(50)
            make.top.equalTo(containerView).offset(8)
            make.left.equalTo(contentView).offset(8)
        }
        
        
        let nameLabel = UILabel()
        nameLabel.isUserInteractionEnabled = true
        nameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(artistTapped(_:))))
        nameLabel.text = post.artist.name
        nameLabel.font = R.font.openSansSemibold(size: 15)
        nameLabel.textColor = UIColor(hexString: "#1a2531")
        
        containerView.addSubview(nameLabel)
        
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(containerView).offset(15)
            make.left.equalTo(profilePhotoImageView.snp.right).offset(15)
        }
        
        
        let timestamp = Date.dateFromSQLString(post.timestamp)
        let infoLabel = UILabel()
        var agoString = (timestamp as NSDate).timeAgoSinceNow()!
        
        switch post.type {
        case "premium", "qna":
            agoString += " on Selklik"
            
        default:
            agoString += " on \(post.type.capitalized)"
        }
        
        infoLabel.text = agoString
        infoLabel.font = R.font.openSansLight(size: 11)
        containerView.addSubview(infoLabel)
        infoLabel.textColor = UIColor(hexString: "#1a2531")
        
        infoLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.top.equalTo(nameLabel.snp.bottom)
        }
        
        if let body = post.body {
            if post.type != "qna" {
                let bodyLabel = ActiveLabel(frame: CGRect.zero)
                bodyLabel.numberOfLines = 0
                bodyLabel.text = body
                bodyLabel.textColor = UIColor(hexString: "#495661")
                bodyLabel.hashtagColor = kTextColor
                bodyLabel.URLColor = kTextColor
                bodyLabel.mentionColor = kTextColor
                bodyLabel.font = R.font.openSans(size: 13)
                
                bodyLabel.handleHashtagTap { hashtag in
                    var urlString: String?
                    
                    switch post.type {
                    case "facebook":
                        urlString = "https://m.facebook.com/graphsearch/str/%2523\(hashtag)/keywords_top"
                        
                    case "twitter":
                        urlString = "https://mobile.twitter.com/hashtag/\(hashtag)"
                        
                    case "instagram":
                        urlString = "https://instagram.com/explore/tags/\(hashtag)"
                        
                    default:
                        break
                    }
                    
                    if let urlString = urlString {
                        let url =  URL(string: urlString)!
                        delegate.feedTableViewCellDelegate(cell: self, didTryToOpenURL: url)
                    }
                }
                
                bodyLabel.handleMentionTap { handle in
                    var urlString: String?
                    
                    switch self.post.type {
                    case "facebook":
                        break
                        
                    case "twitter":
                        urlString = "https://twitter.com/\(handle)"
                        
                    case "instagram":
                        urlString = "https://instagram.com/\(handle)"
                        
                    default:
                        break
                    }
                    
                    if let urlString = urlString {
                        let url =  URL(string: urlString)!
                        delegate.feedTableViewCellDelegate(cell: self, didTryToOpenURL: url)
                    }
                }
                
                bodyLabel.handleURLTap { (url) in
                    delegate.feedTableViewCellDelegate(cell: self, didTryToOpenURL: url)
                }
                
                containerView.addSubview(bodyLabel)
                
                bodyLabel.snp.makeConstraints { (make) in
                    let previousView = containerView.previousView()
                    make.left.equalTo(previousView)
                    make.top.equalTo(previousView.snp.bottom).offset(8)
                    make.right.equalTo(containerView).offset(-15)
                }
            }
        }
        
        if post.photos.count > 0 {
            let ratio = post.photos.first!.width / post.photos.first!.height
            let width = Double(kScreenWidth - CGFloat(33 - 8))
            let height = width / ratio
            
            scrollView = UIScrollView()
            scrollView.isPagingEnabled = true
            scrollView.isUserInteractionEnabled = true
            scrollView.backgroundColor = .clear
            scrollView.contentSize = CGSize(width: width * Double(post.photos.count), height: height)
            scrollView.clipsToBounds = true
            scrollView.delegate = self
            containerView.addSubview(scrollView)
            
            scrollView.snp.makeConstraints({ (make) in
                let previousView = containerView.previousView()
                make.top.equalTo(previousView.snp.bottom).offset(8)
                make.left.equalTo(containerView)
                make.right.equalTo(containerView)
                
                
                make.height.equalTo(height)
            })
            
            var photoImageView: UIImageView!

            post.photos.enumerated().forEach { (index, photo) in
             
                if index == 0 {

                    photoImageView = UIImageView()
                    photoImageView.isUserInteractionEnabled = true
                    photoImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(photoImageViewTapped(_:))))
                    photoImageView.backgroundColor = .groupTableViewBackground
                    photoImageView.image = nil
                    photoImageView.contentMode = .scaleToFill
                    photoImageView.clipsToBounds = true
                    scrollView.isScrollEnabled = false
                    scrollView.addSubview(photoImageView)
                    
                    photoImageView.snp.makeConstraints({ (make) in
                        
                        make.left.equalToSuperview()
                        make.top.equalToSuperview()
                        make.height.equalToSuperview()
                        make.width.equalToSuperview()
                        
                    })

                }
                    
            else {
                
               //    pageCount = UILabel()
                   pageCount.text = "\(1)/\(post.photos.count)"
                   pageCount.textColor = .white
                   pageCount.textAlignment = .center
                   pageCount.backgroundColor = .gray
                   pageCount.alpha = 0.8
                   pageCount.layer.cornerRadius = 11
                   pageCount.clipsToBounds = true
                    
                   containerView.addSubview(pageCount)
                 
                    pageCount.snp.makeConstraints({ (make) in

                        make.bottom.equalTo(scrollView).offset(-8)
                        
                        make.height.equalTo(22)
                        make.width.equalTo(44)
                        make.right.equalTo(scrollView).offset(-8)
                    })
                    
                let piv = UIImageView()
                    
                    piv.isUserInteractionEnabled = true
                   // piv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(photoImageViewTapped(_:))))
                    piv.backgroundColor = .groupTableViewBackground
                    piv.image = nil
                    piv.af_setImage(withURL: photo.versions.full_resolution)
                    piv.contentMode = .scaleToFill
                    piv.clipsToBounds = true
                    scrollView.addSubview(piv)
                    scrollView.isScrollEnabled = true
                    
                    piv.snp.makeConstraints({ (make) in
                        let previousView = scrollView.previousView()
                        make.left.equalTo(previousView.snp.right)
                        make.top.equalToSuperview()
                        make.height.equalToSuperview()
                        make.width.equalToSuperview()
                        
                    })
                }
            }
           
     
//            welccome_WaterMArk.text =  "Tahniah \(kAppDelegate.user!.name) kerana membeli gambar ini. ♥️ CT"
//            welccome_WaterMArk.adjustsFontSizeToFitWidth = true
//            welccome_WaterMArk.alpha = 0.6
//            welccome_WaterMArk.font.withSize(9)
//            
//            welccome_WaterMArk.textColor = UIColor.white
//            welccome_WaterMArk.isHidden = true
//            welccome_WaterMArk.sizeToFit()
//            
//            contentView.addSubview(welccome_WaterMArk)
//            
//            welccome_WaterMArk.snp.makeConstraints({ (make) in
//                
//                make.height.equalTo(40)
//                make.width.equalTo(250)
//                make.centerX.equalTo(photoImageView)
//                make.bottomMargin.equalTo(photoImageView)
//                
//            })
            
            let userName_W_Mak = UILabel()
                userName_W_Mak.text = (kAppDelegate.user?.email)!
                userName_W_Mak.adjustsFontSizeToFitWidth = true
                userName_W_Mak.alpha = 0.4
                userName_W_Mak.font.withSize(9)
                userName_W_Mak.textColor = UIColor.white
                userName_W_Mak.isHidden = true
                userName_W_Mak.sizeToFit()
            
                contentView.addSubview(userName_W_Mak)
            
            userName_W_Mak.snp.makeConstraints({ (make) in
                
                make.height.equalTo(40)
                make.width.equalTo(100)
                make.centerX.equalTo(photoImageView)
                make.topMargin.equalTo(photoImageView)
                
            })
            
            let waterMark = UIImageView()
                waterMark.image = UIImage(named: "water_mark")
                waterMark.alpha = 0.2
                waterMark.isHidden = true
                waterMark.sizeToFit()
                contentView.addSubview(waterMark)
            waterMark.snp.makeConstraints({ (make) in
                make.height.equalTo(50)
                make.width.equalTo(50)
               
                make.center.equalTo(photoImageView)
               
            })
            
            if post.type == "premium" {
                waterMark.isHidden = false
                userName_W_Mak.isHidden = false
                welccome_WaterMArk.isHidden = false
                
            } else if post.type != "premium" {
                waterMark.isHidden = true
                userName_W_Mak.isHidden = true
                welccome_WaterMArk.isHidden = true
            }
            
            if post.type == "premium" && post.metadata.full_access == false && post.attachment_type == "photo" {
                photoImageView.af_setImage(withURL: post.photos.first!.versions.preview!)
                
                waterMark.isHidden = true
                userName_W_Mak.isHidden = true
                welccome_WaterMArk.isHidden = true
            }
            else {
                photoImageView.af_setImage(withURL: post.photos.first!.versions.full_resolution)
            }
            
            if post.attachment_type == "video" && ((post.type != "qna" && post.type != "premium") || (post.type == "premium" && post.metadata.full_access)) {                
                 waterMark.isHidden = true
                 userName_W_Mak.isHidden = true
                 welccome_WaterMArk.isHidden = true
                
                let playImageView = UIImageView(image: R.image.feedPlay())
                playImageView.alpha = 0.9
                playImageView.isUserInteractionEnabled = true
                playImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(photoImageViewTapped(_:))))
              
                contentView.addSubview(playImageView)
                
                playImageView.snp.makeConstraints({ (make) in
                    make.width.equalTo(80)
                    make.height.equalTo(80)
                    make.center.equalTo(photoImageView)
                })
                
            }
            
            if post.type == "qna" {
                let overlayView = UIView()
                overlayView.backgroundColor = .black
                overlayView.alpha = 0.4
                
                contentView.addSubview(overlayView)
                
                overlayView.snp.makeConstraints({ (make) in
                    make.top.equalTo(photoImageView)
                    make.right.equalTo(photoImageView)
                    make.bottom.equalTo(photoImageView)
                    make.left.equalTo(photoImageView)
                })
                
                
                let questionContainerView = UIView()
                
                contentView.addSubview(questionContainerView)
                
                questionContainerView.snp.makeConstraints({ (make) in
                    make.left.equalTo(photoImageView)
                    make.right.equalTo(photoImageView)
                    make.bottom.equalTo(photoImageView)
                })
                
                
                let questionLabel = UILabel()
                questionLabel.text = post.body
                questionLabel.textColor = .white
                questionLabel.font = R.font.openSans(size: 12)
                questionLabel.numberOfLines = 0
                
                questionContainerView.addSubview(questionLabel)
                
                questionLabel.snp.makeConstraints({ (make) in
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-15)
                    make.left.equalToSuperview().offset(60)
                })
                
                
                let askLabel = UILabel()
                askLabel.text = "\(post.metadata.user!.name) asked"
                askLabel.textColor = .white
                askLabel.font = R.font.openSansSemibold(size: 12)
                
                questionContainerView.addSubview(askLabel)
                
                askLabel.snp.makeConstraints({ (make) in
                    make.top.equalToSuperview()
                    make.bottom.equalTo(questionLabel.snp.top).offset(-2)
                    make.left.equalTo(questionLabel)
                })
                
                
                let userProfilePhotoImageView = UIImageView()
                userProfilePhotoImageView.backgroundColor = .groupTableViewBackground
                userProfilePhotoImageView.image = nil
                userProfilePhotoImageView.af_setImage(withURL: post.metadata.user!.profile_photos.thumbnail!)
                userProfilePhotoImageView.clipsToBounds = true
                userProfilePhotoImageView.layer.cornerRadius = 15
                
                questionContainerView.addSubview(userProfilePhotoImageView)
                
                userProfilePhotoImageView.snp.makeConstraints({ (make) in
                    make.width.equalTo(30)
                    make.height.equalTo(30)
                    make.left.equalToSuperview().offset(15)
                    make.top.equalTo(askLabel)
                })
                
                
                let buttonContainerView = UIView()
                
                contentView.addSubview(buttonContainerView)
                
                buttonContainerView.snp.makeConstraints({ (make) in
                    make.left.equalTo(photoImageView)
                    make.right.equalTo(photoImageView)
                    make.top.equalTo(photoImageView)
                    make.bottom.equalTo(questionContainerView.snp.top)
                })
                
                
                let qnaPlayButton = UIButton()
                qnaPlayButton.setImage(R.image.feedPlay(), for: .normal)
                qnaPlayButton.alpha = 0.9
                qnaPlayButton.addTarget(self, action: #selector(qnaPlayButtonTapped(_:)), for: .touchUpInside)
                
                buttonContainerView.addSubview(qnaPlayButton)
                
                qnaPlayButton.snp.makeConstraints({ (make) in
                    make.width.equalTo(80)
                    make.height.equalTo(80)
                    make.center.equalToSuperview()
                })
            }
            
            if post.type == "premium" && post.metadata.full_access == false {
                let purchaseButton = UIButton()
                purchaseButton.addTarget(self, action: #selector(purchaseButtonTapped(_:)), for: .touchUpInside)
                purchaseButton.layer.shadowColor = UIColor.black.cgColor
                purchaseButton.layer.shadowOpacity = 0.25
                purchaseButton.layer.shadowOffset = CGSize(width: 0, height: 1)
                purchaseButton.layer.cornerRadius = 3
                purchaseButton.setTitleColor(.white, for: .normal)
                if post.attachment_type == "video" && post.type != "qna"{
                    purchaseButton.setTitle("\(Int(post.metadata.price).withComma) to Play", for: .normal)
                    waterMark.isHidden = true
                }
                else {
                    purchaseButton.setTitle("\(Int(post.metadata.price).withComma) to View", for: .normal)
                    waterMark.isHidden = true
                }
                
                purchaseButton.titleLabel!.font = R.font.openSans(size: 13)
                purchaseButton.backgroundColor = kButtonColor
                purchaseButton.sizeToFit()
                purchaseButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 15)
                
                contentView.addSubview(purchaseButton)
                
                purchaseButton.snp.makeConstraints({ (make) in
                    make.height.equalTo(40)
                    make.center.equalTo(photoImageView)
                })
                
                
                let starImageView = UIImageView(image: R.image.feedStar())
                contentView.addSubview(starImageView)
                
                starImageView.snp.makeConstraints({ (make) in
                    make.width.equalTo(16)
                    make.height.equalTo(16)
                    make.centerY.equalTo(purchaseButton)
                    make.left.equalTo(purchaseButton).offset(15)
                })
            }
            //else {
            
            //}
        }
        
        
        let actionView = UIView()
        actionView.backgroundColor = UIColor(hexString: "#ffffff")
        containerView.addSubview(actionView)
        
        actionView.snp.makeConstraints { (make) in
            let previousView = containerView.previousView()
            make.top.equalTo(previousView.snp.bottom).offset(15)
            make.height.equalTo(44)
            make.left.equalTo(containerView)
            make.right.equalTo(containerView)
            make.bottom.equalTo(containerView)
        }
        
        
        let actionBorderView = UIView()
        actionBorderView.backgroundColor = UIColor(hexString: "#EDEFEF")
        actionView.addSubview(actionBorderView)
        
        actionBorderView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.right.equalToSuperview()
        }
        
//        let ImageSwap = UIPageControl()
//        ImageSwap.pageIndicatorTintColor  = .gray
//        ImageSwap.currentPage = 3
//        ImageSwap.currentPageIndicatorTintColor = .blue
//        ImageSwap.backgroundColor = .gray
//        actionView.addSubview(ImageSwap)
//        
//        ImageSwap.snp.makeConstraints { (make) in
//            make.width.equalTo(40)
//            make.height.equalTo(15)
//            //make.centerY.equalTo(containerView)
//            make.centerX.equalTo(actionView)
//            make.topMargin.equalTo(actionView).offset(-10)
//        }
        
        
        let likeButton = LikeButton()
        likeButton.liked = post.liked
        likeButton.configureUI()
        likeButton.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
        actionView.addSubview(likeButton)
        
        likeButton.snp.makeConstraints { (make) in
            make.width.equalTo(22)
            make.height.equalTo(22)
            make.centerY.equalTo(actionView)
            make.left.equalTo(actionView).offset(15)
        }
        
        
        likeLabelButton = UIButton()
        likeLabelButton.titleLabel!.font = R.font.openSans(size: 10)
        likeLabelButton.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        likeLabelButton.addTarget(self, action: #selector(likeLabelButtonTapped(_:)), for: .touchUpInside)
        
        actionView.addSubview(likeLabelButton)
        
        likeLabelButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(actionView)
            make.left.equalTo(likeButton.snp.right).offset(6)
        }
        
        
        let commentButton = UIButton()
        commentButton.setImage(R.image.feedComment(), for: .normal)
        commentButton.addTarget(self, action: #selector(commentButtonTapped(_:)), for: .touchUpInside)
        
        actionView.addSubview(commentButton)
        
        commentButton.snp.makeConstraints { (make) in
            make.width.equalTo(22)
            make.height.equalTo(22)
            make.centerY.equalTo(actionView)
            make.left.equalTo(likeLabelButton.snp.right).offset(15)
        }
        
        
        commentLabelButton = UIButton()
        commentLabelButton.titleLabel!.font = R.font.openSans(size: 10)
        commentLabelButton.setTitleColor(UIColor(hexString: "#999999"), for: .normal)
        commentLabelButton.addTarget(self, action: #selector(commentLabelButtonTapped(_:)), for: .touchUpInside)
        
        actionView.addSubview(commentLabelButton)
        
        commentLabelButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(actionView)
            make.left.equalTo(commentButton.snp.right).offset(6)
        }
       
        
        if post.type != "premium" && post.type != "qna" && post.type != "twitter" {
            let socialMediaButton = UIButton()
            socialMediaButton.setImage(UIImage(named: "Feed\(post.type)"), for: .normal)
            socialMediaButton.addTarget(self, action: #selector(socialMediaButtonTapped(_:)), for: .touchUpInside)
            
            actionView.addSubview(socialMediaButton)
            
            socialMediaButton.snp.makeConstraints { (make) in
                make.width.equalTo(22)
                make.height.equalTo(22)
                make.centerY.equalTo(actionView)
                make.left.equalTo(commentLabelButton.snp.right).offset(15)
            }
            viewSocial = UIView()
            viewSocial.backgroundColor = .clear
            actionView.addSubview(viewSocial)
            
            viewSocial.snp.makeConstraints { (make) in
                make.width.equalTo(1)
                make.height.equalTo(1)
                make.centerY.equalTo(actionView)
                make.left.equalTo(commentLabelButton.snp.right).offset(34)
            }
//            let caretImageView = UIImageView(image: R.image.feedCaret())
//            caretImageView.contentMode = .center
//            //caretImageView.backgroundColor = .red
//            caretImageView.isUserInteractionEnabled = true
//            caretImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(socialMediaButtonTapped(_:))))
//            actionView.addSubview(caretImageView)
//
//            caretImageView.snp.makeConstraints { (make) in
//                make.width.equalTo(14)
//                make.height.equalTo(socialMediaButton)
//                make.centerY.equalTo(socialMediaButton)
//                make.left.equalTo(socialMediaButton.snp.right)
//            }
        }
     
        
        viewOption = UIView()
        viewOption.backgroundColor = .clear
        
        actionView.addSubview(viewOption)
        viewOption.snp.makeConstraints { (make) in
            make.width.equalTo(1)
            make.height.equalTo(1)
            make.centerY.equalTo(actionView)
            make.right.equalTo(actionView).offset(-20)
        }
        let optionsButton = UIButton()
        optionsButton.setImage(R.image.feedOptions(), for: .normal)
        optionsButton.addTarget(self, action: #selector(optionsButtonTapped(_:)), for: .touchUpInside)
        
        actionView.addSubview(optionsButton)
        
        optionsButton.snp.makeConstraints { (make) in
            make.width.equalTo(22)
            make.height.equalTo(22)
            make.centerY.equalTo(actionView)
            make.right.equalTo(actionView).offset(-8)
        }
        
        
        let borderView = UIView()
        borderView.backgroundColor = UIColor(hexString: "#dedede")
        actionView.addSubview(borderView)
        
        borderView.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.equalTo(actionView).offset(2)
            make.right.equalTo(actionView).offset(-2)
            make.bottom.equalTo(actionView)
        }
        
        configureUI()
    }
    
    @objc func likeButtonTapped(_ sender: LikeButton) {
        sender.liked = !sender.liked
        post.liked = !post.liked
        
        if post.liked {
            post.like_count = post.like_count + 1
        }
        else if post.like_count - 1 > 0 {
            post.like_count = post.like_count - 1
        }
        else {
            post.like_count = 0
        }
        
        configureUI()
        delegate.feedTableViewCellDelegate(cell: self, didTapLikeButtonOnPost: post)
    }
    
    @objc func likeLabelButtonTapped(_ sender: UIButton) {
        delegate.feedTableViewCellDelegate(cell: self, didTapLikeLabelButton: sender, onPost: post)
    }
    
    @objc func commentButtonTapped(_ sender: UIButton) {
        delegate.feedTableViewCellDelegate(cell: self, didTapCommentButton: sender, onPost: post)
        //print(commentLabelButton,"Working")
    }
    
    @objc func commentLabelButtonTapped(_ sender: UIButton) {
        delegate.feedTableViewCellDelegate(cell: self, didTapCommentLabelButton: sender, onPost: post)
    }
    
    @objc func socialMediaButtonTapped(_ sender: Any) {
       //delegate.feedTableViewCellDelegate(cell: self, didTapSocialMediaButtonOnPost: post)
    }
    
    func configureUI() {
        likeLabelButton.setTitle(pluralForm(object: "like", count: post.like_count), for: .normal)
        commentLabelButton.setTitle(pluralForm(object: "comment", count: post.comment_count), for: .normal)
    }
    
    @objc func artistTapped(_ sender: UITapGestureRecognizer) {
        delegate.feedTableViewCellDelegate(cell: self, didTapArtist: post.artist)
    }
    
    @objc func photoImageViewTapped(_ sender: UITapGestureRecognizer) {
        delegate.feedTableViewCellDelegate(cell: self, didTapPhotoImageView: post)
    }
    
    @objc func purchaseButtonTapped(_ sender: UIButton) {
        //delegate.feedTableViewCellDelegate(cell: self, didTapPhotoImageView: post)
       
    }
    
    @objc func optionsButtonTapped(_ sender: UIButton) {
        delegate.feedTableViewCellDelegate(cell: self, didTapOptionsButton: sender, onPost: post)
       
    }
    
    @objc func qnaPlayButtonTapped(_ sender: UIButton) {
        delegate.feedTableViewCellDelegate(cell: self, didTapQnaPlayButton: sender, onPost: post)
    }
    
    func pluralForm(object: String, count: Int) -> String {
        if count > 1 {
            return "\(count) \(object)s"
        }
        else if count == 1 {
            return "1 \(object)"
        }
        else {
            return object.capitalized
        }
    }
    
}
extension FeedTableViewCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let page = floor((scrollView.contentOffset.x - kScreenWidth / 2) / kScreenWidth) + 1
       
            pageCount.text = "\(Int(page + 1))/\(post.photos.count)"
        
        
    }
}

