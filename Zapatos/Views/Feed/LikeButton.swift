//
//  LikeButton.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/6/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit

class LikeButton: UIButton {

    var liked = false {
        didSet {
            configureUI()
        }
    }
    
    func configureUI() {
        if liked {
            setImage(R.image.feedLiked(), for: .normal)
        }
        else {
            setImage(R.image.feedLike(), for: .normal)
        }
    }

}
