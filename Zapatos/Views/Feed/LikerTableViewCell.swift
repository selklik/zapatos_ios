//
//  LikerTableViewCell.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class LikerTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profilePhotoImageView.layer.cornerRadius = 18
        profilePhotoImageView.clipsToBounds = true
    }
    
    func configure(socialLiker: SocialLiker) {
        nameLabel.text = socialLiker.name
        profilePhotoImageView.af_setImage(withURL: URL(string: socialLiker.profile_photo)!)
    }
    
    func configure(liker: UserOrArtist) {
        nameLabel.text = liker.name
        profilePhotoImageView.af_setImage(withURL: liker.profile_photos.thumbnail!)
    }
    
}

