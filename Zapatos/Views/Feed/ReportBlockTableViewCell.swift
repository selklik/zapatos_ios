//
//  ReportBlockTableViewCell.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class ReportBlockTableViewCell: UITableViewCell {
    var delegate: ReportBlockTableViewCellDelegate!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(delegate: ReportBlockTableViewCellDelegate, post: Post?, artist: Artist?) {
        self.delegate = delegate
        
        if post != nil {
            titleLabel.text = "Hide this post from your feed?"
        }
        
        if let artist = artist {
            titleLabel.text = "Block all \(artist.name)'s posts?"
        }
    }
    
    @IBAction func switchTapped(_ sender: UISwitch) {
        delegate.reportBlockTableViewCell(cell: self, didSetBlock: sender.isOn)
    }
    
}

