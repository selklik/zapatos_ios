//
//  ReportReasonTableViewCell.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import UITextView_Placeholder

class ReportReasonTableViewCell: UITableViewCell {
    @IBOutlet weak var textView: UITextView!
    var delegate: ReportReasonTableViewCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.placeholder = "Why are you reporting this?"
    }
    
    func configure(delegate: ReportReasonTableViewCellDelegate) {
        self.delegate = delegate
    }
    
}


extension ReportReasonTableViewCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let text = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        if text.characters.count > 0 {
            delegate.reportReasonTableViewCell(cell: self, didTypeReason: text)
        }
        else {
            delegate.reportReasonTableViewCell(cell: self, didTypeReason: nil)
        }
        
        return true
    }
    
}

