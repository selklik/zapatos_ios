//
//  FeedTitleView.swift
//  Zain Saidin
//
//  Created by MacBook Air on 21/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class FeedTitleView: UIView {
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
    
    @IBOutlet weak var descLabel: UILabel!
    var delegate: FeedTitleViewDelegate!
    
    func configure(delegate: FeedTitleViewDelegate) {
        self.delegate = delegate
        frame = CGRect(x: 0, y: 0, width: 230, height: 44)
        configureUI()
    }
    
    func configureUI() {
        let selectedFilter = UserDefaults.standard.integer(forKey: "selectedFilter")
        
        switch selectedFilter {
        case 0:
            descLabel.text = "Showing posts from all sources"
        case 1:
            descLabel.text = "Showing premium Selklik posts only"
        case 2:
            descLabel.text = "Showing social media posts only"
        case 3:
            descLabel.text = "Showing Q&A media posts only"
        default:
            break
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {    
        delegate.feedTitleView(view: self, didTapButton: sender)
    }
}
