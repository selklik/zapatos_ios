//
//  HomeWorksTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit

class HomeWorksTableViewCell: UITableViewCell {
    var works: [Work] = []
    var delegate: HomeWorksTableViewCellDelegate!
    
   // func configure(key: String, works: [Work], delegate: HomeWorksTableViewCellDelegate) 
    
    @objc func buttonTapped(_ sender: UIButton) {
        delegate.homeWorksTableViewCell(cell: self, didSelectWork: works[sender.tag])
    }
}
