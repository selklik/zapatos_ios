//
//  HomeHeaderTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class BannerTableViewCell: UITableViewCell {
    var banner: Banner!
    @IBOutlet weak var bannerScrollView: UIScrollView!
    @IBOutlet weak var bannerPageControl: UIPageControl!
   

     var imagesArray = [UIImage]()
     static var i = 0
     var timer: Timer!
     var Banner: BannerData!
    
    override func awakeFromNib() {
        super.awakeFromNib()
  
    }
  
    func configure() {
        SecondAPI.makeGETRequest2(
            to: "banner.php?banner=img",
            parameters: nil,
            completion: { (json) in
                self.Banner = BannerData(json)
               // print("\(self.Banner.data.count) banner")
                self.bannerScrollView.delegate = self
                self.bannerScrollView.delegate = self
                
                
                self.bannerPageControl.numberOfPages = self.Banner.data.count
                self.bannerScrollView.contentSize = CGSize(width: kScreenWidth * CGFloat(self.Banner.data.count), height: self.bannerScrollView.frame.size.height)
                
                (0...(self.Banner.data.count - 1)).forEach { (index) in
                    
                    let imageview = UIImageView()
                    let url = URL(string: (self.Banner.data[index].image_url))!
                    imageview.af_setImage(withURL: url)
                    
                    imageview.contentMode = .scaleAspectFit
                   // imageview.clipsToBounds = true
                   
                    imageview.frame = CGRect(x: kScreenWidth * CGFloat(index), y: 0, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.height)
                    
                    self.contentView.addSubview(self.bannerPageControl)
                    self.bannerScrollView.addSubview(imageview)
                }
        },
            failure: nil
        )
        
        timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { (t) in
            var nextPage: Int
            
            if self.bannerPageControl.currentPage == self.bannerPageControl.numberOfPages - 1 {
                nextPage = 0
               
            }
            else {
                nextPage = (self.bannerPageControl.currentPage + 1)
                
    
            }
            
            let rect = CGRect(x: kScreenWidth * CGFloat(nextPage), y: 20, width: self.bannerScrollView.frame.width, height: self.bannerScrollView.frame.size.height)
            self.bannerScrollView.scrollRectToVisible(rect, animated: true)
        })

    }
    
   
   

}
extension BannerTableViewCell: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = bannerScrollView.frame.size.width
        let page = Int(floor((bannerScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        
        bannerPageControl.currentPage = Int(page)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if timer.isValid {
            timer.invalidate()
        }
    }
}
