//
//  RecentPostsTableViewCell.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit

class RecentPostsTableViewCell: UITableViewCell {
    
 
    
    var videoPost: Post?
    var photoPosts: [Post] = []
    var delegate: RecentPostsTableViewCellDelegate!
    var shoo: Datas!
    var shoeCout: Int = 0
    
    var topRightContainerView = UIView()
    var bottomRightContainerView = UIView()
    var bottomMiddleContainerView = UIView()
    var bottomLeftContainerView = UIView()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    func zapatos(delegate: RecentPostsTableViewCellDelegate) {
        
        getImageData()
        self.delegate = delegate
        
        self.contentView.subviews.forEach { view in
            view.removeFromSuperview()
        }
        let videoContainerView = UIView()
        videoContainerView.backgroundColor = .groupTableViewBackground
        contentView.addSubview(videoContainerView)
        let dimension = (kScreenWidth - 1 - 1) / 3
        let videoDimension = kScreenWidth - dimension - 1
        
        videoContainerView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalTo(videoDimension)
            make.height.equalTo(dimension)
        }
        
        let gradientColor = CAGradientLayer()
        gradientColor.frame = CGRect(x: 0, y: 0, width: (kScreenWidth / 3) * 2 , height: (kScreenWidth - 1 - 1) / 3)
        gradientColor.colors = [UIColor.init(hex: "#FBC254").cgColor, UIColor.black.cgColor,UIColor.white.cgColor]
        gradientColor.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientColor.endPoint = CGPoint(x: 1.0, y: 1.0)
        videoContainerView.layer.addSublayer(gradientColor)
        
        let uilabel = UILabel()
        uilabel.text = "Feel like Celebrity"
        uilabel.textAlignment = .center
        uilabel.textColor = .white
        uilabel.adjustsFontSizeToFitWidth = true
        uilabel.font = R.font.openSansLightItalic(size: 35)
        videoContainerView.addSubview(uilabel)
        
        uilabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(videoContainerView)
            make.centerY.equalTo(videoContainerView)
            make.width.equalTo(videoContainerView)
            make.height.equalTo(34)
            
        }
        let uilabel2 = UILabel()
        uilabel2.text = "Be a zapotniannow!"
        uilabel2.textAlignment = .right
        uilabel2.textColor = .white
        uilabel2.font = R.font.openSansLightItalic(size: 15)
        uilabel.addSubview(uilabel2)
        
        uilabel2.snp.makeConstraints { (make) in
            make.bottom.equalTo(uilabel).offset(17)
            make.right.equalTo(uilabel).offset(-12)
            make.width.equalTo(uilabel)
            make.height.equalTo(17)
            
        }
        
        topRightContainerView.tag = 0
        topRightContainerView.backgroundColor = .white
        contentView.addSubview(topRightContainerView)
        topRightContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(videoContainerView)
            make.left.equalTo(videoContainerView.snp.right).offset(2)
            make.right.equalToSuperview()
            make.height.equalTo(dimension)
            make.width.equalTo(dimension)
        }

      
        bottomRightContainerView.tag = 1
        bottomRightContainerView.backgroundColor = .white
        contentView.addSubview(bottomRightContainerView)
        
        bottomRightContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(topRightContainerView.snp.bottom).offset(2)
            make.left.equalTo(topRightContainerView)
            make.right.equalToSuperview()
            make.height.equalTo(dimension)
            make.width.equalTo(dimension)
            make.bottom.equalToSuperview()
        }

        
        bottomMiddleContainerView.tag = 2
        bottomMiddleContainerView.backgroundColor = .white
        contentView.addSubview(bottomMiddleContainerView)
        
        bottomMiddleContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(bottomRightContainerView)
            make.right.equalTo(bottomRightContainerView.snp.left).offset(-2)
            make.bottom.equalToSuperview()
            make.height.equalTo(dimension)
            make.width.equalTo(dimension)
        }

        bottomLeftContainerView.tag = 3
        bottomLeftContainerView.backgroundColor = .white
        contentView.addSubview(bottomLeftContainerView)
        
        bottomLeftContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(bottomMiddleContainerView)
            make.right.equalTo(bottomMiddleContainerView.snp.left).offset(-2)
            make.left.equalToSuperview()
            make.bottom.equalToSuperview()
            make.height.equalTo(dimension)
            make.width.equalTo(dimension)
        }

    }
  @objc func getImageData(){
        SecondAPI.makeGETRequest2(
            to: "inside.php?category=Men&type=BestSeller",
            parameters: nil,
            completion: { (json) in
                
                self.shoo = Datas(json)
                self.shoeCout = self.shoo.data.count
                
                if self.shoo.data.indices.contains(0) {
                    self.topRightContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.containerViewTapped(_:))))
                    let photoImageView = UIImageView()
                    photoImageView.clipsToBounds = true
                    photoImageView.contentMode = .scaleAspectFill
                    let url = URL(string: (self.shoo.data[0].products[0].prod_img))!
                    photoImageView.af_setImage(withURL: url)
                    self.topRightContainerView.addSubview(photoImageView)
                    
                    photoImageView.snp.makeConstraints({ (make) in
                        make.top.equalToSuperview()
                        make.bottom.equalToSuperview().offset(-50)
                        make.right.equalToSuperview()
                        make.left.equalToSuperview()
                    })
                    let PriceLabel = UILabel()
                    PriceLabel.text = "RM\(self.shoo.data[0].price)"
                    PriceLabel.backgroundColor = .black
                    PriceLabel.textAlignment = .center
                    PriceLabel.adjustsFontSizeToFitWidth = true
                    PriceLabel.textColor = zapatosColor
                    PriceLabel.font = R.font.openSansBold(size: 11)
                    PriceLabel.layer.cornerRadius = 12.5
                    PriceLabel.clipsToBounds = true
                   
                    self.topRightContainerView.addSubview(PriceLabel)
                    
                    PriceLabel.snp.makeConstraints { (make) in
                        make.bottom.equalTo(self.topRightContainerView).offset(-3)
                        make.right.equalTo(self.topRightContainerView).offset(-33)
                        make.left.equalTo(self.topRightContainerView).offset(33)
                        make.height.equalTo(25)
                        
                        
                        
                    }
                    let Pro_name = UILabel()
                    Pro_name.text = self.shoo.data[0].prod_name
                    Pro_name.textAlignment = .center
                    Pro_name.textColor = .black
                    Pro_name.font = R.font.openSansLight(size: 14)
                    self.topRightContainerView.addSubview(Pro_name)
                    
                    Pro_name.snp.makeConstraints { (make) in
                        make.bottom.equalTo(PriceLabel).offset(-30)
                        make.right.equalTo(self.topRightContainerView)
                        make.left.equalTo(self.topRightContainerView)
                        make.height.equalTo(16)
                        
                    }
                }
            if self.shoo.data.indices.contains(1) {
                self.bottomRightContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.containerViewTapped(_:))))
                let photoImageView = UIImageView()
                photoImageView.clipsToBounds = true
                photoImageView.contentMode = .scaleAspectFill
    
                let url = URL(string: (self.shoo.data[1].products[0].prod_img))!
                photoImageView.af_setImage(withURL: url)
                self.bottomRightContainerView.addSubview(photoImageView)
    
                photoImageView.snp.makeConstraints({ (make) in
                    make.top.equalToSuperview()
                    make.bottom.equalToSuperview().offset(-50)
                    make.right.equalToSuperview()
                    make.left.equalToSuperview()
                })
                let PriceLabel = UILabel()
                PriceLabel.text = "RM\(self.shoo.data[1].price)"
                PriceLabel.backgroundColor = .black
                PriceLabel.textAlignment = .center
                PriceLabel.adjustsFontSizeToFitWidth = true
                PriceLabel.textColor = zapatosColor
                PriceLabel.font = R.font.openSansBold(size: 11)
                PriceLabel.layer.cornerRadius = 12.5
                PriceLabel.clipsToBounds = true
                self.bottomRightContainerView.addSubview(PriceLabel)
                
                PriceLabel.snp.makeConstraints { (make) in
                    make.bottom.equalTo(self.bottomRightContainerView).offset(-3)
                    make.right.equalTo(self.bottomRightContainerView).offset(-33)
                    make.left.equalTo(self.bottomRightContainerView).offset(33)
                    make.height.equalTo(25)
                    
                }
                let Pro_name = UILabel()
                Pro_name.text = self.shoo.data[1].prod_name
                Pro_name.textAlignment = .center
                Pro_name.textColor = .black
                Pro_name.font = R.font.openSansLight(size: 14)
                self.bottomRightContainerView.addSubview(Pro_name)
                
                Pro_name.snp.makeConstraints { (make) in
                    make.bottom.equalTo(PriceLabel).offset(-30)
                    make.right.equalTo(self.bottomRightContainerView)
                    make.left.equalTo(self.bottomRightContainerView)
                    make.height.equalTo(16)
                    
                }
            }
              
        if self.shoo.data.indices.contains(2) {
            self.bottomMiddleContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.containerViewTapped(_:))))
            let photoImageView = UIImageView()
            photoImageView.clipsToBounds = true
            photoImageView.contentMode = .scaleAspectFill
            let url = URL(string: (self.shoo.data[2].products[0].prod_img))!
            photoImageView.af_setImage(withURL: url)
            self.bottomMiddleContainerView.addSubview(photoImageView)

            photoImageView.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.bottom.equalToSuperview().offset(-50)
                make.right.equalToSuperview()
                make.left.equalToSuperview()
            })
            let PriceLabel = UILabel()
            PriceLabel.text = "RM\(self.shoo.data[3].price)"
            PriceLabel.backgroundColor = .black
            PriceLabel.textAlignment = .center
            PriceLabel.adjustsFontSizeToFitWidth = true
            PriceLabel.textColor = zapatosColor
            PriceLabel.font = R.font.openSansBold(size: 11)
            PriceLabel.layer.cornerRadius = 12.5
            PriceLabel.clipsToBounds = true
            self.bottomMiddleContainerView.addSubview(PriceLabel)
            
            PriceLabel.snp.makeConstraints { (make) in
                make.bottom.equalTo(self.bottomMiddleContainerView).offset(-3)
                make.right.equalTo(self.bottomMiddleContainerView).offset(-33)
                make.left.equalTo(self.bottomMiddleContainerView).offset(33)
                make.height.equalTo(25)
                
            }
            let Pro_name = UILabel()
            Pro_name.text = self.shoo.data[2].prod_name
            Pro_name.textAlignment = .center
            Pro_name.textColor = .black
            Pro_name.font = R.font.openSansLight(size: 14)
            self.bottomMiddleContainerView.addSubview(Pro_name)
            
            Pro_name.snp.makeConstraints { (make) in
                make.bottom.equalTo(PriceLabel).offset(-30)
                make.right.equalTo(self.bottomMiddleContainerView)
                make.left.equalTo(self.bottomMiddleContainerView)
                make.height.equalTo(16)
                
            }
        }
        if self.shoo.data.indices.contains(3) {
            self.bottomLeftContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.containerViewTapped(_:))))
            let photoImageView = UIImageView()
            photoImageView.clipsToBounds = true
            photoImageView.contentMode = .scaleAspectFill
            photoImageView.contentMode = .scaleAspectFill
            let url = URL(string: (self.shoo.data[3].products[0].prod_img))!
            photoImageView.af_setImage(withURL: url)
            self.bottomLeftContainerView.addSubview(photoImageView)

            photoImageView.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()
                make.bottom.equalToSuperview().offset(-50)
                make.right.equalToSuperview()
                make.left.equalToSuperview()
            })
            let PriceLabel = UILabel()
            PriceLabel.text = "RM\(self.shoo.data[3].price)"
            PriceLabel.backgroundColor = .black
            PriceLabel.textAlignment = .center
            PriceLabel.adjustsFontSizeToFitWidth = true
            PriceLabel.textColor = zapatosColor
            PriceLabel.font = R.font.openSansBold(size: 11)
            PriceLabel.layer.cornerRadius = 12.5
            PriceLabel.clipsToBounds = true
            self.bottomLeftContainerView.addSubview(PriceLabel)
            
            PriceLabel.snp.makeConstraints { (make) in
                make.bottom.equalTo(self.bottomLeftContainerView).offset(-3)
                make.right.equalTo(self.bottomLeftContainerView).offset(-33)
                make.left.equalTo(self.bottomLeftContainerView).offset(33)
                make.height.equalTo(25)
                
            }
            let Pro_name = UILabel()
            Pro_name.text = self.shoo.data[3].prod_name
            Pro_name.textAlignment = .center
            Pro_name.textColor = .black
            Pro_name.font = R.font.openSansLight(size: 14)
            self.bottomLeftContainerView.addSubview(Pro_name)
            
            Pro_name.snp.makeConstraints { (make) in
                make.bottom.equalTo(PriceLabel).offset(-30)
                make.right.equalTo(self.bottomLeftContainerView)
                make.left.equalTo(self.bottomLeftContainerView)
                make.height.equalTo(16)
                
            }
        }
                
        },
            failure: nil
        )
    }
 
    @objc func containerViewTapped(_ sender: UITapGestureRecognizer) {
        let post = shoo.data[sender.view!.tag]
        print("tappeddd")
        delegate.recentPostsTableViewCell(cell: self, didSelectPost: post)
    }
}
