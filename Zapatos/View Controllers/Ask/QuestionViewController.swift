//
//  QuestionViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 24/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

import SwiftyAttributes
import PMAlertController

class QuestionViewController: UIViewController {
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { return .custom }
        set {}
    }
    
    @IBOutlet weak var instructionLabel: UILabel!
    @IBOutlet weak var textFieldContainerView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet var starButtons: [UIButton]!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var delegate: QuestionViewControllerDelegate!
   
    var selectedPriceIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        instructionLabel.attributedText =
            "Ask responsibly! No vulgarity or explicit language please 😃".withTextColor(UIColor(hexString: "#333333")!).withFont(R.font.openSans(size: 14)!)
        
        textFieldContainerView.layer.cornerRadius = 4
        submitButton.layer.cornerRadius = 4
        
       
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardShown(notification:)),
            name: NSNotification.Name(rawValue: "UIKeyboardWillShowNotification"),
            object: nil
        )
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped(_:)))
        backgroundView.addGestureRecognizer(recognizer)
        
        containerView.alpha = 0
        textField.becomeFirstResponder()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func starButtonTapped(_ sender: UIButton) {
  
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        HUD.show()
        
        API.makePOSTRequest(            
            to: "artists/\(kAppDelegate.artist!.id)/messages",
            parameters: [
                "question": textField.text!,
                "price": "1"
            ],
            completion: { json in
                HUD.showInfo(withStatus: "Your question has been sent to \(kArtistShortName)!")
                let message = Message(json)
                self.delegate.questionViewController(controller: self, didCreateMessage: message)
            }            
        )
    }
    
    @objc func keyboardShown(notification: Notification) {
        var keyboardHeight: CGFloat!
        
        if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            keyboardHeight = value.cgRectValue.size.height
        }
        else {
            keyboardHeight = 216
        }
        
        UIView.animate(
            withDuration: 0.3,
            animations: {
                self.bottomConstraint.constant = keyboardHeight
                self.containerView.alpha = 1
                self.view.layoutIfNeeded()
            }
        )
    }
}
