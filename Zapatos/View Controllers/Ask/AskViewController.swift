//
//  HomeViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 23/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//
import UIKit
import SnapKit
import SwiftyJSON
import SwiftyAttributes


class AskViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var askButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var askBG: NSLayoutConstraint!
    @IBOutlet weak var scrollViewTopConstraint: NSLayoutConstraint!
    var room: Room!
    var width = 300
    var height = 320
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if kScreenHeight >= 1024 {
            askBG.constant = 100
            width = 350
            height = 370
            scrollViewTopConstraint.constant = 60
            
        }
        addTitlelessBackButton()
        
        if let path = Bundle.main.path(forResource: "room", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let json = try JSON(data: data)
                room = Room(json)
                print(room.artist.country_name)
            }
            catch let error {
                print(error)
            }
        }
        
        guard let nc = navigationController as? NavigationController else { return }
        nc.makeTransparent()
        nc.statusBarStyle = .default
        nc.setNeedsStatusBarAppearanceUpdate()
        
        askButton.layer.cornerRadius = 3        
        
        let numberOfPages = 4
        scrollView.contentSize = CGSize(width: width * numberOfPages, height: height)
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        (0...(numberOfPages - 1)).forEach { (index) in
            let askTutorialView = R.nib.askTutorialView.firstView(owner: nil)!
            
            switch index {
                case 0:
                    askTutorialView.configure(
                        image: R.image.tutorial1()!,
                        description: "Ask \(kArtistShortName) any question you would like to know the answer to. Her favourite food, her pet’s name, anything!".attributedString
                    )
                
                case 1:
                    askTutorialView.configure(
                        image: R.image.tutorial3()!,
                        description: "Get more bang for your buck! \(kArtistShortName) answers your questions in the form of short videos.".attributedString
                )
                
                case 2:
                    askTutorialView.configure(
                        image: R.image.tutorial2()!,
                        description: "Put a bounty on your question! \(kArtistShortName) accepts an in-app currency called ".attributedString + "Selklik Stars".withFont(R.font.openSansBold(size: 14)!) + ".".attributedString
                )
                default:
                    askTutorialView.configure(
                        image: R.image.refund()!,
                        description: "Star back guarantee! If \(kArtistShortName) can't find the time to answer your question with in 2 days your stars will be refunfded in full.".attributedString
                    )
                
            }
            
            scrollView.addSubview(askTutorialView)
            
            askTutorialView.snp.makeConstraints({ (make) in
                make.top.equalToSuperview()                
                make.width.equalTo(width)
                make.height.equalTo(height)
                
                if index == 0 {
                    make.left.equalToSuperview()
                }
                else {
                    make.left.equalTo(scrollView.secondLastView.snp.right)
                }
                
                if index == numberOfPages - 1 {
                    make.right.equalToSuperview()
                }
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let nc = navigationController as? NavigationController else { return }
        nc.makeTransparent()
        nc.statusBarStyle = .default
        nc.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        guard let nc = navigationController as? NavigationController else { return }
        nc.makeOpaque()
        nc.statusBarStyle = .default
        nc.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
            case R.segue.askViewController.showMessages.identifier:
                let messagesVC = segue.destination as! MessagesViewController
                messagesVC.messages = sender as! [Message]
            
            case R.segue.askViewController.showQuestion.identifier:
                let questionVC = segue.destination as! QuestionViewController
                questionVC.delegate = self
            
            default:
                break
        }
    }
    
    @IBAction func askButtonTapped(_ sender: UIButton) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "Auth")
        if launchedBefore{
            HUD.show()
            API.makeGETRequest(
                to: "artists/\(kArtistID)/messages",
                completion: { json in
                    HUD.dismiss()
                    
                    let messages = json.arrayValue.map({ (json) -> Message in
                        return Message(json)
                    })
                    
                    if messages.count == 0 {
                        self.performSegue(withIdentifier: R.segue.askViewController.showQuestion, sender: nil)
                    }
                    else {
                        self.performSegue(withIdentifier: R.segue.askViewController.showMessages, sender: messages)
                    }
            }
            )
        } else {
            self.performSegue(withIdentifier: R.segue.askViewController.showAuth, sender: nil)
        }
        
        
    }
}


extension AskViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth = CGFloat(width)
        let page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        pageControl.currentPage = Int(page)
    }
}


extension AskViewController: QuestionViewControllerDelegate {
    func questionViewController(controller: QuestionViewController, didCreateMessage message: Message) {
        controller.dismiss(animated: false) {
            self.performSegue(withIdentifier: R.segue.askViewController.showMessages, sender: [message])
        }
    }
}
