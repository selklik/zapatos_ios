//
//  MessagesViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/16/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MessagesViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var askButton: UIButton!
    var refreshControl: UIRefreshControl!
    var messages: [Message] = []

    var questions: [Message] {
        return messages.filter({ (message) -> Bool in
            return message.kind == "question"
        })
    }

    var answers: [Message] {
        return messages.filter({ (message) -> Bool in
            return message.kind == "answer"
        })
    }

    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set { }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        addTitlelessBackButton()

        title = "Questions & Answers"

        configureAskButton()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 58
        tableView.register(MessageTableViewCell.classForCoder(), forCellReuseIdentifier: "MessageTableViewCell")
        tableView.contentInset = UIEdgeInsets(top: 7.5, left: 0, bottom: 7.5, right: 0)

        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)

        askButton.layer.cornerRadius = 3
        bottomContainerView.layer.shadowColor = UIColor.black.cgColor
        bottomContainerView.layer.shadowOpacity = 0.25
        bottomContainerView.layer.shadowRadius = 4
        bottomContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case R.segue.messagesViewController.showQuestion.identifier:
            let questionVC = segue.destination as! QuestionViewController
            questionVC.delegate = self
        default:
            break
        }
    }

    @objc func refresh() {
        fetch(showLoader: false)
    }

    func configureAskButton() {
        askButton.isEnabled = messages.last!.answer != nil

        if askButton.isEnabled {
            askButton.alpha = 1
        }
        else {
            askButton.alpha = 0.25
        }
    }

    func fetch(showLoader: Bool) {
        if showLoader {
            HUD.show()
        }

        API.makeGETRequest(            
            to: "artists/\(kAppDelegate.artist!.id)/messages",
            completion: { json in
               // print(json)

                HUD.dismiss()
                self.refreshControl.endRefreshing()

                self.messages = json.arrayValue.map({ (json) -> Message in
                    return Message(json)
                })
                self.tableView.reloadData()
                self.configureAskButton()
            }
        )
    }

    @IBAction func askButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: R.segue.messagesViewController.showQuestion, sender: nil)
    }
}


extension MessagesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let question = questions[indexPath.row]
        var answer: Message?

        if answers.indices.contains(indexPath.row) {
            answer = answers[indexPath.row]
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageTableViewCell", for: indexPath) as! MessageTableViewCell
        cell.configure(delegate: self, question: question, answer: answer)

        return cell
    }
}


extension MessagesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !answers.indices.contains(indexPath.row) {
            return
        }
    }
}


extension MessagesViewController: MessageTableViewCellDelegate {
    func messageTableViewCell(cell: MessageTableViewCell, didTapPlayButton: UIButton, forAnswer answer: Message) {
        let player = AVPlayer(url: URL(string: answer.answer!.video.url)!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player

        present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}


extension MessagesViewController: QuestionViewControllerDelegate {
    func questionViewController(controller: QuestionViewController, didCreateMessage message: Message) {
        controller.dismiss(animated: false) {
            self.messages.append(message)
            self.tableView.reloadData()
            self.configureAskButton()
        }
    }
}
