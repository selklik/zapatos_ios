//
//  SuggestionsViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 13/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import PMAlertController
import Alamofire
import SwiftyAttributes

class FinalizeSignupViewController: UIViewController, UINavigationControllerDelegate {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var nameTextFiled: UITextField!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var finaUpView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var labelOutlet: UILabel!
    @IBOutlet weak var alertview: UIView!
    
    
    var fileURL: URL?
    var email: String!
    var password: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.init(rawValue: 2)!)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = imageView.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.addSubview(blurView)
        
        headerLabel.attributedText = "SIGN UP AT ".withFont(R.font.openSansLight(size: 14)!) + "SELKLIK".withFont(R.font.openSansBold(size: 14)!)
        
        finaUpView.layer.cornerRadius = 3
        finaUpView.layer.shadowColor = UIColor.black.cgColor
        finaUpView.layer.shadowOpacity = 0.25
        finaUpView.layer.shadowOffset = CGSize(width: 1, height: 1)
        profilePhotoImageView.layer.cornerRadius = 50
        signUpBtn.layer.cornerRadius = 3
        nameTextFiled.layer.borderWidth = 1
        nameTextFiled.layer.borderColor = kAuthBorderColor
        nameTextFiled.layer.cornerRadius = 3
        signUpBtn.isEnabled = false
        signUpBtn.alpha = 0.5
        nameTextFiled.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        
        addKeyboardDismisserTo(view: view)
        self.nameTextFiled.delegate = self;
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        UIApplication.shared.keyWindow!.rootViewController!.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func profilePhotoButton(_ sender: UIButton) {
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .actionSheet
        )
        
        alertController.addAction(UIAlertAction(
            title: "Take a Picture",
            style: .default,
            handler: { (action) in
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = .camera
                
                self.present(imagePickerController, animated: true, completion: nil)
        }
        ))
        
        alertController.addAction(UIAlertAction(
            title: "Choose from Photo Library",
            style: .default,
            handler: { (action) in
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = .photoLibrary
                
                self.present(imagePickerController, animated: true, completion: nil)
        }
        ))
        
        alertController.addAction(UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
        ))
        
        alertController.popoverPresentationController?.sourceView = alertview
        // alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        alertController.popoverPresentationController?.sourceRect = CGRect(x: alertview.bounds.midX, y: alertview.bounds.midY, width: 0, height: 0)
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func signUpBtn(_ sender: Any) {
        let parameters: Parameters = ["email": email as String, "password": password as String, "name": nameTextFiled.text!]
        print(parameters)
        
        HUD.show()
        API.makePOSTRequest(
            to: "auth/signup",
            parameters: parameters,
            completion: { (json) in
                kAppDelegate.user = User(json)
                
                if let fileURL = self.fileURL {
                    API.makeUploadRequest(
                        to: "me/profile-photos",
                        fileURL: fileURL,
                        parameters: nil,
                        completion: { (json) in
                            HUD.dismiss()
                            kAppDelegate.user!.profile_photos = Versions(json["urls"])
                            kAppDelegate.showLoadingStoryboard(launchOptions: nil)
                           
                    },
                        failure: nil
                    )
                }
                else {
                    HUD.dismiss()
                    kAppDelegate.showLoadingStoryboard(launchOptions: nil)
                }
        }
        )
        
    }
}


extension FinalizeSignupViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let path = "\(documentsDirectory)/\(Int(NSDate().timeIntervalSince1970)).png"
            if let data = UIImagePNGRepresentation(image) {
                self.fileURL = URL(fileURLWithPath: path)
                try? data.write(to: fileURL!)
                
            }
            profilePhotoImageView.image = image
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.characters.count == 1 {
            if textField.text?.characters.first == " " {
                textField.text = ""
                return
            }
        }
        
        guard let name = nameTextFiled.text, !name.isEmpty else {
            signUpBtn.isEnabled = false
            signUpBtn.alpha = 0.5;
            
            return
        }
        
        signUpBtn.isEnabled = true
        signUpBtn.alpha = 1.0;
    }
}

extension FinalizeSignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
