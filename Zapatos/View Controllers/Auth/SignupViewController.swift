//
//  SignupViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 13/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyAttributes

class SignupViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var fbSiginBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var signUpView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.init(rawValue: 2)!)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = imageView.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.addSubview(blurView)
        
        descriptionLabel.text = "Zapatos Official App is powered by Selklik, the one-stop platform that connects fans with celebrities.\n\nUsing the app you can also buy premium offical Zapatos products."
        descriptionLabel.layer.shadowColor = UIColor.black.cgColor
        headerLabel.attributedText = "SIGN UP AT ".withFont(R.font.openSansLight(size: 14)!) + "SELKLIK".withFont(R.font.openSansBold(size: 14)!)
        
        emailTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        signUpBtn.isEnabled = false
        signUpBtn.alpha = 0.5
        addKeyboardDismisserTo(view: view)
        emailTextField.delegate = self;
        passwordTextField.delegate = self;
        fbSiginBtn.layer.cornerRadius = 3
        signUpBtn.layer.cornerRadius = 3
        signUpView.layer.cornerRadius = 3
        signUpView.layer.shadowColor = UIColor.black.cgColor
        signUpView.layer.shadowOpacity = 0.25
        signUpView.layer.shadowOffset = CGSize(width: 1, height: 1)
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = kAuthBorderColor
        emailTextField.layer.cornerRadius = 3
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.borderColor = kAuthBorderColor
        passwordTextField.layer.cornerRadius = 3
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case R.segue.signupViewController.showFinalSignup.identifier:
            let finalSignup = segue.destination as! FinalizeSignupViewController
            finalSignup.email = emailTextField.text!
            finalSignup.password = passwordTextField.text!
            
        default:
            break
        }
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        let login = FBSDKLoginManager()
        
        login.logIn(
            withReadPermissions: ["public_profile", "email"],
            from: self,
            handler: { (result, error) in
                if let error = error {
                    print(error)
                }
                else if result!.isCancelled {
                    print("cancelled")
                }
                else {
                    HUD.show()
                    
                    API.makePOSTRequest(
                        to: "auth/facebook/signup",
                        parameters: ["facebook_access_token": result!.token.tokenString as String],
                        completion: { json in
                            HUD.dismiss()
                            kAppDelegate.user = User(json)
                            kAppDelegate.showLoadingStoryboard(launchOptions: nil)
                           
                    }
                    )
                }
        }
        )
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        UIApplication.shared.keyWindow!.rootViewController!.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: R.segue.signupViewController.showFinalSignup, sender: nil)
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: R.segue.signupViewController.showLogin, sender: nil)
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.characters.count == 1 {
            if textField.text?.characters.first == " " {
                textField.text = ""
                return
            }
        }
        
        guard let email = emailTextField.text, !email.isEmpty, let password = passwordTextField.text, !password.isEmpty else {
            signUpBtn.isEnabled = false
            signUpBtn.alpha = 0.5;
            
            return
        }
        
        signUpBtn.isEnabled = true
        signUpBtn.alpha = 1.0;
    }
}


extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        
        return false
    }
}
