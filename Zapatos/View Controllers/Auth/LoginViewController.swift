//
//  LoginViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 13/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//


import UIKit
import PMAlertController
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyAttributes


class LoginViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var fbloginBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var terms_conditionLabel: UILabel!
    @IBOutlet weak var closeOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        terms_conditionLabel.text = "By creating an account, you agree to Zapatos’s Terms of Use and Privacy Policy."
        
        let text = (terms_conditionLabel.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        
        let range1 = (text as NSString).range(of: "Terms of Use")
        underlineAttriString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        terms_conditionLabel.attributedText = underlineAttriString
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(tapLabel(gesture:)))
        terms_conditionLabel.addGestureRecognizer(recognizer)

        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.init(rawValue: 2)!)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = imageView.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.addSubview(blurView)
        
        emailTextField.setUnderline()
        passwordTextField.setUnderline()
        
        emailTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        loginBtn.isEnabled = false
        loginBtn.alpha = 1
        addKeyboardDismisserTo(view: view)
        emailTextField.delegate = self;
        passwordTextField.delegate = self;
        loginBtn.layer.cornerRadius = 5
        fbloginBtn.layer.cornerRadius = 5
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
       
    }
    
    
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        HUD.show()
        
        let parameters = ["email": emailTextField.text! as String, "password": passwordTextField.text! as String]
        API.makePOSTRequest(
            to: "auth/login",
            parameters: parameters,
            completion: { (json) in
                kAppDelegate.user = User(json)
                kAppDelegate.showLoadingStoryboard(launchOptions: nil)
               
                print(json)
        }
        )
    }
    
    @IBAction func closeBtn(_ sender: UIButton) {
        
        print("close close close")
        HUD.dismiss()
        self.tabBarController?.tabBar.isHidden = false
        performSegue(withIdentifier: R.segue.loginViewController.showTab, sender: nil)
    }
    @IBAction func facebookButtonTapped(_ sender: UIButton) {
        let login = FBSDKLoginManager()
        
        login.logIn(
            withReadPermissions: ["public_profile", "email"],
            from: self,
            handler: { (result, error) in
                if error != nil {
                    
                }
                else if result!.isCancelled {
                    print("cancelled")
                }
                else {
                    HUD.show()
                    
                    API.makePOSTRequest(
                        to: "auth/facebook/login",
                        parameters: ["facebook_access_token": result!.token.tokenString as String],
                        completion: { (json) in
                            HUD.dismiss()
                            
                            kAppDelegate.user = User(json)
                            kAppDelegate.showLoadingStoryboard(launchOptions: nil)
                            UserDefaults.standard.set(true, forKey: "Auth")
                    }
                    )
                }
        }
        )
    }
    
    @IBAction func forgetPasswordButtonTapped(_ sender: UIButton) {
        let alertController = PMAlertController(
            title: "Forget Password",
            description: "Please enter your email address",
            image: nil,
            style: .alert
        )
        
        alertController.addTextField { (textField) in
            textField!.placeholder = "Email address"
        }
        
        alertController.addAction(PMAlertAction(
            title: "Submit",
            style: .default,
            action: {
                HUD.show()
                
                API.makePOSTRequest(
                    to: "auth/reset-password",
                    parameters: ["email": alertController.textFields.first!.text!],
                    completion: { (json) in
                        let response = GenericResponse(json)
                        HUD.showInfo(withStatus: "Password reset instructions have been sent to \(alertController.textFields.first!.text!)")
                        print(response.email_sent)
                },
                    failure: nil
                )
        }
        ))
        
        alertController.addAction(PMAlertAction(title: "Cancel", style: .cancel))
        
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        performSegue(withIdentifier: R.segue.loginViewController.showSignup, sender: nil)
        
    }
    
    @objc func editingChanged(_ textField: UITextField) {
        if textField.text?.characters.count == 1 {
            if textField.text?.characters.first == " " {
                textField.text = ""
                return
            }
        }
        
        guard
            let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty
            
            else {
                loginBtn.isEnabled = false
                loginBtn.alpha = 0.5;
                return
        }
        
        loginBtn.isEnabled = true
        loginBtn.alpha = 1.0;
    }
    @objc func tapLabel(gesture: UITapGestureRecognizer) {
        let text = (terms_conditionLabel.text)!
        let termsRange = (text as NSString).range(of: "Terms of Use")
        let privacyRange = (text as NSString).range(of: "Privacy Policy.")
        
        if gesture.didTapAttributedTextInLabel(label: terms_conditionLabel, inRange: termsRange) {
            let webVC = R.storyboard.account.webKitViewController()!
            webVC.isModal = true
            webVC.title = "Terms of Service"
            webVC.url = URL(string: "https://myyb2.mobsocial.net/pages/tos")!
            let nc = NavigationController(rootViewController: webVC)
            
            present(nc, animated: true, completion: nil)
            
        }
        else if gesture.didTapAttributedTextInLabel(label: terms_conditionLabel, inRange: privacyRange) {
            let webVC = R.storyboard.account.webKitViewController()!
            webVC.isModal = true
            webVC.url = URL(string: "https://myyb2.mobsocial.net/pages/privacy")!
            webVC.title = "Privacy Policy"
            let nc = NavigationController(rootViewController: webVC)
            
            present(nc, animated: true, completion: nil)
        }
        else {
            // print("Tapped none")
        }
    }
}
extension UITextField {
    
    func setUnderline(){
    
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor(hexString: "#FFD700")?.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
       
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
extension LoginViewController: UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return false
    }
    
}
extension UITapGestureRecognizer {
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                          y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
                                                     y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}

