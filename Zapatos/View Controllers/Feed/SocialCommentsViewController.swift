//
//  SocialCommentsViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/13/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit

import UIScrollView_InfiniteScroll

class SocialCommentsViewController: UIViewController {
    var post: Post!
    @IBOutlet weak var tableView: UITableView!
    var comments: [SocialComment] = []
    let perPage = 25
    var refreshControl: UIRefreshControl!
    var nextPageCursorID: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        title = "\(post.type.capitalized) Comments"
        
        tableView.register(R.nib.commentTableViewCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 89
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        fetch(append: false, showLoader: true)
    }
    
    @objc func refresh() {
        fetch(append: false, showLoader: false)
    }
    
    func fetch(append: Bool, showLoader: Bool) {
        if showLoader {
            HUD.show()
        }
        
        var parameters: [String: AnyObject] = [:]
        
        if let nextPageCursorID = nextPageCursorID {
            parameters["next_page_cursor_id"] = nextPageCursorID as AnyObject?
        }
        
        print("\(post.type)-comments/\(post.metadata.post_id!)")
        
        API.makeGETRequest(            
            to: "\(post.type)-comments/\(post.metadata.post_id!)",
            parameters: parameters,
            completion: { (json) in
                let response = SocialCommentResponse(json)
                HUD.dismiss()
                self.refreshControl.endRefreshing()
                
                print(response.comments.count)
                
                self.nextPageCursorID = response.next_page_cursor_id
                
                if append {
                    if self.nextPageCursorID == nil {
                        self.removeInfiniteScroll()
                    }
                    else {
                        self.comments += response.comments
                    }
                }
                else {
                    self.comments = response.comments
                    
                    if self.comments.count == 0 {
                        let emptyView = Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)?.first as! EmptyView
                        
                        emptyView.configure(
                            image: R.image.emptyComment(),
                            firstLine: "There is no comment yet.",
                            secondLine: "Be the first one to comment!",
                            offset: nil
                        )
                        
                        self.tableView.backgroundView = emptyView
                    }
                    else {
                        self.tableView!.backgroundView = nil
                    }
                    
                    if self.comments.count == self.perPage {
                        self.addInfiniteScroll()
                    }
                }
                
                self.tableView.reloadData()

                self.tableView.finishInfiniteScroll()
        },
            failure: nil
        )
   
    }
    
    func addInfiniteScroll() {
        if post.type == "facebook" {
            tableView.addInfiniteScroll { (scrollView) in
                if self.nextPageCursorID != nil {
                    self.fetch(append: true, showLoader: false)
                }
            }
        }
    }
    
    func removeInfiniteScroll() {
        tableView!.removeInfiniteScroll()
    }
}


extension SocialCommentsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let comment = comments[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.commentTableViewCell.identifier, for: indexPath) as! CommentTableViewCell
        cell.configure(socialComment: comment)
        return cell
    }
    
}


extension SocialCommentsViewController: UITableViewDelegate {}
