//
//  PhotoViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/8/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import SnapKit
import AlamofireImage
import Alamofire

class PhotoViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    var imageView: UIImageView!
    var waterMark: UIImageView!
    var userName_W_Mak: UILabel!
    var post: Post!
    var likeButton: UIButton!
    var likeLabelButton: UIButton!
    var commentLabelButton: UIButton!
    var delegate: PhotoViewControllerDelegate!
    
    override var prefersStatusBarHidden: Bool {
        get { return true }
        set {}
    }
    
    override var modalTransitionStyle: UIModalTransitionStyle {
        get { return .crossDissolve }
        set {}
    }
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { return .custom }
        set {}
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let body = post.body {
            let bodyLabel = UILabel()
            bodyLabel.font = R.font.openSans(size: 13)
            bodyLabel.text = body
            bodyLabel.textColor = .white
            bodyLabel.numberOfLines = 2
            view.addSubview(bodyLabel)
            
            bodyLabel.snp.makeConstraints({ (make) in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)                
                make.bottom.equalToSuperview().offset(-15)
            })
            
            
            let separatorView = UIView()
            separatorView.backgroundColor = UIColor(hexString: "#222222")
            view.addSubview(separatorView)
            
            separatorView.snp.makeConstraints { (make) in
                make.height.equalTo(1)
                make.left.equalToSuperview()
                make.right.equalToSuperview()
                make.bottom.equalTo(bodyLabel.snp.top).offset(-15)
            }
        }
        
        
        likeButton = UIButton()
        likeButton.addTarget(self, action: #selector(likeButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(likeButton)
        
        likeButton.snp.makeConstraints { (make) in
            make.width.equalTo(30)
            make.height.equalTo(30)
            make.left.equalToSuperview().offset(15)
            
            if post.body != nil {
                make.bottom.equalTo(view.previousView().snp.top).offset(-15)
            }
            else {
                make.bottom.equalToSuperview().offset(-15)
            }
        }
        
        
        likeLabelButton = UIButton()
        likeLabelButton.addTarget(self, action: #selector(likeLabelButtonTapped(_:)), for: .touchUpInside)
        likeLabelButton.titleLabel!.font = R.font.openSansLight(size: 12)
        likeLabelButton.setTitleColor(.white, for: .normal)
        
        view.addSubview(likeLabelButton)
        
        likeLabelButton.snp.makeConstraints { (make) in
            make.top.equalTo(likeButton)
            make.left.equalTo(likeButton.snp.right).offset(8)
        }
        
        
        let commentButton = UIButton()
        commentButton.setImage(R.image.photoComment(), for: .normal)
        commentButton.addTarget(self, action: #selector(commentButtonTapped(_:)), for: .touchUpInside)
        
        view.addSubview(commentButton)
        
        commentButton.snp.makeConstraints { (make) in
            make.width.equalTo(30)
            make.height.equalTo(30)
            make.top.equalTo(likeLabelButton)
            make.left.equalTo(likeLabelButton.snp.right).offset(20)
        }
        
        
        commentLabelButton = UIButton()
        commentLabelButton.titleLabel!.font = R.font.openSansLight(size: 12)
        commentLabelButton.setTitleColor(.white, for: .normal)
        commentLabelButton.addTarget(self, action: #selector(commentLabelButtonTapped(_:)), for: .touchUpInside)
        
        view.addSubview(commentLabelButton)
        
        commentLabelButton.snp.makeConstraints { (make) in
            make.top.equalTo(commentButton)
            make.left.equalTo(commentButton.snp.right).offset(8)
        }
        
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0.5
        
        view.addSubview(backgroundView)
        
        backgroundView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalTo(likeButton).offset(-15)
        }
        
        view.sendSubview(toBack: backgroundView)
        view.sendSubview(toBack: scrollView)
        
        
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 6
        scrollView.contentSize = UIScreen.main.bounds.size
        
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: UIScreen.main.bounds.size.height)
        imageView.af_setImage(withURL: post.photos.first!.versions.full_resolution)      
        scrollView.addSubview(imageView)
        
        userName_W_Mak = UILabel()
        userName_W_Mak.text = (kAppDelegate.user?.email)!
        userName_W_Mak.adjustsFontSizeToFitWidth = true
        userName_W_Mak.alpha = 0.3
        userName_W_Mak.isHidden = true
        userName_W_Mak.font = UIFont(name: "openSansLight", size: 8)
        userName_W_Mak.textColor = UIColor.white
        userName_W_Mak.sizeToFit()
       // userName_W_Mak.backgroundColor = .red
        
        imageView.addSubview(userName_W_Mak)
        
        userName_W_Mak.snp.makeConstraints({ (make) in
            
            make.height.equalTo(40)
            make.width.equalTo(100)
            make.centerX.equalTo(imageView)
            make.top.equalTo(imageView).offset(50)
            
        })
        waterMark = UIImageView()
        waterMark.contentMode = .scaleAspectFit
        waterMark.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: UIScreen.main.bounds.size.height)
        waterMark.image = UIImage(named: "water_mark")
        waterMark.isHidden = true
        waterMark.alpha = 0.4
        imageView.addSubview(waterMark)
        
        waterMark.snp.makeConstraints({ (make) in
            make.height.equalTo(50)
            make.width.equalTo(50)
            make.center.equalTo(imageView)
            make.bottom.equalTo(imageView).offset(8)
        })
        switch post.type {
        case "premium":
           waterMark.isHidden = false
           userName_W_Mak.isHidden = false
        default:
            break
        }
       
        configureUI()
    }

    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func configureUI() {
        if post.liked {
            likeButton.setImage(R.image.photoLiked(), for: .normal)
        }
        else {
            likeButton.setImage(R.image.photoLike(), for: .normal)
        }
        
        likeLabelButton.setTitle(pluralForm(object: "like", count: post.like_count), for: .normal)
        commentLabelButton.setTitle(pluralForm(object: "comment", count: post.comment_count), for: .normal)
    }
    
    func pluralForm(object: String, count: Int) -> String {
        if count > 1 {
            return "\(count) \(object)s"
        }
        
        return "\(count) \(object)"
    }
    
    @objc func likeButtonTapped(_ sender: LikeButton) {
        post.liked = !post.liked
       // var method: HTTPMethod!
        
        if post.liked {
            post.like_count = post.like_count + 1
            
            API.makePOSTRequest(
                to: "posts/\(post.id)/likes",
                parameters: nil,
                completion: { (json) in
                    
                },
                failure: nil
            )
        }
        else if post.like_count - 1 > 0 {
            post.like_count = post.like_count - 1
            
            API.makeDELETERequest(                
                to: "posts/\(post.id)/likes",
                parameters: nil,
                completion: { (json) in
                    
                },
                failure: nil
            )
        }
        else {
            post.like_count = 0
            
            API.makeDELETERequest(
                to: "posts/\(post.id)/likes",
                parameters: nil,
                completion: { (json) in
                },
                failure: nil
            )
        }
        
        configureUI()
        delegate.photoViewController(controller: self, didUpdatePost: post)
    }
    
    @objc func likeLabelButtonTapped(_ sender: UIButton) {
        delegate.photoViewController(controller: self, didTapLikeLabelButton: sender, forPost: post)
    }
    
    @objc func commentButtonTapped(_ sender: UIButton) {
        delegate.photoViewController(controller: self, didTapCommentButton: sender, forPost: post)
    }
    
    @objc func commentLabelButtonTapped(_ sender: UIButton) {
        delegate.photoViewController(controller: self, didTapCommentLabelButton: sender, forPost: post)
    }
}


extension PhotoViewController: UIScrollViewDelegate {

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {

        return imageView

    }


}

