//
//  LikersViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 05/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll


class LikersViewController: UIViewController {
    var post: Post!
    var likers: [UserOrArtist] = []
    var page = 1
    var perPage = 30
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        tableView.register(R.nib.likerTableViewCell)

        refreshControl = UIRefreshControl()
        refreshControl!.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        fetch(append: false, showLoader: true)
    }
    
    @objc func refresh() {
        fetch(append: false, showLoader: false)
    }

    func fetch(append: Bool, showLoader: Bool) {
        if append == false {
            page = 1
        }

        if showLoader {
            HUD.show()
        }

        API.makeGETRequest(            
            to: "posts/\(post.id)/likes",
            parameters: ["page": page],
            completion: { (json) in
                let response = LikesResponse(json)
                HUD.dismiss()
                    self.refreshControl!.endRefreshing()
    
                    if append {
                        if response.liked_by.count == 0 {
                            self.removeInfiniteScroll()
                        }
                        else {
                            self.likers += response.liked_by
                        }
                    }
                    else {
                        self.likers = response.liked_by
    
                        if self.likers.count == 0 {
                            let emptyView = Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)?.first as! EmptyView
    
                            emptyView.configure(
                                image: R.image.emptyComment(),
                                firstLine: "There is no likes yet.",
                                secondLine: "Be the first one to like!",
                                offset: nil
                            )
    
                            self.tableView.backgroundView = emptyView
                        }
                        else {
                            self.tableView!.backgroundView = nil
                        }
    
                        if self.likers.count == self.perPage {
                            self.addInfiniteScroll()
                        }
                    }
    
                    self.tableView.reloadData()
                    self.tableView.finishInfiniteScroll()
        },
            failure: nil)

    }
    
    func addInfiniteScroll() {
        tableView.addInfiniteScroll { (scrollView) in
            self.page += 1
            self.fetch(append: true, showLoader: false)
        }
    }
    
    func removeInfiniteScroll() {
        tableView.removeInfiniteScroll()
    }
    
}


extension LikersViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let liker = likers[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.likerTableViewCell.identifier, for: indexPath) as! LikerTableViewCell
        cell.configure(liker: liker)
        
        return cell
    }
    
}

extension LikersViewController: UITableViewDelegate {}

