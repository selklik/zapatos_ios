//
//  FeedViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/5/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import SwiftyAttributes
import UIScrollView_InfiniteScroll
import Alamofire
import AVKit
import AVFoundation
import SnapKit

class FeedViewController: PostsViewController {
    override var parameters: Parameters {
        get { return ["type": type] }
    }
    
    override var shouldFetchPosts: Bool {
        return !isSingle
    }
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return isSingle }
        set {}
    }
    
    var isSingle = false
    var isModal = false

    var type: String {
        get {
            let selectedFilter = UserDefaults.standard.integer(forKey: "selectedFilter")
            
            switch selectedFilter {
            case 0:
                return ""
                
            case 1:
                return "premium"
                
            case 2:
                return "social"
                
            case 3:
                return "qna"
                
            default:
                return "qna"
            }
        }

        set(val) {
            var selectedFilter: Int!
            
            switch val {
            case "":
                selectedFilter = 0

            case "premium":
                selectedFilter = 1

            case "social":
                selectedFilter = 2
                
            case "qna":
                selectedFilter = 3
            default:
                break
            }
            
            UserDefaults.standard.set(selectedFilter, forKey: "selectedFilter")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if isSingle {
            title = "Post"
            
            if isModal {
                navigationItem.leftBarButtonItem = UIBarButtonItem(
                    barButtonSystemItem: .stop,
                    target: self,
                    action: #selector(closeBarButtonTapped(_:))
                )
            }
        }
        else {
            let feedTitleView = R.nib.feedTitleView.firstView(owner: nil)!
            feedTitleView.configure(delegate: self)
            navigationItem.titleView = feedTitleView
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        switch segue.identifier! {

        case R.segue.feedViewController.showFilter.identifier:
            let filterVC = segue.destination as! FilterViewController
            filterVC.delegate = self

        default:
            break
        }
    }
    
    @objc func closeBarButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}


extension FeedViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = posts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedTableViewCell", for: indexPath) as! FeedTableViewCell
        cell.configure(delegate: self, post: post)

        return cell
    }
}


extension FeedViewController: UITableViewDelegate { }

extension FeedViewController: FilterViewControllerDelegate {
    func filterViewController(controller: FilterViewController, didSelectFilter selectedFilter: Int) {
        switch selectedFilter {
        case 0:
            type = ""

        case 1:
            type = "premium"

        case 2:
            type = "social"
            
        case 3:
            type = "qna"

        default:
            break
        }
        
        let feedTitleView = navigationItem.titleView as! FeedTitleView
        feedTitleView.configureUI()

        fetchFromAPI(append: false, showLoader: true)
    }
}

extension FeedViewController: FeedTitleViewDelegate {
    func feedTitleView(view: FeedTitleView, didTapButton button: UIButton) {
        performSegue(withIdentifier: R.segue.feedViewController.showFilter, sender: nil)
    }
}
