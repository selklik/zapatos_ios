//
//  PostsViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/7/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import AVKit
import SafariServices
import SwiftyAttributes

class PostsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var viewAction: FeedTableViewCell!
    var posts: [Post] = []
    var page = 1
    let perPage = 20
    var refreshControl: UIRefreshControl?
    
    var parameters: Parameters {
        get { return [:] }
    }

    var postsSection: Int {
        get { return 0 }
    }

    var shouldFetchFromCache: Bool {
        get { return false }
    }
    
    var shouldFetchPosts: Bool {
        get { return true }
    }

    var module: String {
        get { return "artists/\(kArtistID)/posts" }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        addTitlelessBackButton()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
        tableView.register(FeedTableViewCell.classForCoder(), forCellReuseIdentifier: "FeedTableViewCell")

        if shouldFetchPosts {
            refreshControl = UIRefreshControl()
            refreshControl!.addTarget(self, action: #selector(refresh), for: .valueChanged)
            tableView.addSubview(refreshControl!)
            
            if shouldFetchFromCache {
                fetchFromCache()
            }
            else {
                fetchFromAPI(append: false, showLoader: true)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {

        case R.segue.feedViewController.showPhoto.identifier:
            let photoVC = segue.destination as! PhotoViewController
            photoVC.post = sender as! Post
            photoVC.delegate = self

        case R.segue.feedViewController.showPurchase.identifier:
            let purchaseVC = segue.destination as! PurchaseViewController
            purchaseVC.post = sender as! Post
            purchaseVC.delegate = self

        case R.segue.feedViewController.showSocialComments.identifier:
            let socialCommentsVC = segue.destination as! SocialCommentsViewController
            socialCommentsVC.post = sender as! Post

        case R.segue.feedViewController.showSocialLikers.identifier:
            let socialLikers = segue.destination as! SocialLikersViewController
            socialLikers.post = sender as! Post

        case R.segue.feedViewController.showComments.identifier:
            let arr = sender as! [Any]
            let commentsVC = segue.destination as! CommentsViewController
            commentsVC.shouldShowKeyboard = arr[0] as! Bool
            commentsVC.post = arr[1] as! Post

        case R.segue.feedViewController.showLikers.identifier:
            let likersVC = segue.destination as! LikersViewController
            likersVC.post = sender as! Post

        case R.segue.feedViewController.showReport.identifier:
            let nc = segue.destination as! NavigationController
            let reportVC = nc.viewControllers.first as! ReportViewController
            reportVC.post = sender as? Post

        default:
            break
        }
    }

    @objc func refresh() {
        fetchFromAPI(append: false, showLoader: false)
    }

    func addInfiniteScroll() {
        tableView.addInfiniteScroll { scrollView in
            self.page += 1
            self.fetchFromAPI(append: true, showLoader: false)
        }
    }

    func removeInfiniteScroll() {
        tableView.removeInfiniteScroll()
    }

    func fetchFromCache() {
        print("fetching from API...")
        fetchFromAPI(append: false, showLoader: true)
    }

    func saveToCache() {
        if !shouldFetchFromCache { return }

        var postsToBeSaved: [Post]!

        if posts.count > perPage {
            postsToBeSaved = [Post](posts[0...(perPage - 1)])
        }
        else {
            postsToBeSaved = posts
        }

        print("Saved \(postsToBeSaved.count) posts")
    }

    func fetchFromAPI(append: Bool, showLoader: Bool) {
        if append == false {
            page = 1
        }

        if showLoader {
            HUD.show()            
        }
        
        var appendedParameters = parameters
        appendedParameters["page"] = page

        API.makeGETRequest(
            to: module,
            parameters: appendedParameters,
            completion: { json in
                HUD.dismiss()
                if let refreshControl = self.refreshControl {
                    refreshControl.endRefreshing()
                }

                let posts = json.arrayValue.map({ (json) -> Post in
                    return Post(json)
                })                

                if append {
                    if posts.count == 0 {
                        self.removeInfiniteScroll()
                    }
                    else {
                        self.posts += posts
                    }
                }
                else {
                    self.posts = posts
                    self.configureBackground()

                    if posts.count == self.perPage {
                        self.addInfiniteScroll()
                    }

                    self.saveToCache()
                }

                self.tableView.reloadData()
                self.tableView.finishInfiniteScroll()

                if append == false {
                    if self.posts.count > 0 {
                        let indexPath = IndexPath(row: 0, section: self.postsSection)
                        //self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
                    }
                }
            }
        )
    }

    func configureBackground() {
        if posts.count == 0 {
            let emptyView = R.nib.emptyView.firstView(owner: nil)!

            emptyView.configure(
                image: R.image.emptyFeed(),
                firstLine: "There is no post yet.",
                secondLine: "Try selecting a diffrent filter, perhaps?",
                offset: nil
            )

            self.tableView.backgroundView = emptyView
            self.tableView.tableHeaderView = nil
        }
        else {
            if UserDefaults.standard.bool(forKey: "feed_tutorial_completed") {
                self.tableView.tableHeaderView = nil
                self.tableView.backgroundView = nil
                
                return
            }
            
            let feedHeaderView = R.nib.feedHeaderView.firstView(owner: nil)!
            let font = R.font.openSansSemibold(size: 11)!
            var text = "We aggregate \(kArtistShortName)'s social media posts from ".attributedString
            text = text + "Facebook".withFont(font) + ", ".attributedString + "Twitter".withFont(font)
            text = text + " and ".attributedString + "Instagram".withFont(font) + ". You can also see premium, exclusive posts that can never be found anywhere else but here!".attributedString
            feedHeaderView.configure(
                text: text,
                delegate: self
            )
            
            self.tableView.tableHeaderView = feedHeaderView
            self.tableView.backgroundView = nil
        }
    }
}


extension PostsViewController: FeedTableViewCellDelegate {
    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTryToOpenURL url: URL) {
        let safariVC = SFSafariViewController(url: url)
        safariVC.view.tintColor = kTextColor

        present(safariVC, animated: true, completion: nil)
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapQnaPlayButton button: UIButton, onPost post: Post) {
        if post.metadata.full_access {
            let player = AVPlayer(url: post.attachment!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player

            present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
        else {
            API.makeGETRequest(
                to: "me",
                completion: { json in
                    HUD.dismiss()
                    let user = User(json)
                    kAppDelegate.user = user
                    self.performSegue(withIdentifier: R.segue.feedViewController.showPurchase.identifier, sender: post)
                }
            )
        }
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapOptionsButton button: UIButton, onPost post: Post) {
        let alertController = UIAlertController(
            title: "Post Options",
            message: nil,
            preferredStyle: .actionSheet
        )

        alertController.addAction(UIAlertAction(
            title: "Report post",
            style: .default,
            handler: { (action) in
                self.performSegue(withIdentifier: R.segue.feedViewController.showReport.identifier, sender: post)
            }
            ))

        alertController.addAction(UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
            ))
       
        alertController.popoverPresentationController?.sourceView = cell.viewOption
      
        present(alertController, animated: true, completion: nil)
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapCommentButton button: UIButton, onPost post: Post) {
        performSegue(withIdentifier: R.segue.feedViewController.showComments.identifier, sender: [true, post])
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapCommentLabelButton button: UIButton, onPost post: Post) {
        performSegue(withIdentifier: R.segue.feedViewController.showComments.identifier, sender: [false, post])
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapPhotoImageView post: Post) {
        guard let attachment_type = post.attachment_type else { return }

        switch post.type {
        case "premium":
            if post.metadata.full_access {
                switch attachment_type {
                case "video":
                    
                    let player = AVPlayer(url: post.attachment!)
                    let playerViewController = CustomAVPlayerViewController()
                    playerViewController.player = player

                    present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }

                case "photo", "link":
                    performSegue(withIdentifier: R.segue.feedViewController.showPhoto.identifier, sender: post)

                default:
                    break
                }
            }
            else {
                HUD.show()
                API.makeGETRequest(
                    to: "me",
                    parameters: nil,
                    completion: { (json) in
                        let user = User(json)
                        HUD.dismiss()

                        kAppDelegate.user = user
                        self.performSegue(withIdentifier: R.segue.feedViewController.showPurchase.identifier, sender: post)
                    },
                    failure: nil
                )
            }

        default:
            switch attachment_type {
            case "video":
                let player = AVPlayer(url: post.attachment!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player

                present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }

            case "photo", "link":
                performSegue(withIdentifier: R.segue.feedViewController.showPhoto.identifier, sender: post)

            default:
                break
            }
        }
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapSocialMediaButtonOnPost post: Post) {
        let alertController = UIAlertController(
            title: nil,
            message: nil,
            preferredStyle: .actionSheet
        )

        alertController.addAction(UIAlertAction(
            title: "View on \(post.type.capitalized)",
            style: .default,
            handler: { (action) in
                var url: URL!

                switch post.type {
                case "twitter":
                    url = URL(string: "https://twitter.com/\(post.metadata.account_username!)/status/\(post.metadata.post_id!)")!

                case "facebook":
                    url = URL(string: "https://facebook.com/\(post.metadata.post_id!)")!

                case "instagram":
                    url = URL(string: "https://www.instagram.com/p/\(post.metadata.post_id!)/")!

                default:
                    break
                }
              
                
                let safariVC = SFSafariViewController(url: url)
                safariVC.view.tintColor = kTextColor
                self.present(safariVC, animated: true, completion: nil)
            }
            ))

        alertController.addAction(UIAlertAction(
            title: "View \(post.type.capitalized) comments (\(post.metadata.comment_count))",
            style: .default,
            handler: { (action) in
                self.performSegue(withIdentifier: R.segue.feedViewController.showSocialComments.identifier, sender: post)
            }
            ))

        alertController.addAction(UIAlertAction(
            title: "View \(post.type.capitalized) likes (\(post.metadata.like_count))",
            style: .default,
            handler: { (action) in
                self.performSegue(withIdentifier: R.segue.feedViewController.showSocialLikers.identifier, sender: post)
            }
            ))

        alertController.addAction(UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil
            ))
        alertController.popoverPresentationController?.sourceView = cell.viewSocial
        present(alertController, animated: true, completion: nil)
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapArtist artist: Artist) {
        //performSegue(withIdentifier: R.segue.feedViewController.showArtist.identifier, sender: artist)
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapLikeButtonOnPost post: Post) {
        if post.liked {
            //method = .post
            API.makePOSTRequest(
                to: "posts/\(post.id)/likes",
                completion: { (json) in
                })
        }
        else {
            //method = .delete
            API.makeDELETERequest(
                to: "posts/\(post.id)/likes",
                completion: { (json) in
            })
        }
    }

    func feedTableViewCellDelegate(cell: FeedTableViewCell, didTapLikeLabelButton button: UIButton, onPost post: Post) {
        performSegue(withIdentifier: R.segue.feedViewController.showLikers.identifier, sender: post)
    }
}


extension PostsViewController: PhotoViewControllerDelegate {
    func photoViewController(controller: PhotoViewController, didUpdatePost post: Post) {
        tableView.reloadData()
    }

    func photoViewController(controller: PhotoViewController, didTapLikeLabelButton button: UIButton, forPost post: Post) {
        dismiss(animated: true) {
            self.performSegue(withIdentifier: R.segue.feedViewController.showLikers.identifier, sender: post)
        }
    }

    func photoViewController(controller: PhotoViewController, didTapCommentButton button: UIButton, forPost post: Post) {
        dismiss(animated: true) {
            self.performSegue(withIdentifier: R.segue.feedViewController.showComments.identifier, sender: [true, post])
        }
    }

    func photoViewController(controller: PhotoViewController, didTapCommentLabelButton button: UIButton, forPost post: Post) {
        dismiss(animated: true) {
            self.performSegue(withIdentifier: R.segue.feedViewController.showComments.identifier, sender: [false, post])
        }
    }
}


extension PostsViewController: PurchaseViewControllerDelegate {
    func purchaseViewController(controller: PurchaseViewController, didPurchasedPost post: Post, withPurchase purchase: Purchase) {
        tableView.reloadData()
    }
}


extension PostsViewController: FeedHeaderViewDelegate {
    func feedHeaderView(view: FeedHeaderView, didTapButton button: UIButton) {
        UserDefaults.standard.set(true, forKey: "feed_tutorial_completed")
        tableView.tableHeaderView = nil
    }
}
