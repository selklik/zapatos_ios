//
//  PurchaseViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 12/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

import SwiftyAttributes

class PurchaseViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    var post: Post!
    @IBOutlet weak var photoImageView: UIImageView!
    var delegate: PurchaseViewControllerDelegate!

    override var modalPresentationStyle: UIModalPresentationStyle {
        get { return .custom }
        set { }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        containerView.layer.cornerRadius = 3

        buttons.enumerated().forEach { (index, button) in
            button.layer.cornerRadius = 3

            if index == 0 {
                button.layer.borderWidth = 1
                button.layer.borderColor = kSecondaryTextColor!.cgColor
            }
        }

        nameLabel.attributedText = "Purchase this from ".attributedString + post.artist.name.withFont(R.font.openSansBold(size: 14)!) + "?".attributedString

        if post.attachment_type == "video" {
            photoImageView.af_setImage(withURL: post.photos.first!.versions.full_resolution)
        }
        else {
            photoImageView.af_setImage(withURL: post.photos.first!.versions.preview!)
        }

        configureUI()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case R.segue.purchaseViewController.showReload.identifier:
            let nc = segue.destination as! NavigationController
            let reloadVC = nc.viewControllers.first as! ReloadViewController
            reloadVC.isModal = true
            reloadVC.delegate = self

        default:
            break
        }
    }

    func configureUI() {
        if let user = kAppDelegate.user {
            creditLabel.text = "\(kAppDelegate.user!.current_balance.withComma)"

            if user.current_balance >= Int(post.metadata.price) {
                infoLabel.text = "Your Stars balance:"
                buttons[1].setTitle("Purchase (\(Int(post.metadata.price).withComma) Stars)", for: .normal)
            }
            else {
                infoLabel.text = "Insufficient Stars!"
                buttons[1].setTitle("Reload Stars", for: .normal)
            }
        }
    }

    @IBAction func actionButtonTapped(_ sender: UIButton) {
        guard let user = kAppDelegate.user else { return }

        if user.current_balance >= Int(post.metadata.price) {
            HUD.show()

            var module: String!

            switch post.type {
            case "premium":
                module = "posts/\(post.id)/purchases"

            case "qna":
                module = "question-and-answers/\(post.metadata.question_and_answer_id)/purchases"

            default:
                break
            }

            API.makePOSTRequest(                
                to: module,
                parameters: nil,
                completion: { (json) in
                    let purchase = Purchase(json)
                    HUD.dismiss()                    
                    HUD.showInfo(withStatus: "Purchased successfuly")

                    self.post.metadata.full_access = true
                    self.delegate.purchaseViewController(controller: self, didPurchasedPost: self.post, withPurchase: purchase)
                    self.dismiss(animated: true, completion: nil)

                },
                failure: nil
            )
        }
        else {
            performSegue(withIdentifier: R.segue.purchaseViewController.showReload, sender: nil)
        }
    }

    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}


extension PurchaseViewController: ReloadViewControllerDelegate {
    func reloadViewController(controller: ReloadViewController, didPurchaseTopup topup: IOSTopup) {
        HUD.show()

        API.makeGETRequest(
            to: "me",
            parameters: nil,
            completion: { (json) in
                let user = User(json)
                HUD.dismiss()
                HUD.showInfo(withStatus: "Purchased successfuly")

                kAppDelegate.user = user                
                self.configureUI()
            },
            failure: nil)

    }
}

