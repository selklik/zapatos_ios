//
//  CommentsViewController.swift
//  Zain Saidin
//  Created by MacBook Air on 05/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

import UIKit
import UIScrollView_InfiniteScroll

import SlackTextViewController

class CommentsViewController: SLKTextViewController {    
    var post: Post!
    var comments: [Comment] = []
    var shouldShowKeyboard = false
    var page = 1
    let perPage = 10
    var refreshControl: UIRefreshControl!
    
    override var isInverted: Bool {
        get { return false }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView!.separatorStyle = .none
        tableView!.register(R.nib.commentTableViewCell)
        tableView!.tableFooterView = UIView()
        tableView!.rowHeight = UITableViewAutomaticDimension
        tableView!.estimatedRowHeight = 89
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView!.addSubview(refreshControl)
        
        textView.placeholder = "Write a comment"
        textInputbar.isTranslucent = false
        textInputbar.backgroundColor = UIColor.groupTableViewBackground
        
        fetch(append: false, showLoader: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if shouldShowKeyboard {
            textView.becomeFirstResponder()
        }
    }
    
    @objc func refresh() {
        fetch(append: false, showLoader: false)
    }
    
    func fetch(append: Bool, showLoader: Bool) {
        if append == false {
            page = 1
        }

        if showLoader {
            HUD.show()
        }
        API.makeGETRequest(            
            to: "posts/\(post.id)/comments",
            parameters: ["page": page],
            completion: { (json) in
                let response = CommentsResponse(json)
                HUD.dismiss()
                self.refreshControl.endRefreshing()

                if append {
                    if response.comments.count == 0 {
                        self.removeInfiniteScroll()
                    }
                    else {
                        self.comments += response.comments
                    }
                }
                else {
                    self.comments = response.comments

                    if self.comments.count == 0 {
                        self.configureBackground()
                    }
                    else {
                        self.tableView!.backgroundView = nil
                    }

                    if self.comments.count == self.perPage {
                        self.addInfiniteScroll()
                    }
                }

                self.tableView!.reloadData()
                self.tableView!.finishInfiniteScroll()
        },
            failure: nil)

    }
    
    func addInfiniteScroll() {
        tableView!.addInfiniteScroll { (scrollView) in
            self.page += 1
            self.fetch(append: true, showLoader: false)
        }
    }
    
    func removeInfiniteScroll() {
        tableView!.removeInfiniteScroll()
    }
    
    func configureBackground() {
        let emptyView = Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)?.first as! EmptyView
        
        emptyView.configure(
            image: R.image.emptyComment(),
            firstLine: "There is no comment yet.",
            secondLine: "Be the first one to comment!",
            offset: nil
        )
        
        tableView!.backgroundView = emptyView
    }
    
    override func didPressRightButton(_ sender: Any?) {
        textView.refreshFirstResponder()
        textView.resignFirstResponder()

        let user = kAppDelegate.user!

        let from = UserOrArtist(
            id: user.id,
            name: user.name,
            profile_photos: user.profile_photos,
            notification: user.notification,
            followers_count: 0
        )

        let comment = Comment(
            id: 0,
            from: from,
            owner: true,
            body: textView.text,
            timestamp: ""
        )
//        comment.body = textView.text
//        comment.from = from

        comments.append(comment)
        tableView!.reloadData()

        API.makePOSTRequest(            
            to: "posts/\(post.id)/comments",
            parameters:["body": comment.body],
            completion: { (json) in
                let savedComment = Comment(json)
                
                let index = self.comments.index(where: { (c) -> Bool in
                    return comment.body == c.body
                })
                
                //let index = self.comments.index(of: comment)!
                self.comments[index!] = savedComment
                let indexPath = IndexPath(row: index!, section: 0)
                self.tableView!.reloadRows(at: [indexPath], with: .none)
             
                if self.comments.count == 1 {
                    self.tableView?.backgroundView = nil
                }
            },
            failure: nil
        )

        super.didPressRightButton(sender)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let comment = comments.reversed()[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.commentTableViewCell.identifier, for: indexPath) as! CommentTableViewCell
        cell.configure(comment: comment)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let comment = comments[indexPath.row]
        
        if comment.id == 0 {
            return false
        }
        
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let comment = comments[indexPath.row]
        print(comment.owner)
        if comment.id == 0 {
            return
        }
        
        if comment.owner {
            let alertController = UIAlertController(
                title: "Comment Options",
                message: nil,
                preferredStyle: .actionSheet
            )
        alertController.addAction(UIAlertAction(
            title: "Delete Comment",
            style: .default,
            handler: { (action) in
                if let indexPath = self.tableView!.indexPathForSelectedRow {
                    self.tableView!.deselectRow(at: indexPath, animated: true)
                }
                API.makeDELETERequest(
                    to: "me/posts/\(self.post.id)/comments/\(comment.id)",
                    parameters: nil,
                    completion: { (json) in
                        
                },
                    failure: nil)
                
                self.tableView!.beginUpdates()
                self.comments.remove(at: indexPath.row)
                self.tableView!.deleteRows(at: [indexPath], with: .fade)
                self.tableView!.endUpdates()
                
                
                print("delete")
        }))
            alertController.addAction(UIAlertAction(
                title: "Cancel",
                style: .cancel,
                handler: { (action) in
                    if let indexPath = self.tableView!.indexPathForSelectedRow {
                        self.tableView!.deselectRow(at: indexPath, animated: true)
                    }
            }
            ))
            
            present(alertController, animated: true, completion: nil)
    }
       
    }
    
    
}

