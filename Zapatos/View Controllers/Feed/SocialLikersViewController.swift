//
//  SocialLikersViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/13/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll


class SocialLikersViewController: UIViewController {
    var post: Post!
    @IBOutlet weak var tableView: UITableView!
    var likers: [SocialLiker] = []
    let perPage = 25
    var refreshControl: UIRefreshControl!
    var nextPageCursorID: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "\(post.type.capitalized) Likes"
        
        tableView.register(R.nib.likerTableViewCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 66
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        fetch(append: false, showLoader: true)
    }

    @objc func refresh() {
        fetch(append: false, showLoader: false)
    }
    
    func fetch(append: Bool, showLoader: Bool) {
        if showLoader {
            HUD.show()
        }
        
        var parameters: [String: AnyObject] = [:]
        
        if let nextPageCursorID = nextPageCursorID {
            parameters["next_page_cursor_id"] = nextPageCursorID as AnyObject?
        }
        API.makeGETRequest(            
            to: "\(post.type)-likes/\(post.metadata.post_id!)",
            parameters: parameters,
            completion: { (json) in
                let response = SocialLikeResponse(json)
                HUD.dismiss()
                self.refreshControl.endRefreshing()
                self.nextPageCursorID = response.next_page_cursor_id
                
                if append {
                    if self.nextPageCursorID == nil {
                        self.removeInfiniteScroll()
                    }
                    else {
                        self.likers += response.liked_by
                    }
                }
                else {
                    self.likers = response.liked_by
                    
                    if self.likers.count == 0 {
                        let emptyView = Bundle.main.loadNibNamed("EmptyView", owner: self, options: nil)?.first as! EmptyView
                        
                        emptyView.configure(
                            image: R.image.emptyComment(),
                            firstLine: "There is no likes yet.",
                            secondLine: "Be the first one to like!",
                            offset: nil
                        )
                        
                        self.tableView.backgroundView = emptyView
                    }
                    else {
                        self.tableView!.backgroundView = nil
                    }
                    
                    if self.likers.count == self.perPage {
                        self.addInfiniteScroll()
                    }
                }
                
                self.tableView.reloadData()
                self.tableView.finishInfiniteScroll()
        },
            failure: nil)
        
    }
    
    func addInfiniteScroll() {
        if post.type == "facebook" {
            tableView.addInfiniteScroll { (scrollView) in
                if self.nextPageCursorID != nil {
                    self.fetch(append: true, showLoader: false)
                }
            }
        }
    }
    
    func removeInfiniteScroll() {
        tableView!.removeInfiniteScroll()
    }
    
}


extension SocialLikersViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let liker = likers[indexPath.row]
                
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.likerTableViewCell.identifier, for: indexPath) as! LikerTableViewCell
        cell.configure(socialLiker: liker)
        
        return cell
    }
    
}


extension SocialLikersViewController: UITableViewDelegate {}
