//
//  FilterViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 14/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { return .custom }
        set {}
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIView!
    
    
    var delegate: FilterViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.cornerRadius = 6
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(backgroundViewTapped(_:)))
        backgroundView.addGestureRecognizer(recognizer)
    }
    
    @objc func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
        dismiss(animated: false, completion: nil)
    }    
}


extension FilterViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.filterTableViewCell, for: indexPath)!
        cell.configure(row: indexPath.row)
        
        return cell
    }
}

extension FilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {        
        tableView.reloadData()
        tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        delegate.filterViewController(controller: self, didSelectFilter: indexPath.row)
        dismiss(animated: false, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

