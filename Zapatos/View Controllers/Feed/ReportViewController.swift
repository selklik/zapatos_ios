//
//  ReportViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 09/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

import Alamofire

class ReportViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var post: Post?
    var artist: Artist?
    var reason: String?
    var block = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submitButtonTapped(_ sender: UIBarButtonItem) {
        tableView.endEditing(true)
        HUD.show()
        
        let parameters: Parameters = [
            "block": block.description,
            "reason": reason!
        ]
        
        if let post = post {
            API.makePOSTRequest(
                to: "posts/\(post.id)/reports",
                parameters: parameters,
                completion: { (json) in
                    HUD.showInfo(withStatus: "Report sent successfully")
                    self.dismiss(animated: true, completion: nil)
                },
                failure: nil
            )
        }
        else if let artist = artist {
            API.makePOSTRequest(
                to: "artists/\(artist.id)/reports",
                parameters: parameters,
                completion: { (json) in
                    HUD.showInfo(withStatus: "Report sent successfully")                
                    self.dismiss(animated: true, completion: nil)
            },
                failure: nil
            )

        }
    }
    
}


extension ReportViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.reportReasonTableViewCell, for: indexPath)!
            cell.configure(delegate: self)
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.reportBlockTableViewCell, for: indexPath)!
            cell.configure(delegate: self, post: post, artist: artist)
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 100
            
        default:
            return 44
        }
    }
    
}


extension ReportViewController: UITableViewDelegate {}


extension ReportViewController: ReportReasonTableViewCellDelegate {
    
    func reportReasonTableViewCell(cell: ReportReasonTableViewCell, didTypeReason reason: String?) {
        self.reason = reason
        navigationItem.rightBarButtonItem?.isEnabled = reason != nil
    }
    
}


extension ReportViewController: ReportBlockTableViewCellDelegate {
    
    func reportBlockTableViewCell(cell: ReportBlockTableViewCell, didSetBlock block: Bool) {
        print("Block: \(block)")
        self.block = block
    }
    
}

