//
//  LoadingViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit
import NVActivityIndicatorView

class LoadingViewController: UIViewController {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let circularView = UIView()
        circularView.backgroundColor = UIColor(hexString: "#ffffff", alpha: 0.1)
        circularView.layer.cornerRadius = 35
        view.addSubview(circularView)
        
        circularView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(70)
            make.height.equalTo(70)
        }
        
        let activityIndicatorView = NVActivityIndicatorView(
            frame: .zero,
            type: .ballSpinFadeLoader,
            color: kTabColor,
            padding: nil
        )
        
        activityIndicatorView.startAnimating()
        
        view.addSubview(activityIndicatorView)
        
        activityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(50)
            make.height.equalTo(50)
        }
       

         self.performSegue(withIdentifier: R.segue.loadingViewController.showTab, sender: nil)
}
}
