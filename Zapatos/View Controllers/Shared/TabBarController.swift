//
//  TabBarController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 23/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SwiftHEXColors
import CoreData

class TabBarController: UITabBarController {
 
    let menuButton = UIButton()
    var addOrder = [OrderList]()
    let countLabel = UILabel()
    let coredataOperations = PersistenceService()
    let launchedBefore = UserDefaults.standard.bool(forKey: "Auth")
    
    func tutorial() {
        
//        acoountTutorialView.isHidden = true
//        profileView.isHidden = true
//
//        tutroialSosView.isHidden = true
//        SOSBtnVies.isHidden = true
       

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        catchData()
        setupMiddleButton()
        tabBar.unselectedItemTintColor = kSecondaryTextColor
        menuButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
      
        if launchedBefore{
            
            let homeVC = R.storyboard.home.instantiateInitialViewController()!
            homeVC.tabBarItem.title = "Home"
            homeVC.tabBarItem.image = R.image.home()
            homeVC.tabBarItem.selectedImage = R.image.home()
            
            let storeVC = R.storyboard.shoestore.instantiateInitialViewController()!
            storeVC.tabBarItem.title = "Store"
            storeVC.tabBarItem.image = R.image.store()
            storeVC.tabBarItem.selectedImage = R.image.store()
            
            let feedVC = R.storyboard.feed.instantiateInitialViewController()!
            feedVC.tabBarItem.title = "Feed"
            feedVC.tabBarItem.image = R.image.feed()
            feedVC.tabBarItem.selectedImage = R.image.feed()
            
            
            let accountVC = R.storyboard.account.instantiateInitialViewController()!
            accountVC.tabBarItem.title = "Account"
            accountVC.tabBarItem.image = R.image.account()
            accountVC.tabBarItem.selectedImage = R.image.account()
            
            let askVC = R.storyboard.ask.instantiateInitialViewController()!
            askVC.tabBarItem.title = "Ask \(kArtistShortName)"
            askVC.tabBarItem.image = R.image.ask()
            askVC.tabBarItem.selectedImage = R.image.ask()
            
            viewControllers = [homeVC, storeVC, feedVC, askVC, accountVC]
            menuButton.isHidden = false
            
         
          //  performSegue(withIdentifier: R.storyboard.auth().instantiateInitialViewController(), sender: nil)
        }
            
        else
            
        {

            let homeVC = R.storyboard.home.instantiateInitialViewController()!
            homeVC.tabBarItem.title = "Home"
            homeVC.tabBarItem.image = R.image.home()
            homeVC.tabBarItem.selectedImage = R.image.home()
            
            let storeVC = R.storyboard.shoestore.instantiateInitialViewController()!
            storeVC.tabBarItem.title = "Store"
            storeVC.tabBarItem.image = R.image.store()
            storeVC.tabBarItem.selectedImage = R.image.store()
            
            let feedVC = R.storyboard.ask.instantiateInitialViewController()!
            feedVC.tabBarItem.title = "Feed"
            feedVC.tabBarItem.image = R.image.feed()
            feedVC.tabBarItem.selectedImage = R.image.feed()
            menuButton.isHidden = true
            
            let accountVC = R.storyboard.auth.instantiateInitialViewController()!
            accountVC.tabBarItem.title = "Account"
            accountVC.tabBarItem.image = R.image.account()
            accountVC.tabBarItem.selectedImage = R.image.account()
            menuButton.isHidden = true
            
            let askVC = R.storyboard.ask.instantiateInitialViewController()!
            askVC.tabBarItem.title = "Ask \(kArtistShortName)"
            askVC.tabBarItem.image = R.image.ask()
            askVC.tabBarItem.selectedImage = R.image.ask()
            menuButton.isHidden = true
            
            viewControllers = [homeVC, storeVC, feedVC, askVC, accountVC]
           print("ssdsdsds")
            
        }
        

        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        catchData()
    }
    
    @objc func buttonAction(sender: UIButton!) {
        if addOrder.count == 0 {
           
            let storyBoard: UIStoryboard = UIStoryboard(name: "Shoestore", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "emptyCart") as! NavigationController
            self.present(newViewController, animated: true, completion: nil)

        } else {
           
            performSegue(withIdentifier: R.segue.tabBarController.showCart, sender: nil)
        }
       
    }
    
    func catchData() {
        let fetchRequest: NSFetchRequest<OrderList> = OrderList.fetchRequest()
        
        do {
            let people = try PersistenceService.context.fetch(fetchRequest)
            self.addOrder = people
            print(people.count,"count")
            countLabel.text = String(people.count)
            getData()
        } catch {}
    }
    
    func getData() {
        let contex = PersistenceService.persistentContainer.viewContext
        do {
            addOrder = try contex.fetch(OrderList.fetchRequest())
            
        } catch {
            print("Error not get data")
        }
        
    }

    func setupMiddleButton() {
      //  countLabel.text = "0"
        countLabel.backgroundColor = .black
        countLabel.textColor = zapatosColor
        countLabel.textAlignment = .center
        countLabel.font = R.font.openSansBold(size: 13)
        self.menuButton.addSubview(countLabel)
        countLabel.layer.cornerRadius = 10
        countLabel.clipsToBounds = true
        
        countLabel.snp.makeConstraints { (make) in
            // make.top.bottom.equalTo(view)
            make.top.equalTo(menuButton).offset(-3)
            make.right.equalTo(menuButton).offset(4)
            make.width.equalTo(20)
            make.height.equalTo(20)
        }
        
        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        menuButton.frame = CGRect(x: 0, y: 0, width: tabBarItemSize.width, height: tabBar.frame.size.height)
        menuButton.backgroundColor = .black
        menuButton.layer.cornerRadius = 22.5
        menuButton.setBackgroundImage(UIImage(named: "ic_cart"), for: .normal)
        menuButton.tintColor = zapatosColor
        
       view.addSubview(menuButton)
        
        menuButton.snp.makeConstraints { (make) in
      
            make.bottom.equalTo(view).offset(-90)
            make.right.equalTo(view).offset(-10)
            
            make.width.equalTo(45)
            make.height.equalTo(45)
        }

    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is TabBarController {
            if let newVC = tabBarController.storyboard?.instantiateViewController(withIdentifier: "auth") {
                tabBarController.present(newVC, animated: true)
                return false
            }
        }
        
        return true
    }

}
