//
//  NavigationController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
    var isTransparentMode = false
    var isShowingNavBar = false
    var statusBarStyle: UIStatusBarStyle = .default
    var originalShadowImage: UIImage?
    var originalBackgroundImage: UIImage?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        set {}
        get {
            return statusBarStyle
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        originalShadowImage = navigationBar.shadowImage
        originalBackgroundImage = navigationBar.backgroundImage(for: .default)
        makeOpaque()
    }

    func makeTransparent() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = .clear
        navigationBar.backgroundColor = .clear
        navigationBar.tintColor = .white
        
        navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font: R.font.openSansSemibold(size: 14)!,
        ]                
    }
    
    func makeOpaque() {
        navigationBar.setBackgroundImage(originalBackgroundImage, for: .default)
        navigationBar.shadowImage = originalShadowImage
        navigationBar.tintColor = kSecondaryTextColor
        
        navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor(hexString: "#030303")!,
            NSAttributedStringKey.font: R.font.openSansSemibold(size: 14)!,
        ]
    }
}
