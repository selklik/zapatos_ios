//
//  EmptyCartViewController.swift
//  Zapatos
//
//  Created by MacBook Air on 03/01/2019.
//  Copyright © 2019 Selklik Inc. All rights reserved.
//

import UIKit
import CoreData

class EmptyCartViewController: UIViewController {

    var addOrder = [OrderList]()
    let coredataOperations = PersistenceService()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        catchData()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        catchData()
    }
    func catchData(){
        let fetchRequest: NSFetchRequest<OrderList> = OrderList.fetchRequest()
        
        do {
            let people = try PersistenceService.context.fetch(fetchRequest)
            self.addOrder = people
            print(people.count,"count")
            getData()
            
        } catch {}
    }
    func getData() {
        let contex = PersistenceService.persistentContainer.viewContext
        do {
            addOrder = try contex.fetch(OrderList.fetchRequest())
            
        } catch {
            print("Error not get data")
        }
        
    }
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
       
    }
}
