//
//  SwapViewController.swift
//  Siti
//
//  Created by MacBook Air on 06/08/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip


class SwapViewController: ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
        
        
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = UIColor(hex: "#EDC35A")
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .red
        
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .gray
            newCell?.label.textColor = .black
        }
     super.viewDidLoad()
    }

    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
       
        let child_1 = storyboard!.instantiateViewController(withIdentifier: "ShoesKidzViewController") as! ShoesKidzViewController
        child_1.itemInfo = "KIDZ"
        
        let child_2 = storyboard!.instantiateViewController(withIdentifier: "ShoesMenViewController") as! ShoesMenViewController
        child_2.itemInfo = "MEN"
        
        let child_3 = storyboard!.instantiateViewController(withIdentifier: "ShoesWomenViewController") as! ShoesWomenViewController
        child_3.itemInfo = "WOMEN"
        
//        let child_4 = R.storyboard.check.instantiateInitialViewController()!
//         child_3.itemInfo = "Chexk"
        return [child_1, child_2, child_3]
        
    
    }

}
