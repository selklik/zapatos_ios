//
//  SingleProductViewController.swift
//  Zapatos
//
//  Created by MacBook Air on 01/10/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit
import DropDown
import PMAlertController
import CoreData

class SingleProductViewController: UIViewController {
    

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var pageControl:UIPageControl!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var discriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var addtocartOutlet: UIButton!
    @IBOutlet weak var buyOutlet: UIButton!
    @IBOutlet weak var addToCartView: UIView!
    @IBOutlet weak var butView: UIView!
    @IBOutlet weak var addtoCartImage: UIImageView!
    @IBOutlet weak var addtoCardLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var testView: UIView!
   
    
    let coredataOperations = PersistenceService()
    var addOrder = [OrderList]()
    
    var singleProduct: Shoes!
    var Size: SizeQuantity!
    var quantity = 0
    var index: Int = 0
    var sizeView = UIView()
    var emptyImageview = UIImageView()
    let chooseColor = DropDown()
    var user: User!
    var button = UIButton()
    var shipAdd: AddressData!
    var AddCount: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        collectionView.allowsMultipleSelection = true
       
        automaticallyAdjustsScrollViewInsets = false
        priceLabel.text = "RM \(singleProduct.price)"
        productName.text = singleProduct.prod_name
        discriptionLabel.text = singleProduct.description
   
        butimagetintcColor()
        self.pageControl.numberOfPages = singleProduct.products.count
        pageControl.transform = CGAffineTransform(scaleX: 1.5, y: 1.5);
        priceLabel.layer.cornerRadius = priceLabel.frame.size.height / 2
        quantityLabel.layer.cornerRadius = 12
        quantityLabel.isHidden = true
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.reloadData()
    
        let fetchRequest: NSFetchRequest<OrderList> = OrderList.fetchRequest()
        
        do {
            let people = try PersistenceService.context.fetch(fetchRequest)
            self.addOrder = people
           // print(people.count,"count")
            getData()
            
        } catch {}
        
        sizeBtn(index: 0)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.reloadData()
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case R.segue.singleProductViewController.showCart.identifier:
            guard let segueData = sender as? OrderList
                else { return }
            let nc = segue.destination as! NavigationController
            let cartVC = nc.viewControllers.first as! CartViewController
            cartVC.singleProduct = singleProduct
            cartVC.address = sender as? Address
            cartVC.listOrder = [segueData]
        
        default:
            break
        }
    }
    override func viewDidLayoutSubviews() {
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
    }
    @IBAction func closeBtb(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedToBuy(_ sender: UIButton) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "Auth")
        if launchedBefore{
            performSegue(withIdentifier: R.segue.singleProductViewController.showCart, sender: addOrder)
            postOrder()
        }
        else
            
        {
            performSegue(withIdentifier: R.segue.singleProductViewController.showAuth, sender: nil)
       
        }
        

    }
    func buyAlert(){
        
    }
    
    var ui : Int = 0
    
    func sizeBtn(index: Int) {
     
        let space:CGFloat = 2
        let buttonWidth = 30 as CGFloat
        let buttonHeight = 30 as CGFloat
        
        var yAxis : CGFloat = sizeView.frame.size.height/2
        

        for i in 0...(singleProduct.products[index].size_quantity.count - 1){
           // print(singleProduct.products[index].size_quantity[i],"coaaaaa")
            button = UIButton(type: .roundedRect)
            button.frame = CGRect(x: 0, y: yAxis, width: buttonWidth, height: buttonHeight)
            button.setTitle(singleProduct.products[index].size_quantity[i].size, for: .normal)
            button.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
            button.titleLabel?.font =   R.font.openSansBold(size: 20)
            button.setTitleColor(zapatosColor, for: .selected)
            button.setTitleColor(.white, for: .normal)
            button.clipsToBounds = true
            button.layer.cornerRadius = 15
            button.backgroundColor = UIColor.gray
            
            button.tag = i
           
            yAxis = yAxis+buttonHeight+space
            
            testView.addSubview(button)
            if i == 0 {
                
                button.tintColor = .black
                button.backgroundColor = .black
                button.isSelected = true
            }
        if singleProduct.products[index].size_quantity[i].quantity == "0"{
                button.backgroundColor = .lightGray
                let image = UIImage(named: "NavBarClose")
                button.setImage(image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
                button.tintColor = UIColor.red
            
                button.setTitle(nil, for: .normal)
                print("Out of Stock")
                button.isEnabled = false
            }
        }
        
    }
    
    @objc func handleButton(_ sender: UIButton) {

        for btn in testView.subviews as! [UIButton] {
        
            if sender.tag == btn.tag {
                
                if singleProduct.products[index].size_quantity[sender.tag].quantity == "0"{
                    
                    let image = UIImage(named: "NavBarClose")
                    btn.setImage(image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
                    btn.tintColor = UIColor.red
                    
                    btn.setImage(image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
                    btn.setTitle(nil, for: .normal)
                    btn.isEnabled = false
                    btn.backgroundColor = .lightGray
                    

                } else {
                    btn.isSelected = true
                    btn.tintColor = .black
                    btn.backgroundColor = .black
                  //  print(singleProduct.products[index].size_quantity[sender.tag].quantity,".size_quantity.count")
                    UserDefaults.standard.set(singleProduct.products[index].size_quantity[sender.tag].size, forKey: "savedSize")
                }
               
                } else {
                
                    btn.isSelected = false
                    btn.backgroundColor = .gray
                    btn.tintColor = .gray
               
            }
         
        }
      
    }

    func fetchAddress() {
        
        SecondAPI.makeGETRequest2(
               
            to: "cust.php?email=\(kAppDelegate.user!.email)",
            completion: { json in
                HUD.dismiss()
                self.shipAdd = AddressData(json)
                self.AddCount = self.shipAdd.data.count
                print(self.shipAdd.data.count,"aaaaaaaaa")
                self.performSegue(withIdentifier: R.segue.singleProductViewController.showCart, sender: nil)
        },
            failure: { json in
                HUD.dismiss()
                self.performSegue(withIdentifier: R.segue.singleProductViewController.showCart, sender: nil)
        }
        )
    }
    @IBAction func addTocartBtn(_ sender: Any) {
       
        let launchedBefore = UserDefaults.standard.bool(forKey: "Auth")
        if launchedBefore{
            if let button = sender as? UIButton {
                
                if button.isSelected {
                    
                    let alertVC = PMAlertController(
                        title:"",
                        description: "You have alredy added the item,\n do you want to add again?",
                        image: UIImage(named: "img.png"),
                        style: .alert
                    )
                    alertVC.addAction(PMAlertAction(title: "Yes", style: .default, action: { () -> Void in
                        
                        let userdefaults = UserDefaults.standard
                        
                        if userdefaults.string(forKey: "savedColor") != nil &&  userdefaults.string(forKey: "savedImage") != nil {
                            self.postOrder()
                        } else {
                            HUD.dismiss()
                            let alertVC = PMAlertController(title: "Alert!", description: "Choose Your Color or Size", image: nil, style: .alert)
                            
                            alertVC.addAction(PMAlertAction(
                                title: "OK",
                                style: .cancel,
                                action: nil
                            ))
                            self.present(alertVC, animated: true, completion: nil)
                        }
                        
                    }))
                    alertVC.addAction(PMAlertAction(title: "Go to Cart", style: .default, action: { () -> Void in
                        
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Store", bundle: nil)
                        let newViewController = storyBoard.instantiateViewController(withIdentifier: "CartViewController") as! NavigationController
                        self.present(newViewController, animated: true, completion: nil)
                        
                    }))
                    
                    self.present(alertVC, animated: true, completion: nil)
                    
                    
                } else {
                    let userdefaults = UserDefaults.standard
                    if userdefaults.string(forKey: "savedColor") != nil {
                        button.isSelected = true
                        addtoCartImage.image = addtoCartImage.image!.withRenderingMode(.alwaysTemplate)
                        addtoCartImage.tintColor = UIColor(hex: "#FBC254")
                        addToCartView.tintColor = UIColor(hex: "#FBC254")
                        addtoCardLabel.textColor = UIColor(hex: "#FBC254")
                        addtoCardLabel.font = UIFont.systemFont(ofSize: 10, weight: .semibold)
                        addToCartView.backgroundColor = .black
                        postOrder()
                    } else {
                        HUD.dismiss()
                        let alertVC = PMAlertController(title: "Alert!", description: "Choose Your Color or Size", image: nil, style: .alert)
                        
                        alertVC.addAction(PMAlertAction(
                            title: "OK",
                            style: .cancel,
                            action: nil
                        ))
                        self.present(alertVC, animated: true, completion: nil)
                    }
                    
                    
                }
            }
            
        }
        else
            
        {
    
            performSegue(withIdentifier: R.segue.singleProductViewController.showAuth, sender: nil)
        }
        
    }
    
    func postOrder(){
        let  add_order = OrderList(context: PersistenceService.context)
        let userdefaults = UserDefaults.standard
        
        if userdefaults.string(forKey: "savedColor") != nil {
             add_order.product_color = UserDefaults.standard.string(forKey: "savedColor")!
        } else {
            add_order.product_color  = singleProduct.products.first?.prod_color
        }
        if userdefaults.string(forKey: "savedImage") != nil {
            add_order.product_imag = UserDefaults.standard.string(forKey: "savedImage")!
        } else {
            add_order.product_imag = singleProduct.products.first?.prod_img
             print("no savedImage")
        }
        if userdefaults.string(forKey: "savedSize") != nil {
            add_order.product_size = UserDefaults.standard.string(forKey: "savedSize")!
        } else {
            add_order.product_size = singleProduct.products.first?.size_quantity.first?.size
        }
        add_order.product_id = singleProduct.id
        add_order.product_name = singleProduct.prod_name
        add_order.product_price = singleProduct.price
    
        PersistenceService.saveContext()
        self.addOrder.append(add_order)
        
    }
    func butimagetintcColor() {
        
        addtoCartImage.image = addtoCartImage.image!.withRenderingMode(.alwaysTemplate)
        addtoCartImage.tintColor = .black
    
        addToCartView.tintColor = .black
        addtocartOutlet.layer.cornerRadius = 40
        addToCartView.layer.cornerRadius = 40
        
        let buyButtonImage = UIImage(named: "ic_shoppingbag")
        buyOutlet.setImage(buyButtonImage!.withRenderingMode(.alwaysTemplate), for: .normal)
        buyOutlet.tintColor = .black
    }

}

        var oldValue: Int = 0
        var newValue: Int = 0


extension SingleProductViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //Change the current page
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        pageControl?.currentPage = Int(roundedIndex)
        newValue = Int(roundedIndex)
        
        if (oldValue != newValue){

            for mybt in self.testView.subviews as! [UIButton] {
                mybt.removeFromSuperview();
               
            }

              sizeBtn(index: newValue)
              print(sizeView.subviews.count,"index")
        }
              oldValue = newValue
        
        
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return singleProduct.products.count
    }
    
   

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        // this is called before the cell is displayed, check if this is the cell to be selected, and if yes, select it:
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .top)
        
        let savedImage = singleProduct.products[indexPath.row].prod_img
        let savedColor = singleProduct.products[indexPath.row].prod_color
        UserDefaults.standard.set(savedImage, forKey: "savedImage")
        UserDefaults.standard.set(savedColor, forKey: "savedColor")
        print("Changeed Selection",savedImage,savedColor)
    
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SingleProductCollectionViewCell
        cell.setData(singleProduct: singleProduct.products[indexPath.row].prod_img)
        
        if indexPath.row == 0 {
           
    
        } else{
        
        }

       
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
      //  print("selected")
        
    }
 
    func getData() {
        let contex = PersistenceService.persistentContainer.viewContext
        do {
            addOrder = try contex.fetch(OrderList.fetchRequest())
            
        } catch {
            print("Error not get data")
        }
        
    }

}

