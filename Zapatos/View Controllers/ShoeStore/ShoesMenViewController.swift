//
//  ViewController.swift
//  GridViewExampleApp
//
//  Created by Chandimal, Sameera on 12/22/17.
//  Copyright © 2017 Pearson. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Alamofire
import SwiftyJSON
import AlamofireImage


class ShoesMenViewController: UIViewController {
    
    var shoe: Datas!

    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var collectionView: UICollectionView!
    var itemInfo: IndicatorInfo = "View"

    var sizesd: Int = 0
    
    var estimateWidth = 160.0
    var cellMarginSize = 16.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SecondAPI.makeGETRequest2(
            to: "inside.php?category=Men",
            parameters: nil,
            completion: { (json) in
            self.shoe = Datas(json)
                
                self.sizesd = self.shoe.data.count
                // Set Delegates
                self.collectionView.delegate = self
                self.collectionView.dataSource = self
                self.collectionView.reloadData()
                print(self.shoe.data.count,"aaa")
                // Register cells
                self.collectionView.register(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
                
                // SetupGrid view
                self.setupGridView()
            
        },
            failure: nil
        )
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case R.segue.shoesMenViewController.showShoeDetail.identifier:

            let nc = segue.destination as! NavigationController
            let shoeDtetailVC = nc.viewControllers.first as! SingleProductViewController
            let selectedShoe = self.shoe.data[(sender as! IndexPath).row]
            shoeDtetailVC.singleProduct = selectedShoe
        
        default:
            break
        }
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setupGridView()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

   
    func setupGridView() {
        let flow = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
}

extension ShoesMenViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return sizesd
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: R.segue.shoesMenViewController.showShoeDetail, sender: indexPath)
        print("selected")
       
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as! ItemCell
        cell.setData(pro: self.shoe.data[indexPath.row])
        cell.layer.borderColor = UIColor.init(hexString: "#F4F4F4")?.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8 // optional
  
        
        return cell
    }
}

extension ShoesMenViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: width)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        
        return width
    }
}
extension ShoesMenViewController: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
