//
//  ShoesWomenViewController.swift
//  Siti
//
//  Created by MacBook Air on 31/07/2018.
//  Copyright © 2018 Selklik Inc. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ShoesWomenViewController: UIViewController,IndicatorInfoProvider {

    var itemInfo: IndicatorInfo = "View"
    @IBOutlet weak var comingSoonLablel: UILabel!
    @IBOutlet weak var uiView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
}
