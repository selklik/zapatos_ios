//
//  HomeViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 23/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SafariServices
import AVKit
import AVFoundation
import Lightbox
import FacebookCore
import FBSDKShareKit
import Alamofire

class HomeViewController: UIViewController {
    var products: [Product] = [Product(), Product(), Product()]
    var posts: [Post] = []
    var sections: [HomeViewControllerSection: Int] = [:]
    var refreshControl: UIRefreshControl!
    var banner: BannerData!
    var shoesList: Datas!
    var shoeCount: Int = 0
    
    @IBOutlet weak var shakeView: UIView!
    @IBOutlet weak var sorryLabel: UILabel!
    @IBOutlet weak var tryNextFriday: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    // Enable detection of shake motion
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
             shakeView.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { // in half a second...
                self.shakeView.isHidden = true
            }
            // shake()
            print("Why are you shaking me?")
        }
    }
    func shake(){
        let minimum = 0
        let maximum = 1000
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
       
        if dayInWeek == "Friday"
        {
            let randomInt = Int.random(in: minimum..<maximum)
           print(dayInWeek,"jeet gaya")
            
            let parameters: Parameters = [
                "name": kAppDelegate.user!.name,
                "email": kAppDelegate.user!.email
            ]
            
            let urlString = "cust.php?shake=\(parameters)"
            let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
           
          print(encodedUrl!)
            
            SecondAPI.makeGETRequest2(
                to: encodedUrl!,
                parameters: nil,
                completion: { (json) in
                    print(json,"hklhklhl")
                    if json == "success" {
                       
                        if (randomInt == 555) {
                            
                            self.sorryLabel.text = "Congratulations"
                            self.tryNextFriday.text = "Your name has been nominated for lucky draw."
                           
                        } else {
                            
                            //zapatos_promo.alpha = 1.0f
                            self.sorryLabel.text = "Sorry, Better luck next time."
                            self.tryNextFriday.text = "Try on next Friday"
                            
                        }
                    } else {
                        
                        //zapatos_promo.alpha = 1.0f
                        self.sorryLabel.text = "Sorry, You tried already"
                        self.tryNextFriday.text = "Try on next Friday"
                    }


                    
                    
            },
                failure: { (error) in
                
                 print(error,"jeet gaya")
            }
            )
           
        } else {
            
            sorryLabel.text = "Sorry, You tried already"
            tryNextFriday.text = "Try on next Friday"
        }

    }
    override func viewDidLoad() {
        
        
    
    
        
      self.becomeFirstResponder()
          shakeView.isHidden = true
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        AppEventsLogger.log("My custom event.");

        addTitlelessBackButton()

        sections = [
            .header: 0,
            .recentPosts: 1,
            .sectionHeader2: 2,
            .featuredProducts: 3
            
        ]

        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 49, right: 0)

        if let nc = navigationController as? NavigationController {
            nc.makeTransparent()
            nc.statusBarStyle = .default
            nc.setNeedsStatusBarAppearanceUpdate()
        }

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(R.nib.sectionHeaderTableViewCell)
        tableView.register(R.nib.recentPostsTableViewCell)
        tableView.register(R.nib.productTableViewCell)
      
     
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(orderCreated(_:)),
            name: NSNotification.Name(rawValue: "orderCreated"),
            object: nil
        )
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(fetch), for: .valueChanged)
        tableView.addSubview(refreshControl)

        fetch()
    }
    
   
    @objc func fetch() {
        SecondAPI.makeGETRequest2(
            to: "banner.php?banner=img",
            parameters: nil,
            completion: { (json) in
                self.banner = BannerData(json)
               // print("\(self.banner.data.count) banner")
                
        },
            failure: nil
        )
        SecondAPI.makeGETRequest2(
            to: "inside.php?category=Men&type=NewArrival",
            parameters: nil,
            completion: { (json) in
                
                self.shoesList = Datas(json)
                self.shoeCount = self.shoesList.data.count
                self.tableView.register(R.nib.productTableViewCell)
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
        },
            failure: nil
        )
         self.refreshControl.endRefreshing()
    }

    
    @objc func orderCreated(_ sender: Foundation.Notification) {
       // let order = sender.object as! Order
       // let product = order.product
        //let index = products.index { (p) -> Bool in return product.id == p.id }!
        //products[index] = product
        tableView.reloadSections(IndexSet(integer: self.sections[.featuredProducts]!), with: .fade)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }

        guard let nc = navigationController as? NavigationController else { return }

        if tableView.contentOffset.y > 190 {
            navigationItem.title = "Home"
            nc.makeOpaque()
            nc.statusBarStyle = .default
        }
        else {
            navigationItem.title = nil
            nc.makeTransparent()
            nc.statusBarStyle = .default
        }
        
        nc.setNeedsStatusBarAppearanceUpdate()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if let nc = navigationController as? NavigationController {
            nc.makeOpaque()
            nc.statusBarStyle = .default
            nc.setNeedsStatusBarAppearanceUpdate()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }

        switch identifier {
//        case R.segue.homeViewController.showProduct.identifier:
//            let productVC = segue.destination as! ProductViewController
//            productVC.product = sender as! Product
//        case R.segue.homeViewController.showFeed.identifier:
//            let feedVC = segue.destination as! FeedViewController
//            feedVC.posts = [sender as! Post]
//            feedVC.isSingle = true
//        case R.segue.homeViewController.showMenShoe.identifier:
//            let productsVC = segue.destination as! ShoesMenViewController
//            productsVC.shoe = sender as! Datas
            
        case R.segue.homeViewController.showProduct.identifier:
            let nc = segue.destination as! NavigationController
            let shoeDtetailVC = nc.viewControllers.first as! SingleProductViewController
         //  let selectedShoe = self.shoesList.data[(sender as! IndexPath).row]
            shoeDtetailVC.singleProduct = sender as? Shoes
           
        default:
            break
        }
    }
}


extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
       
        case sections[.featuredProducts]!:
            if shoeCount > 3 {
                return 3
            } else {
                return shoeCount
            }

        default:
            return 1
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
            
        case sections[.header]!:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.bannerTableViewCell, for: indexPath)!
            cell.configure()
            return cell
 
        case sections[.recentPosts]!:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.recentPostsTableViewCell, for: indexPath)!
            cell.zapatos(delegate: self)
            return cell

        case sections[.sectionHeader2]!:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.sectionHeaderTableViewCell, for: indexPath)!
            cell.configure(title: "Products", image: R.image.cart(), callToAction: "View all", object: self, type: nil, delegate: self)
            return cell

        case sections[.featuredProducts]!:
            let product = shoesList.data[indexPath.row]
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.productTableViewCell, for: indexPath)!
                cell.configure(product: product)

                return cell
       
        default:
            return UITableViewCell()
        }
    }
}


extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section != sections[.featuredProducts]! { return nil }
        
        
        let view = UIView()
        view.backgroundColor = .clear
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != sections[.featuredProducts]! { return 0 }
         if section != sections[.recentPosts]! { return 0 }
        
        return 15
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {

        case sections[.featuredProducts]!:
            guard (tableView.cellForRow(at: indexPath) as? ProductTableViewCell) != nil else { return }
            let product = self.shoesList.data[indexPath.row]
            performSegue(withIdentifier: R.segue.homeViewController.showProduct, sender: product)
       
        default:
            break
        }
       
    }
}


extension HomeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let nc = navigationController as? NavigationController else { return }

        if scrollView.contentOffset.y > 190 {
            navigationItem.title = "Home"
            nc.makeOpaque()
            nc.statusBarStyle = .default
        }
        else {
            navigationItem.title = nil
            nc.makeTransparent()
            nc.statusBarStyle = .default
        }

        nc.setNeedsStatusBarAppearanceUpdate()
    }
}


extension HomeViewController: RecentPostsTableViewCellDelegate {
    func recentPostsTableViewCell(cell: RecentPostsTableViewCell, didSelectPost post: Shoes) {
        performSegue(withIdentifier: R.segue.homeViewController.showProduct, sender: post)
    }
}



extension HomeViewController: SectionHeaderTableViewCellDelegate {
    func sectionHeaderTableViewCell(cell: SectionHeaderTableViewCell, didTapButton button: SectionHeaderButton, object: Any?, type: SectionHeaderTableViewCellType?) {
        performSegue(withIdentifier: R.segue.homeViewController.showShoeStore.identifier, sender: nil)
       
    }
    
}


