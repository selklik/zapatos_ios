//
//  CardsViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit

import PMAlertController

class CardsViewController: UIViewController {
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var addButton: UIButton!
    var cards: [Card] = []
    var isLoaded = false
    var isModal = false
    var delegate: CardsViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.layer.cornerRadius = 3
        bottomContainerView.layer.shadowColor = UIColor.black.cgColor
        bottomContainerView.layer.shadowOpacity = 0.25
        bottomContainerView.layer.shadowRadius = 4
        bottomContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(R.nib.notAddedTableViewCell)
        tableView.register(CardTableViewCell.classForCoder(), forCellReuseIdentifier: "CardTableViewCell")
        
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .stop,
                target: self,
                action: #selector(closeBarButtonTapped(_:))
            )
        }
        
        fetch()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case R.segue.cartViewController.showAddCard.identifier:
            let addCardVC = segue.destination as! AddCardViewController
            addCardVC.delegate = self
        default:
            break
        }
    }
    
    @objc func closeBarButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: R.segue.cardsViewController.showAddCard, sender: nil)
    }
    
    func fetch() {
        HUD.show()
        
        API.makeGETRequest(            
            to: "me/cards",
            completion: { json in
                HUD.dismiss()
                
                self.cards = json.arrayValue.map({ (json) -> Card in
                    return Card(json)
                })
                
                self.isLoaded = true
                self.tableView.reloadData()
            }
        )
    }
}

extension CardsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if !isLoaded {
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isLoaded {
            return 0
        }
        
        if cards.isEmpty {
            return 1
        }
        
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cards.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.notAddedTableViewCell, for: indexPath)!
            
            cell.configure(
                delegate: self,
                type: .card,
                image: R.image.noCard()!,
                description: "You have not added a credit or debit card yet 😖",
                buttonTitle: "Add a Card"
            )
            
            return cell
        }
        else {
            let card = cards[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as! CardTableViewCell
            cell.configure(card: card, isListMode: true)
            
            return cell
        }
    }
}


extension CardsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let card = cards[indexPath.row]
        return !card.is_default
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let card = cards[indexPath.row]
        
        let alertController = PMAlertController(
            title: "Confirmation",
            description: "Set this card as default?",
            image: nil,
            style: .alert
        )
        
        alertController.addAction(PMAlertAction(
            title: "OK",
            style: .default,
            action: {
                HUD.show()
                
                API.makePUTRequest(
                    to: "me/cards/\(card.id)",
                    parameters: ["is_default": true],
                    completion: { json in
                        if let delegate = self.delegate {
                            let card = Card(json)
                            delegate.cardsViewController(controller: self, didSetCardAsDefault: card)
                        }
                        self.fetch()
                    }
                )
        }
        ))
        
        alertController.addAction(PMAlertAction(
            title: "Cancel",
            style: .cancel,
            action: {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
        }
        ))
        
        present(alertController, animated: true, completion: nil)
    }
}


extension CardsViewController: NotAddedTableViewCellDelegate {
    func notAddedTableViewCell(cell: NotAddedTableViewCell, didTapButton button: UIButton, forType type: NotAddedTableViewCellType) {
        performSegue(withIdentifier: R.segue.cardsViewController.showAddCard, sender: nil)
    }
}

extension CardsViewController: AddCardViewControllerDelegate {
    func addCardViewController(controller: AddCardViewController, didCreateCard card: Card) {
        cards.append(card)
        
        if cards.count == 1 {
            if let delegate = delegate {
                delegate.cardsViewController(controller: self, didSetCardAsDefault: card)
            }
        }
        
        tableView.reloadData()
    }
}
