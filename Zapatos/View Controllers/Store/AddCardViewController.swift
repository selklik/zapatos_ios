//
//  AddCardViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 26/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import Stripe
import SnapKit

import PMAlertController

class AddCardViewController: UIViewController {
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { return .custom }
        set {}
    }
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var containerView: UIView!
    let paymentCardTextField = STPPaymentCardTextField()
    var delegate: AddCardViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
                
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.4
        boxView.layer.shadowOffset = CGSize(width: 0, height: 1)
        boxView.layer.shadowRadius = 2
        button.layer.cornerRadius = 3
        
        paymentCardTextField.borderWidth = 0
        paymentCardTextField.delegate = self
        paymentCardTextField.becomeFirstResponder()
        
        containerView.addSubview(paymentCardTextField)
        
        paymentCardTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        configureUI()
    }
    
    func configureUI() {
        button.isEnabled = paymentCardTextField.isValid
        
        if button.isEnabled {
            button.alpha = 1
        }
        else {
            button.alpha = 0.25
        }
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        let sourceParams = STPSourceParams.cardParams(withCard: paymentCardTextField.cardParams)
        
        HUD.show()
        
        STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
            if let source = source {
                API.makePOSTRequest(                    
                    to: "me/cards",
                    parameters: ["source_id": source.stripeID],
                    completion: { json in
                        HUD.showInfo(withStatus: "Card added")                        
                        self.delegate.addCardViewController(controller: self, didCreateCard: Card(json))
                        self.dismiss(animated: true, completion: nil)
                    }
                )
            }
            
            if let error = error {
                HUD.showError(withStatus: error.localizedDescription)
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        paymentCardTextField.resignFirstResponder()
        dismiss(animated: true, completion: nil)
    }
}


extension AddCardViewController: STPPaymentCardTextFieldDelegate {
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        configureUI()
    }
}
