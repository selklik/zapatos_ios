//
//  ReceiptViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 28/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit
import WebKit

class WebViewController: UIViewController {
    var webView: WKWebView!
    var Url: URL!
    var isModal = false
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Url!,"aaaaa")
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closeButtonTapped(_:)))
        }
        
        webView = WKWebView()
        webView.alpha = 0
        webView.navigationDelegate = self
        view.addSubview(webView)
        
        webView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
       HUD.show()
       webView.load(URLRequest(url: Url!))
    }
    
    @objc func closeButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}


extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.alpha = 1
        HUD.dismiss()
    }
}
