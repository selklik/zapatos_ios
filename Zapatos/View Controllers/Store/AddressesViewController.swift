//
//  AddressesViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import Alamofire

import PMAlertController

class AddressesViewController: UIViewController {
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var addButton: UIButton!
    
    var isLoaded = false
    var isModal = false
    var delegate: AddressesViewControllerDelegate?
    var user: User!
    var shipAdd: AddressData!
    var AddressCount: Int = 0
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addButton.layer.cornerRadius = 3
        bottomContainerView.layer.shadowColor = UIColor.black.cgColor
        bottomContainerView.layer.shadowOpacity = 0.25
        bottomContainerView.layer.shadowRadius = 4
        bottomContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(R.nib.notAddedTableViewCell)        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(fetch), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .stop,
                target: self,
                action: #selector(closeBarButtonTapped(_:))
            )
        }
       // print(fetch(),"fetch()")
        fetch()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case R.segue.addressesViewController.showAddAddress.identifier:
            let addAddressVC = segue.destination as! AddAddressViewController
            addAddressVC.delegate = self
        default:
            break
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: R.segue.addressesViewController.showAddAddress, sender: nil)
    }
    
    @objc func closeBarButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func fetch() {
        HUD.show()
        SecondAPI.makeGETRequest2(
            to: "cust.php?email=\(kAppDelegate.user!.email)",
            parameters: nil,
            completion: { (json) in
                HUD.dismiss()
  
                self.shipAdd = AddressData(json)
                self.AddressCount = self.shipAdd.data.count
             
                self.tableView.register(R.nib.addressTableViewCell)
                
                self.tableView.delegate = self
                self.tableView.dataSource = self
                
                self.isLoaded = true
                self.tableView.reloadData()
        },
            failure: nil
        )
      self.refreshControl.endRefreshing()
    }
}

extension AddressesViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if !isLoaded {
            return 0
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isLoaded {
            return 0
        }

        if shipAdd.data.isEmpty {
            return 1
        }
        
        return AddressCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if shipAdd.data.isEmpty {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.notAddedTableViewCell, for: indexPath)!

            cell.configure(
                delegate: self,
                type: .address,
                image: R.image.noAddress()!,
                description: "You have not added a shipping address yet 😞",
                buttonTitle: "Add an Address"
            )

            return cell
        }
        else {
            let address = shipAdd.data[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.addressTableViewCell, for: indexPath)!
            cell.configure(address: address, isListMode: true)
            
            return cell
        }
    }
}


extension AddressesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let address = shipAdd.data[indexPath.row]
        UserDefaults.standard.set(address.address, forKey: "is_default")
      
        let alertController = PMAlertController(
            title: "Confirmation",
            description: "Set this address as default?\n OR \nDelete this Address?",
            image: nil,
            style: .alert
        )
        
        alertController.addAction(PMAlertAction(
            title: "Delete Address",
            style: .default,
            action: {
            if let indexPath = self.tableView!.indexPathForSelectedRow {
                    self.tableView!.deselectRow(at: indexPath, animated: true)
                }
                let parameters: Parameters = [
                    "id": address.id,
                    "email": address.email
                    ]
                
                let jsonData = try! JSONSerialization.data(withJSONObject: parameters)
                let addressString = String(data: jsonData, encoding: String.Encoding.utf8)
                print(addressString!,"addressString")
                
                let urlString = "cust.php?deladd=\(addressString!)"
                let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                print(encodedUrl!,"encodedUrl")
                
                SecondAPI.makeGETRequest2(
                    to: encodedUrl!,
                    parameters: nil,
                    completion: { (json) in
                        self.tableView!.beginUpdates()
                        self.tableView!.endUpdates()
                        self.fetch()
                },
                    failure: { (error) in

                    HUD.showInfo(withStatus: "Address is Deleted")
                        self.tableView!.beginUpdates()
                        self.tableView!.endUpdates()
                        self.fetch()
                    }
                )
            
                
        }
        ))
        
        alertController.addAction(PMAlertAction(
            title: "Set as Default Address",
            style: .default,
            action: {
                HUD.show()
               
        let parameters: Parameters = [
                    "id": address.id,
                    "email": kAppDelegate.user!.email,
                    "is_default": "1",
                 
                ]
                print(parameters)
                
                let jsonData = try! JSONSerialization.data(withJSONObject: parameters)
                let addressString = String(data: jsonData, encoding: String.Encoding.utf8)
                print(addressString!,"addressString")
                
                let urlString = "cust.php?defadd=\(addressString!)"
                let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                print(encodedUrl!,"encodedUrl")
                
                SecondAPI.makeGETRequest2(
                    to: encodedUrl!,
                    parameters: nil,
                    completion: { (json) in
            
                        let address = Address(json)
                        self.delegate!.addressesViewController(controller: self, didSetAddressAsDefault: address)
    
                        self.tableView!.beginUpdates()
                        self.tableView!.endUpdates()
                        self.fetch()
                        
                },
                    failure: { (error) in
                
                        self.tableView!.beginUpdates()
                        self.tableView!.endUpdates()
                        self.fetch()
                }
                )
        }
        ))
       

        alertController.addAction(PMAlertAction(
            title: "Cancel",
            style: .cancel,
            action: {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    self.tableView.deselectRow(at: indexPath, animated: true)
                }
        }
        ))
        
        present(alertController, animated: true, completion: nil)
    }
}


extension AddressesViewController: NotAddedTableViewCellDelegate {
    func notAddedTableViewCell(cell: NotAddedTableViewCell, didTapButton button: UIButton, forType type: NotAddedTableViewCellType) {
        performSegue(withIdentifier: R.segue.addressesViewController.showAddAddress, sender: nil)
    }
}


extension AddressesViewController: AddAddressViewControllerDelegate {
    func addAddressViewController(controller: AddAddressViewController, didCreateAddress address: Address) {
        shipAdd.data.append(address)
        
        if shipAdd.data.count == 1 {
            if let delegate = delegate {
                delegate.addressesViewController(controller: self, didSetAddressAsDefault: address)
            }
        }
        
        tableView.reloadData()
    }
}
