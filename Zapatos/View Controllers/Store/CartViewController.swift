//
//  CartViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 03/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SafariServices
import PMAlertController
import CoreData
import Alamofire

class CartViewController: UIViewController {
    
    var singleProduct: Shoes!
    var listOrder = [OrderList]()
    var sections: [CartViewControllerSection: Int] = [:]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var subtotalLabel: UILabel!    
    @IBOutlet weak var shippingFee: UILabel!
    
    var card: Card?
    var address: Address!
    var Color: String!
    var Size: String!
    var sum = 0.00
    var url: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
           
        let fetchRequest: NSFetchRequest<OrderList> = OrderList.fetchRequest()
        
        do {
            let people = try PersistenceService.context.fetch(fetchRequest)
            self.listOrder = people
            print(people.count,"count")
            self.tableView!.beginUpdates()
            self.tableView!.endUpdates()
            getData()
        } catch {}
    
        title = "Checkout"
        addTitlelessBackButton()
       
        checkoutButton.layer.cornerRadius = 3
        bottomContainerView.layer.shadowColor = UIColor.black.cgColor
        bottomContainerView.layer.shadowOpacity = 0.25
        bottomContainerView.layer.shadowRadius = 4
        bottomContainerView.layer.shadowOffset = CGSize(width: 0, height: 0)

        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.register(R.nib.sectionHeaderTableViewCell)
        tableView.register(R.nib.notAddedTableViewCell)        
        tableView.register(CardTableViewCell.classForCoder(), forCellReuseIdentifier: "CardTableViewCell")
        tableView.register(R.nib.addressTableViewCell)
        checkoutButton.isEnabled = false
        checkoutButton.alpha = 0.25
        sections = [
            .sectionHeaderProducts: 0,
            .products: 1,
            .sectionHeaderAddress: 2,
            .address: 3
        ]
        checkoutButton.isEnabled = false
        checkoutButton.alpha = 0.25
      
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(orderNotCreated(_:)),
            name: NSNotification.Name(rawValue: "orderNotCreated"),
            object: nil
        )
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        self.tableView!.beginUpdates()
        self.tableView!.endUpdates()
        HUD.dismiss()
    }
    
    @objc func orderNotCreated(_ sender: Foundation.Notification) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }
        
        switch identifier {
        case R.segue.cartViewController.showAddCard.identifier:
            let addCardVC = segue.destination as! AddCardViewController
            addCardVC.delegate = self
        case R.segue.cartViewController.showAddAddress.identifier:
            let addAddressVC = segue.destination as! AddAddressViewController
            addAddressVC.delegate = self
        case R.segue.cartViewController.showAddresses.identifier:
            let addressesVC = segue.destination as! AddressesViewController
            addressesVC.delegate = self
        case R.segue.cartViewController.showCards.identifier:
            let cardsVC = segue.destination as! CardsViewController
            cardsVC.delegate = self
        case R.segue.cartViewController.showWebView.identifier:
            let webKitVC = segue.destination as! WebViewController
            webKitVC.Url = url
         
           
        default:
            break
        }
       
    }

    @IBAction func closeBarButtonTapped(_ sender: UIBarButtonItem) {
         self.tableView.reloadData()
         self.tableView!.beginUpdates()
         self.tableView!.endUpdates()
         dismiss(animated: true, completion: nil)
         UserDefaults.standard.removeObject(forKey: "is_default")
         UserDefaults.standard.removeObject(forKey: "savedColor")
         UserDefaults.standard.removeObject(forKey: "savedSize")
         UserDefaults.standard.removeObject(forKey: "savedImage")
    }
    

    @IBAction func checkoutButtonTapped(_ sender: UIButton) {
        print("tapped")
        
         HUD.show()
         let phone_no =  UserDefaults.standard.string(forKey: "is_default")!.components(separatedBy: "PhoneNo: ")[1].replacingOccurrences(of: ".", with: "").trimmingCharacters(in: .whitespaces)
       
        let total = sum
        var prod = [Any]()
        
        for i in 0...listOrder.count - 1 {
            let products: Parameters = [
                
                "prod_id": listOrder[i].product_id!,
                "prod_qnty": "1",
                "prod_color":listOrder[i].product_color!,
                "prod_size":listOrder[i].product_size!,
                "prod_img":listOrder[i].product_imag!
            ]
                 prod.append(products)
        }
    
//        for q in 0...prod.count - 1 {
//            print(prod[q],"aasaa")
//        }
//        
//       
        
        let parameters: Parameters = [
            "cus_name": kAppDelegate.user!.name,
            "cus_email": kAppDelegate.user!.email,
            "cus_address": UserDefaults.standard.string(forKey: "is_default")!,
            "cus_phone": phone_no,
            "cus_cunit": "RM",
            "cus_payMethod": "",
            "description": "Zapatos Sale Debit",
            "totalAmount": total*100,
            "products": prod
           ]
        
        let jsonData = try! JSONSerialization.data(withJSONObject: parameters)
        let addressString = String(data: jsonData, encoding: String.Encoding.utf8)
    //    print(addressString!,"addressString")

        print(parameters)
        
        let urlString = "http://shoplot.biz/billplz_sandbox/?prod_info=\(addressString!)"
        let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(urlString,"urlString")
        print(encodedUrl!,"encodedUrl")
        url = URL(string: encodedUrl!)!
       
        performSegue(withIdentifier: R.segue.cartViewController.showWebView, sender: url!)
        
        
    }
   
    
    func getData() {
        let contex = PersistenceService.persistentContainer.viewContext
        do {
            listOrder = try contex.fetch(OrderList.fetchRequest())
            
        } catch {
            print("Error not get data")
        }
        
    }
    func configureUI() {
        checkoutButton.isEnabled =  true
        checkoutButton.alpha = 1
       
    }
}


extension CartViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case sections[.products]!:
           
                sum = 0
                for item in listOrder {
                    let myString2 = item.product_price!
                    let myInt2 = (myString2 as NSString).integerValue
                    
                    let userdefaults = UserDefaults.standard
                    if let savedValue = userdefaults.string(forKey: "is_default") {
                        if savedValue.contains("Sarawak") || savedValue.contains("Sabah"){
                            shippingFee.text = "RM\(15)x\(listOrder.count)"
                            sum += Double(myInt2 + 15)
                            subtotalLabel.text = "RM\(sum)"
                            
                        } else {
                            shippingFee.text =  "RM\(10)x\(listOrder.count)"
                            sum += Double(myInt2 + 10)
                            subtotalLabel.text = "RM\(sum)"
                        }
                    } else {
                       
                    }
                    
                    
                }
            
            if listOrder.count == 0 {
                UserDefaults.standard.removeObject(forKey: "is_default")
                UserDefaults.standard.removeObject(forKey: "savedColor")
                UserDefaults.standard.removeObject(forKey: "savedSize")
                UserDefaults.standard.removeObject(forKey: "savedImage")
                dismiss(animated: true, completion: nil)
            }

            return listOrder.count
        default:
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case sections[.sectionHeaderProducts]!:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.sectionHeaderTableViewCell, for: indexPath)!
            cell.configure(title: "Item to be Purchased")

            return cell

        case sections[.products]!:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.cartItemTableViewCell, for: indexPath)!
            let listOrders = listOrder[indexPath.row]
             cell.configure(product: listOrders)
        
            return cell

    

        case sections[.sectionHeaderAddress]!:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.sectionHeaderTableViewCell, for: indexPath)!
            cell.configure(title: "Shipping Address", callToAction: "Manage", type: .manageAddresses, delegate: self)

            return cell

        case sections[.address]!:
            
            let userdefaults = UserDefaults.standard
            if userdefaults.string(forKey: "is_default") != nil{
                configureUI()
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.addressTableViewCell, for: indexPath)!
                cell.addressLabel.text = UserDefaults.standard.string(forKey: "is_default")
                
               
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.notAddedTableViewCell, for: indexPath)!
                cell.configure(
                    delegate: self,
                    type: .address,
                    image: R.image.noAddress()!,
                    description: "You have not select a shipping address yet 😞",
                    buttonTitle: "Select Address"
                )
                 return cell
               
               
            }

        default:
            return UITableViewCell()
        }
    }
   
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        switch  indexPath.section {
        case sections[.products]!:
            let contex = PersistenceService.persistentContainer.viewContext
            if editingStyle == .delete {
                let peoples = listOrder[indexPath.row]
                
                contex.delete(peoples)
                PersistenceService.saveContext()
                do {
                    
                    if listOrder.isEmpty{
                        dismiss(animated: true, completion: nil)
                    } else {
                    listOrder = try contex.fetch(OrderList.fetchRequest())
                    }
                    print("Delete")
                }
                catch {
                    print("Error not get data")
                }
            }
            
            self.tableView.reloadData()
            self.tableView!.beginUpdates()
            self.tableView!.endUpdates()
            
        default:
            break
        }
      
    }
}


extension CartViewController: SectionHeaderTableViewCellDelegate {
    func sectionHeaderTableViewCell(cell: SectionHeaderTableViewCell, didTapButton button: SectionHeaderButton, object: Any?, type: SectionHeaderTableViewCellType?) {
        guard let type = type else { return }
        
        switch type {
        case .manageCards:
            performSegue(withIdentifier: R.segue.cartViewController.showCards, sender: nil)
        case .manageAddresses:
            performSegue(withIdentifier: R.segue.cartViewController.showAddresses, sender: nil)
        }
    }
}


extension CartViewController: NotAddedTableViewCellDelegate {
    func notAddedTableViewCell(cell: NotAddedTableViewCell, didTapButton button: UIButton, forType type: NotAddedTableViewCellType) {
        switch type {
        case .card:
            performSegue(withIdentifier: R.segue.cartViewController.showAddCard, sender: nil)
        case .address:
            performSegue(withIdentifier: R.segue.cartViewController.showAddresses, sender: nil)
            print("sdsdf")
        }
    }
}


extension CartViewController: AddCardViewControllerDelegate {
    func addCardViewController(controller: AddCardViewController, didCreateCard card: Card) {
        self.card = card
        tableView.reloadData()
    }
}

extension CartViewController: AddAddressViewControllerDelegate {
    func addAddressViewController(controller: AddAddressViewController, didCreateAddress address: Address) {
        self.address = address
        tableView.reloadData()
    }
}


extension CartViewController: AddressesViewControllerDelegate {
    func addressesViewController(controller: AddressesViewController, didSetAddressAsDefault address: Address) {
        self.address = address
        tableView.reloadData()
    }
}


extension CartViewController: CardsViewControllerDelegate {
    func cardsViewController(controller: CardsViewController, didSetCardAsDefault card: Card) {
        self.card = card

        tableView.reloadData()
    }
}

