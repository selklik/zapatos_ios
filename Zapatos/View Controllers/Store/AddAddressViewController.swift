//
//  AddAddressViewController.swift
//  Zain Saidin
//
//  Created by Izad Che Muda on 27/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import DropDown
import Alamofire


class AddAddressViewController: UIViewController, UITextFieldDelegate{
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { return .custom }
        set {}
    }
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var button: UIButton!    
    @IBOutlet var textFieldContainerViews: [UIView]!
    @IBOutlet weak var addLine1: UITextField!
    @IBOutlet weak var addLine2: UITextField!
    @IBOutlet weak var zipCode: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var country: UITextField!
    @IBOutlet weak var selectState: UIButton!
    @IBOutlet weak var phoneNoTextField: UITextField!
    @IBOutlet weak var fullName: UITextField!
    
    
    var delegate: AddAddressViewControllerDelegate!
    let addressComponent = AddressComponent()
    var arrayofState = ["Johor","Kedah","Perak","Selangor","Malacca","Negeri Sembilan","Pahang","Perlis","Penang","Sabah","Sarawak","Terengganu"]
    let chooseState = DropDown()
    
    
    
    lazy var dropDowns: [DropDown] = {
        return [
            
            self.chooseState,
            
            ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDropDowns()
        dropDowns.forEach { $0.dismissMode = .onTap }
        dropDowns.forEach { $0.direction = .any }
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.4
        boxView.layer.shadowOffset = CGSize(width: 0, height: 1)
        boxView.layer.shadowRadius = 2
        button.layer.cornerRadius = 3
          self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AddAddressViewController.hideKeyboard)))
        zipCode.keyboardType = UIKeyboardType.numberPad
        phoneNoTextField.keyboardType = UIKeyboardType.numberPad
        
        textFieldContainerViews.forEach { (textFieldContainerView) in
            textFieldContainerView.layer.cornerRadius = 3
        }
        
       // textField.first!.becomeFirstResponder()
       // configureUI()
    }
    @IBAction func TappedchooseState(_ sender: Any) {
        chooseState.show()
    }
    func configureUI() {
        button.isEnabled = addressComponent.isValid
        
        if addressComponent.isValid {
            print(addressComponent.parameters)
        }
        
        if button.isEnabled {
            button.alpha = 1
        }
        else {
            button.alpha = 0.25
        }
    }
    func setupChooseDropDown() {
        chooseState.anchorView = selectState
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseState.bottomOffset = CGPoint(x: 0, y: selectState.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseState.dataSource = arrayofState
        
        // Action triggered on selection
        chooseState.selectionAction = { [weak self] (index, item) in
            self?.selectState.setTitle(item, for: .normal)
            self?.selectState.setTitleColor(.black, for: .normal)
            UserDefaults.standard.set(item, forKey: "state")
        }
    }
    
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    
    func setupDropDowns() {
        
        setupChooseDropDown()
        
    }
 
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        HUD.show()
       
       
        let states = UserDefaults.standard.string(forKey: "state")
        let addressArray : String = "(\(addLine1.text!) \(addLine2.text!)), Zipcode: \(zipCode.text!), City: \(city.text!), State: \(states!), Country: \("Malaysia"), PhoneNo: +60\(phoneNoTextField.text!)."
      
        print(addressArray,"addressArray")
        
       let parameters: Parameters = [
            "name": fullName.text!,
            "email": kAppDelegate.user!.email,
            "phone": "+60\(phoneNoTextField.text!)",
            "address": addressArray
        ]
        print(parameters)
        
        let jsonData = try! JSONSerialization.data(withJSONObject: parameters)
        let addressString = String(data: jsonData, encoding: String.Encoding.utf8)
         print(addressString!,"addressString")
        
        let urlString = "cust.php?custadd=\(addressString!)"
        let encodedUrl = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        print(encodedUrl!,"encodedUrl")
        
         SecondAPI.makeGETRequest2(
            to: encodedUrl!,
            parameters: nil,
            completion: { (json) in
                
                HUD.showInfo(withStatus: "Address added")
                self.delegate.addAddressViewController(controller: self, didCreateAddress: Address(json))
                self.dismiss(animated: true, completion: nil)
                
         },
           failure: { (error) in
                self.dismiss(animated: true, completion: nil)
                HUD.showInfo(withStatus: "Address added")
            }
            )
        
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}


