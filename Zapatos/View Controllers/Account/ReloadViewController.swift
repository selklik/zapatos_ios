//
//  ReloadViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/17/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyAttributes

class ReloadViewController: UIViewController {
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    @IBOutlet weak var tableView: UITableView!
    var delegate: ReloadViewControllerDelegate!
    var productsRequest: SKProductsRequest!
    var products: [SKProduct] = []
    var isModal = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(
                barButtonSystemItem: .stop,
                target: self,
                action: #selector(closeBarButtonTapped(_:))
            )
        }
        
        let productIdentifiers: Set<String> = [
            "com.selklik.iap.sitiapp.8_000",
            "com.selklik.iap.sitiapp.8_000",
            "com.selklik.iap.sitiapp.25_000",
            "com.selklik.iap.sitiapp.45_000",
            "com.selklik.iap.sitiapp.100_000",
            "com.selklik.iap.sitiapp.250_000"
        ]
        
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.register(R.nib.instructionTableViewCell)
        
        HUD.show()
        
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest.delegate = self
        productsRequest.start()
        
        SKPaymentQueue.default().add(self)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(userSaved(_:)),
            name: NSNotification.Name(rawValue: "userSaved"),
            object: nil
        )

    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func userSaved(_ sender: Foundation.Notification) {
        tableView.reloadData()
    }

    @objc func closeBarButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func fetchReceipt() {
        if let receiptURL = Bundle.main.appStoreReceiptURL {
            if let receipt = try? Data(contentsOf: receiptURL) {
                let receipt = receipt.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
                print("===================")
               // print(receipt)
                print("===================")
                API.makePOSTRequest(                    
                    to: "me/ios-topups",
                    parameters: ["receipt_data": receipt],
                    completion: { (json) in
                        let topup = IOSTopup(json)
                        kAppDelegate.user!.current_balance = topup.current_balance
                        
                        HUD.dismiss()
                        self.delegate.reloadViewController(controller: self, didPurchaseTopup: topup)
                        
                        if self.isModal {
                            self.dismiss(animated: true, completion: nil)
                        }
                        else {
                            self.navigationController!.popViewController(animated: true)
                        }
                },
                    failure: nil
                )
            }
        }
    }
}


extension ReloadViewController: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        products = response.products
        
        products.sort { (a, b) -> Bool in
            return a.price.doubleValue < b.price.doubleValue
        }
        
        HUD.dismiss()
        tableView.reloadData()
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        //print(error.localizedDescription)
    }
}


extension ReloadViewController: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        transactions.forEach { (transaction) in
            switch transaction.transactionState {
                case .purchased:
                   // print("purchased")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    fetchReceipt()
                    
                case .failed:
                    HUD.dismiss()
                   // print("failed")
                  
                    if let error = transaction.error {
                        print(error.localizedDescription)
                    }
                    else {
                     //   print("Failed to purchase")
                    }
                    
                    SKPaymentQueue.default().finishTransaction(transaction)
                    
                case .restored:
                    HUD.dismiss()
                    //print("restored")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    
                case .deferred:
                    HUD.dismiss()
                   // print("deferred")
                    
                case .purchasing:
                    print("purchasing")
            }
        }
    }
}


extension ReloadViewController: ReloadTableViewCellDelegate {
    func reloadTableViewCell(cell: ReloadTableViewCell, didTapBuyButton button: UIButton, forProduct product: SKProduct) {
        HUD.show()
        
        print("Attempting to purchase")
        
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
}


extension ReloadViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if products.count == 0 {
            return 0
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if products.count == 0 {
            return 0
        }
        
        switch section {
        case 1:
            return products.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            var attributedText = "You currently have ".attributedString
            
            if kAppDelegate.user!.current_balance > 0 {
                attributedText = attributedText + "\(kAppDelegate.user!.current_balance.withComma) Stars".withFont(R.font.openSansBold(size: 14)!)
            }
            else {
                attributedText = attributedText + "0 Star".withFont(R.font.openSansBold(size: 14)!)
            }
            
            attributedText = attributedText + ". You are free to reload anytime you want. Choose a reload amount:".attributedString
            
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.instructionTableViewCell, for: indexPath)!
            cell.configure(attributedText: attributedText)
            
            return cell
            
        case 1:
            let product = products[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.reloadTableViewCell, for: indexPath)!
            cell.configure(delegate: self, product: product)
            
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}
