//
//  WebKitViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 3/6/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit
import SnapKit
import WebKit

class WebKitViewController: UIViewController {
    var webView: WKWebView!
    var url: URL!
    var trackid: String!
    var isModal = false
    
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       print(trackid)
        if isModal {
            navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(closeButtonTapped(_:)))
        }
        
        webView = WKWebView()
        webView.alpha = 0
        webView.navigationDelegate = self
        view.addSubview(webView)
        
        webView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        HUD.show()
        webView.load(URLRequest(url: url))
    }
    
    @objc func closeButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}


extension WebKitViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.alpha = 1
        HUD.dismiss()
    }
}
