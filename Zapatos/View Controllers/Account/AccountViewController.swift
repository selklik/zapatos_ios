//
//  AskViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 23/11/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import SnapKit
import PMAlertController

class AccountViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var isLoaded = false
    var Order: OrderData!
    var orderCount: Int = 0
    var orderIndex: Int = 0
    var u: String!
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let accountTitleView = R.nib.accountTitleView.firstView(owner: nil)!
        accountTitleView.configure(delegate: self)
        
        navigationItem.titleView = accountTitleView
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        tableView.register(R.nib.sectionHeaderTableViewCell)
        tableView.register(R.nib.loadingTableViewCell)
        tableView.register(R.nib.notificationTableViewCell)
        addTitlelessBackButton()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(fetch), for: .valueChanged)
        tableView.addSubview(refreshControl)
        tableView.reloadData()
        fetch()
        
    }
  
    
    @objc func userSaved(_ sender: Foundation.Notification) {
        tableView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserDefaults.standard.string(forKey: "trackId") != nil {
            UserDefaults.standard.removeObject(forKey: "trackId")
        } else {
            fetch()
        }
       
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case R.segue.accountViewController.showWebKit.identifier:
            let webKitVC = segue.destination as! WebKitViewController
            let s = sender as! [Any]
            webKitVC.url = s[0] as? URL
            webKitVC.title = s[1] as? String
            webKitVC.trackid = Order.data[orderIndex].trackid
        case R.segue.accountViewController.showReload.identifier:
            let reloadVC = segue.destination as! ReloadViewController
            reloadVC.delegate = self
            

        default:
            break
        }
    }
    
    @objc func fetch() {
        
        SecondAPI.makeGETRequest2(
            to: "cust.php?listorder=\(kAppDelegate.user!.email)",
            parameters: nil,
            completion: { json in
                self.refreshControl.endRefreshing()
                self.Order = OrderData(json)
                self.orderCount = self.Order.data.count
                print("Total Order", self.Order.data.count )
                self.tableView.delegate = self
                self.tableView.dataSource = self
                self.tableView.reloadData()
                
                self.isLoaded = true
            
            },
            
            failure: nil
        )
        
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {        
        return 6
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            if isLoaded && !Order.data.isEmpty {
                return orderCount
             }
            
            return 1
        case 2:
            return 1
        case 3:
            return 3
        case 4:
            return 1
        case 5:
            return 2
        default:
            return 0
        }
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.yourOrdersTableViewCell, for: indexPath)!
            return cell
            
        case 1:
            if isLoaded && !Order.data.isEmpty {
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.orderTableViewCell, for: indexPath)!
                cell.configure(delegate: self, order: Order.data[indexPath.row])
                
                return cell
            }
            else if !isLoaded {
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.loadingTableViewCell, for: indexPath)!
                return cell
            }
            else if isLoaded && Order.data.isEmpty {
                let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.emptyOrderTableViewCell, for: indexPath)!
                return cell
            }
            else {
                return UITableViewCell()
            }
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccuontButtonTableViewCell", for: indexPath) as! AccuontButtonTableViewCell
                cell.nameLabel.text = "Shipping Addresses"
                cell.iconImage.image = UIImage(named: "ShippingAdd")
    
                return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccuontButtonTableViewCell", for: indexPath) as! AccuontButtonTableViewCell
            if indexPath.row == 0 {
                cell.nameLabel.text = "Terms of Service"
                cell.iconImage.image = UIImage(named: "terms&service")

            }
            else if indexPath.row == 1 {
                cell.nameLabel.text = "Privacy Policy"
                cell.iconImage.image = UIImage(named: "privacy&policy")

            }
            else {
                cell.nameLabel.text = "F.A.Q"
                cell.iconImage.image = UIImage(named: "f.a.q")
            }

            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
            cell.notifiLabel.text = "Notification"
            cell.notifiImage.image = UIImage(named: "notification")
            return cell
         case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AccuontButtonTableViewCell", for: indexPath) as! AccuontButtonTableViewCell
            
            if indexPath.row == 0 {
                cell.nameLabel.text = "Edit Profile"
                cell.iconImage.image = UIImage(named: "editProfile")
                
            }
            else {
                cell.nameLabel.text = "Logout"
                cell.iconImage.image = UIImage(named: "logout")
            }
            return cell
           

        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }
        
        let view = UIView()
        view.backgroundColor = .clear

        return view
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        
        return 15
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.tuple {
        case (2, 0):
            performSegue(withIdentifier: R.segue.accountViewController.showAddresses, sender: nil)
        
        case (3, 0):
            let url = URL(string: "https://myyb2.mobsocial.net/pages/tos")!
            performSegue(withIdentifier: R.segue.accountViewController.showWebKit, sender: [url, "Terms of Service"])

        case (3, 1):
            let url = URL(string: "https://myyb2.mobsocial.net/pages/privacy")!
            performSegue(withIdentifier: R.segue.accountViewController.showWebKit, sender: [url, "Privacy Policy"])

        case (3, 2):
            let url = URL(string: "https://myyb2.mobsocial.net/faq")!
            performSegue(withIdentifier: R.segue.accountViewController.showWebKit, sender: [url, "Frequently Asked Question"])
     
        case (5, 0):
            performSegue(withIdentifier: R.segue.accountViewController.showEditProfile, sender: nil)
    
        case (5, 1):
            let alertController = PMAlertController(
                title: "Confirmation",
                description: "Are you sure you want to log out?",
                image: nil,
                style: .alert
            )

            alertController.addAction(PMAlertAction(
                title: "Yes",
                style: .default,
                action: {
                    if let oneSignalUserID = kAppDelegate.oneSignalUserID {
                        HUD.show()
                        
                        API.makePOSTRequest(                            
                            to: "auth/logout",
                            parameters: ["device_id": oneSignalUserID],
                            completion: { (json) in
                                HUD.dismiss()
                                kAppDelegate.logout()
                                UserDefaults.standard.removeObject(forKey: "is_default")
                                UserDefaults.standard.removeObject(forKey: "savedColor")
                                UserDefaults.standard.removeObject(forKey: "savedSize")
                                UserDefaults.standard.removeObject(forKey: "savedImage")
                                UserDefaults.standard.removeObject(forKey: "trackid")
                            },
                            failure: nil
                        )
                    }
                    else {
                        kAppDelegate.logout()
                    }
                }
            ))

            alertController.addAction(PMAlertAction(
                title: "Cancel",
                style: .cancel,
                action: {
                    if let indexPath = self.tableView.indexPathForSelectedRow {
                        self.tableView.deselectRow(at: indexPath, animated: true)
                    }
                }
            ))

            present(alertController, animated: true, completion: nil)
            
        default:
            guard let _ = tableView.cellForRow(at: indexPath) as? OrderTableViewCell else { break }
            
            let order = Order.data[indexPath.row]
            if  order.trackid.isEmpty{
                  
            } else {
            let url = URL(string: "https://www.tracking.my/poslaju/\(order.trackid)")!
            performSegue(withIdentifier: R.segue.accountViewController.showWebKit, sender: [url, "Order Track"])
            }
        }
    }
    
}


extension AccountViewController: AccountTitleViewDelegate {
    func accountTitleView(view: AccountTitleView, didTapButton button: UIButton) {
        print("boom")
    }    
}
extension AccountViewController: TrackingTableViewDelagate {
    func trackTapped(cell: OrderTableViewCell, didTapBuyButton button: UIButton) {
//        let ord = Order.data.count
//       // cell.configure(delegate: self, order: Order)
//        print("tracking")
//        let trackid = UserDefaults.standard.string(forKey: "trackid")
//        let url = URL(string: "https://www.tracking.my/poslaju/\(trackid!)")!
//        print(url)
//        performSegue(withIdentifier: R.segue.accountViewController.showWebKit, sender: [url, "Order Track"])
    }
    
}

extension AccountViewController: ReloadViewControllerDelegate {
    func reloadViewController(controller: ReloadViewController, didPurchaseTopup topup: IOSTopup) {
    }

}
