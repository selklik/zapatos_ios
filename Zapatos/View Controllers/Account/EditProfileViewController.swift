//
//  EditProfileViewController.swift
//  Zain Saidin
//
//  Created by MacBook Air on 19/12/2017.
//  Copyright © 2017 Selklik Inc. All rights reserved.
//

import UIKit
import PMAlertController

class EditProfileViewController: UIViewController {
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    var user: User!
    var fileURL: URL?
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let nc = navigationController as? NavigationController {
            nc.isTransparentMode = true
        }

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(userSaved(_:)),
            name: NSNotification.Name(rawValue: "userSaved"),
            object: nil
        )
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func userSaved(_ sender: Foundation.Notification) {
        tableView.reloadData()
    }

    @IBAction func closeButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}

extension EditProfileViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 2
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfilePhotoTableViewCell", for: indexPath) as! EditProfilePhotoTableViewCell
            cell.configure(user: kAppDelegate.user!)

            cell.ProfilePhoto?.isUserInteractionEnabled = true

            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileTableViewCell", for: indexPath) as! EditProfileTableViewCell
            cell.configure(user: kAppDelegate.user!, row: indexPath.row)
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}

extension EditProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 209
        case 1:
            return 70
        default:
            return 44
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.tuple {
        case (0, 0):
            guard let cell = tableView.cellForRow(at: indexPath) as? EditProfilePhotoTableViewCell else { return }
            
            let alertController = UIAlertController(
                title: nil,
                message: nil,
                preferredStyle: .actionSheet
            )

            alertController.addAction(UIAlertAction(
                title: "Take a Picture",
                style: .default,
                handler: { (action) in
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .camera

                    self.present(imagePickerController, animated: true, completion: nil)
                }
            ))

            alertController.addAction(UIAlertAction(
                title: "Choose from Photo Library",
                style: .default,
                handler: { (action) in
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self
                    imagePickerController.sourceType = .photoLibrary

                    self.present(imagePickerController, animated: true, completion: nil)
                }
            ))

            alertController.addAction(UIAlertAction(
                title: "Cancel",
                style: .cancel,
                handler: nil
            ))
            alertController.popoverPresentationController?.sourceView = cell.tapView
            present(alertController, animated: true, completion: nil)
        case(1, 0):
            let alertController = PMAlertController(
                title: "Edit Name",
                description: "Please enter your name",
                image: nil,
                style: .alert
            )

            alertController.addTextField({ (textField) in
                textField!.placeholder = kAppDelegate.user!.name
            })

            alertController.addAction(PMAlertAction(
                title: "Submit",
                style: .default,
                action: {
                    HUD.show()

                    API.makePUTRequest(
                        to: "me",
                        parameters: ["name": alertController.textFields.first!.text!],
                        completion: { (json) in
                            HUD.dismiss()
                            let user = User(json)
                            kAppDelegate.user = user
                        },
                        failure: nil
                    )
                }
            ))

            alertController.addAction(PMAlertAction(
                title: "Cancel",
                style: .cancel
            ))

            present(alertController, animated: true, completion: nil)
        case(1, 1):
            let alertController = PMAlertController(
                title: "Change password",
                description: "Please enter your current and new password",
                image: nil,
                style: .alert
            )

            alertController.addTextField({ (textField) in
                textField!.isSecureTextEntry = true
                textField!.placeholder = "Current password"
            })

            alertController.addTextField({ (textField) in
                textField!.isSecureTextEntry = true
                textField!.placeholder = "New password"
            })

            alertController.addAction(PMAlertAction(
                title: "Submit",
                style: .default,
                action: {
                    HUD.show()
                    let textFields = alertController.textFields

                    API.makePUTRequest(
                        to: "me/password",
                        parameters: [
                            "current_password": textFields[0].text!,
                            "new_password": textFields[1].text!
                        ],
                        completion: { (json) in
                            HUD.dismiss()
                            let user = User(json)
                            kAppDelegate.user = user
                        },
                        failure: { result in
                            HUD.showError(withStatus: "Failed to change password. Make sure your current password is correct.")
                        }
                    )
                }
                ))

            alertController.addAction(PMAlertAction(
                title: "Cancel",
                style: .cancel
                ))

            present(alertController, animated: true, completion: nil)

        default:
            break
        }
    }
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        HUD.show()

        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = tableView.cellForRow(at: indexPath) as! EditProfilePhotoTableViewCell
            cell.ProfilePhoto.image = image
            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let path = "\(documentsDirectory)/\(Int(NSDate().timeIntervalSince1970)).png"

            if let data = UIImagePNGRepresentation(image) {
                self.fileURL = URL(fileURLWithPath: path)
                try? data.write(to: fileURL!)

                API.makeUploadRequest(
                    to: "me/profile-photos",
                    fileURL: fileURL!,
                    parameters: nil,
                    completion: { (json) in
                      //  print(json["urls"])
                        kAppDelegate.user!.profile_photos = Versions(json["urls"])
                        HUD.dismiss()
                        HUD.showInfo(withStatus: "Profile photo updated")
                    },
                    failure: nil
                )
            }

            dismiss(animated: true, completion: nil)
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

