//
//  HistoryViewController.swift
//  Selklik
//
//  Created by Izad Che Muda on 2/17/17.
//  Copyright © 2017 Izad Che Muda. All rights reserved.
//

import UIKit

import SwiftyAttributes

class HistoryViewController: UIViewController {
    override var hidesBottomBarWhenPushed: Bool {
        get { return true }
        set {}
    }
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    var transactions: [Transaction] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        tableView.register(HistoryTableViewCell.classForCoder(), forCellReuseIdentifier: "HistoryTableViewCell")
        tableView.register(R.nib.instructionTableViewCell)
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        fetch(showLoader: true)
    }
    
    @objc func refresh() {
        fetch(showLoader: false)
    }
    
    func fetch(showLoader: Bool) {
        if showLoader {
            HUD.show()
        }
        
        API.makeGETRequest(            
            to: "me/transactions",
            completion: { json in
                let transactions = json.arrayValue.map({ (json) -> Transaction in
                    return Transaction(json)
                })
                
                HUD.dismiss()
                self.refreshControl.endRefreshing()
                self.transactions = transactions
                
                if transactions.count == 0 {
                    let imageView = UIImageView()
                    imageView.image = R.image.empty()
                    imageView.contentMode = .center
                    
                    self.tableView.backgroundView = imageView
                }
                else {
                    self.tableView.backgroundView = nil
                }
                
                self.tableView.reloadData()
            }
        )                
    }
}


extension HistoryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if transactions.count == 0 {
            return 0
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if transactions.count == 0 {
            return 0
        }
        
        switch section {
        case 1:
            return transactions.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let transaction = transactions[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
            cell.configure(transaction: transaction)
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.instructionTableViewCell, for: indexPath)!
            cell.configure(attributedText: "The complete history of your in-app purchases and reloads are shown here.".attributedString)
            
            return cell
        }
    }
    
}
